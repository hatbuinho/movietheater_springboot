# **Movie Theater**

## Spring boot (Backend)
>Create database using `SQL Server` or `MySQL`, etc...  

>Change config in application.properties

>Start Spring Boot using `Eclipse` or `Intellij IDEA`


## ReactJS (Frontend)
Open terminal and run command
```bash
# access to ReactJS location
cd frontend

# install all dependencies
npm install 

# run ReactJS in development mode
npm start

# build ReactJS for deployment
npm build

