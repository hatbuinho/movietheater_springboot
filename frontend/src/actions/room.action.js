import { RoomType, MessageType } from './types';
import RoomService from '../services/room';
import { handleCommonError } from './handleError';

const getAllRooms = () => async (dispatch) => {
  try {
    const res = await RoomService.getAll();

    dispatch({
      type: RoomType.GET_ALL,
      payload: res.data
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

const updateRoom =
  (id, room = {}) =>
  async (dispatch) => {
    try {
      const res = await RoomService.update(id, room);
      dispatch({
        type: RoomType.UPDATE,
        payload: res.data
      });
      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: {message: 'Update Room successfully'}
      });
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

export { updateRoom, getAllRooms };
