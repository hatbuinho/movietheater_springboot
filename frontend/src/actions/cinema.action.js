import { CinemaType, MessageType } from './types';
import CinemaService from '../services/cinema';
import { handleCommonError } from './handleError';

const createCinema =
  (cinema = {}) =>
  async (dispatch) => {
    try {
      const res = await CinemaService.create(cinema);

      dispatch({
        type: CinemaType.CREATE,
        payload: res.data
      });

      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: 'Create Cinema successfully'
      });

      return Promise.resolve();
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

const updateCinema =
  (id, cinema = {}) =>
  async (dispatch) => {
    try {
      const res = await CinemaService.update(id, cinema);
      dispatch({
        type: CinemaService.update,
        payload: res.data
      });
      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: 'Update Cinema successfully'
      });
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

const deleteCinema = (id) => async (dispatch) => {
  try {
    await CinemaService.deleteById(id);
    dispatch({
      type: CinemaType.DELETE,
      payload: { id }
    });
    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: 'Delete successfully'
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

const deleteAllCinema = () => async (dispatch) => {
  try {
    await CinemaService.deleteAll();
    dispatch({
      type: CinemaType.DELETE_ALL
    });
    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: 'Delete successfully'
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

export { createCinema, updateCinema, deleteCinema, deleteAllCinema };
