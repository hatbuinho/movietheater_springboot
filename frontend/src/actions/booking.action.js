import BookingService from '../services/booking';
import { handleCommonError } from './handleError';
import { setMessage } from './message.action';
import { BookingType } from './types';

let message = '';

export const selectSeat = (seat) => ({
  type: BookingType.SELECT,
  payload: seat
});

export const getSelectedSeat = () => ({
  type: BookingType.GET_SELECTED
});

export const clearSelectedSeat = () => ({
  type: BookingType.CLEAR_SELETED_SEAT
});

export const setQuantity = (quantity) => ({
  type: BookingType.SET_QUANTITY,
  payload: quantity
});

export const getQuantity = () => ({
  type: BookingType.GET_QUANTITY
});

export const createBooking =
  (numberOfSeat, showSeatIds = [], userId, showId) =>
  async (dispatch) => {
    console.log(numberOfSeat);
    try {
      const res = await BookingService.book({
        numberOfSeat,
        showSeatIds,
        userId,
        showId
      });
      console.log(res.data);
      dispatch({
        type: BookingType.BOOK_TICKETS,
        payload: res.data
      });

      return Promise.resolve();
    } catch (err) {
      console.log(err);
      return handleCommonError(err, dispatch);
    }
  };

export const confirmBooking = (payment) => async (dispatch) => {
  try {
    await BookingService.confirm(payment);
    message = 'Confirm successfully';
    dispatch(setMessage({message}));
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

export const checkinBooking = (bookingId) => async (dispatch) => {
  try {
    console.log('booking id', bookingId);
    const res = await BookingService.checkIn(bookingId);
    console.log(res.data);
    dispatch({
      type: BookingType.CHECK_IN,
      payload: res.data
    });

    message = 'Check-in successfully';
    dispatch(setMessage({message}));
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

export const cancelBooking = (bookingId) => async (dispatch) => {
  try {
    const res = await BookingService.cancel(bookingId);
    dispatch({
      type: BookingType.CANCEL,
      payload: res.data
    });
    message = 'Cancel successfully'
    dispatch(setMessage({message}));
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

export const findBookingByUser = (userId) => async (dispatch) => {
  try {
    const res = await BookingService.findUserBooking(userId);
    dispatch({
      type: BookingType.FIND_BY_USER,
      payload: res.data
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

export const findAllBookings = () => async (dispatch) => {
  try {
    const res = await BookingService.findAll();
    dispatch({
      type: BookingType.FIND_ALL,
      payload: res.data
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};
