import { SeatType, MessageType } from './types';
import SeatService from '../services/seat';
import { handleCommonError } from './handleError';

let message = '';


const getAllSeats = () => async (dispatch) => {
  try {
    const res = await SeatService.getAll();

    dispatch({
      type: SeatType.GET_ALL,
      payload: res.data
    });

    return Promise.resolve();
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

const createSeat =
  (seat = {}) =>
  async (dispatch) => {
    try {
      const res = await SeatService.create(seat);

      dispatch({
        type: SeatType.CREATE,
        payload: res.data
      });

      message = 'Create Seat successfully';

      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: { message }
      });

      return Promise.resolve();
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

const updateSeat =
  (id, seat = {}) =>
  async (dispatch) => {
    console.log(seat);
    try {
      const res = await SeatService.update(id, seat);
      dispatch({
        type: SeatType.UPDATE,
        payload: res.data
      });
      message = 'Update Seat successfully';
      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: { message }
      });
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

const deleteSeat = (id) => async (dispatch) => {
  try {
    await SeatService.deleteById(id);
    dispatch({
      type: SeatType.DELETE,
      payload: { id }
    });
    message = 'Delete successfully';
    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: { message }
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

const deleteAllSeats = () => async (dispatch) => {
  try {
    await SeatService.deleteAll();
    dispatch({
      type: SeatType.DELETE_ALL
    });
    message = 'Delete successfully';
    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: { message }
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

export { createSeat, updateSeat, deleteAllSeats, deleteSeat, getAllSeats };
