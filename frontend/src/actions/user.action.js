import { UserType, MessageType } from './types';
import { handleCommonError } from './handleError';
import UserService from '../services/user/user.service';

let message = '';


const findALlUser = () => async (dispatch) => {
  try {
    const res = await UserService.findAll();

    dispatch({
      type: UserType.FIND_ALL,
      payload: res.data
    });

    return Promise.resolve();
  } catch (err) {
    console.log(err);
    return handleCommonError(err, dispatch);
  }
};

const findUserById = (userId) => async (dispatch) => {
  try {
    const res = await UserService.findById(userId);
    dispatch({
      type: UserType.FIND_BY_ID,
      payload: res.data
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return handleCommonError();
  }
};

const createUser =
  (user = {}) =>
  async (dispatch) => {
    try {
      const res = await UserService.create(user);

      dispatch({
        type: UserType.CREATE,
        payload: res.data
      });
      message = 'Create User successfully';
      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: { message }
      });

      return Promise.resolve();
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

const updateUser =
  (id, user = {}) =>
  async (dispatch) => {
    try {
      const res = await UserService.update(id, user);
      dispatch({
        type: UserType.UPDATE,
        payload: res.data
      });
      message = 'Update User successfully';
      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: { message }
      });
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

const deleteUserById = (id) => async (dispatch) => {
  try {
    await UserService.deleteById(id);
    dispatch({
      type: UserType.DELETE_BY_ID,
      payload: { userId: id }
    });
    message = 'Delete successfully';
    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: { message }
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

const UserAction = {
  createUser,
  updateUser,
  deleteUserById,
  findALlUser,
  findUserById
};

export default UserAction;
