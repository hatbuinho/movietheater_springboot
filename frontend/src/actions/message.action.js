import { MessageType } from './types';

const setMessage = (message) => ({
  type: MessageType.SET_MESSAGE,
  payload: message
});

const clearMessage = () => ({
  type: MessageType.CLEAR_MESSAGE
});

export {setMessage, clearMessage}