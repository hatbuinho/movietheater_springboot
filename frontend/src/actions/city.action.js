import { CityType, MessageType } from './types';
import CityService from '../services/City';
import { handleCommonError } from './handleError';

const createCity =
  (cinema = {}) =>
  async (dispatch) => {
    try {
      const res = await CityService.create(cinema);

      dispatch({
        type: CityType.CREATE,
        payload: res.data
      });

      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: 'Create Cinema successfully'
      });

      return Promise.resolve();
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

const updateCity =
  (id, cinema = {}) =>
  async (dispatch) => {
    try {
      const res = await CityService.update(id, cinema);
      dispatch({
        type: CityService.update,
        payload: res.data
      });
      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: 'Update City successfully'
      });
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

const deleteCity = (id) => async (dispatch) => {
  try {
    await CityService.deleteById(id);
    dispatch({
      type: CityType.DELETE,
      payload: { id }
    });
    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: 'Delete successfully'
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

const deleteAllCity = () => async (dispatch) => {
  try {
    await CityService.deleteAll();
    dispatch({
      type: CityType.DELETE_ALL
    });
    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: 'Delete successfully'
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

export { createCity , updateCity, deleteCity, deleteAllCity };
