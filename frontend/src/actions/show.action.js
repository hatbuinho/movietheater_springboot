import { ShowType, MessageType } from './types';
import { handleCommonError } from './handleError';
import ShowService from '../services/show/show.service';

let message = '';


const getAllShows = () => async (dispatch) => {
  try {
    const res = await ShowService.findAll();

    dispatch({
      type: ShowType.GET_ALL,
      payload: res.data
    });

    return Promise.resolve();
  } catch (err) {
    console.log(err);
    return handleCommonError(err, dispatch);
  }
};

const findShowById = (showId) => async (dispatch) => {
  try {
    const res = await ShowService.findById(showId);
    dispatch({
      type: ShowType.GET_BY_ID,
      payload: res.data
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return handleCommonError();
  }
};

const createShow =
  (show = {}) =>
  async (dispatch) => {
    try {
      const res = await ShowService.create(show);

      dispatch({
        type: ShowType.CREATE,
        payload: res.data
      });
      message = 'Create Show successfully';
      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: { message }
      });

      return Promise.resolve();
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

const updateShow =
  (id, show = {}) =>
  async (dispatch) => {
    try {
      const res = await ShowService.update(id, show);
      dispatch({
        type: ShowType.UPDATE,
        payload: res.data
      });
      message = 'Update Show successfully';
      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: { message }
      });
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

const deleteShow = (id) => async (dispatch) => {
  try {
    await ShowService.deleteById(id);
    dispatch({
      type: ShowType.DELETE,
      payload: { showId: id }
    });
    message = 'Delete successfully';
    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: { message }
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

const deleteAllShows = () => async (dispatch) => {
  try {
    await ShowService.deleteAll();
    dispatch({
      type: ShowType.DELETE_ALL
    });
    message = 'Delete successfully';
    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: { message }
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

const findShowByMovieAndDate = (movieId, date) => async (dispatch) => {
  try {
    const res = await ShowService.findByMovieAndDate(movieId, date);
    dispatch({
      type: ShowType.FIND_BY_MOVIE_AND_DATE,
      payload: res.data
    });

    return Promise.resolve(res.data);
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

export {
  createShow,
  deleteAllShows,
  deleteShow,
  getAllShows,
  updateShow,
  findShowById,
  findShowByMovieAndDate
};
