import { AuthType, MessageType } from './types';

import AuthService from '../services/auth/auth.service';

const register = (user) => async (dispatch) => {
  try {
    const res = await AuthService.register(user);
    dispatch({
      type: AuthType.REGISTER_SUCCESS
    });

    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: res.data.message
    });

    return Promise.resolve();
  } catch (err) {
    const message =
      err.response?.data?.message || err.message || err.toString();

    dispatch({
      type: AuthType.REGISTER_FAIL
    });

    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: message
    });

    return Promise.reject();
  }
};

const login = (username, password) => async (dispatch) => {
  try {
    const res = await AuthService.login(username, password);

    dispatch({
      type: AuthType.LOGIN_SUCCESS,
      payload: res.data
    });

    return Promise.resolve();
  } catch (err) {
    let message = err.response?.data?.message || err.message || err.toString();
    console.log(message);

    if (err.response?.status === 401) {
      message = 'Username or password is wrong';
    }

    dispatch({
      type: AuthType.LOGIN_FAIL
    });

    dispatch({
      type: MessageType.SET_MESSAGE,
      payload: {message}
    });


    return Promise.reject();
  }
};

const logout = () => (dispatch) => {
  AuthService.logout();

  dispatch({
    type: AuthType.LOGOUT
  });
};

export { login, logout, register };
