import { MovieType, MessageType } from './types';

import MovieService from '../services/movie/movie.service';
import { handleCommonError } from './handleError';

let message = '';

export const createMovie =
  ({
    title,
    description,
    duration,
    releaseDate,
    genre,
    language,
    country,
    smallImage,
    largeImage
  }) =>
  async (dispatch) => {
    try {
      const res = await MovieService.create({
        title,
        description,
        duration,
        releaseDate,
        genre,
        smallImage,
        largeImage,
        language,
        country
      });

      dispatch({
        type: MovieType.CREATE_MOVIE,
        payload: res.data
      });
message = 'Created successfully'
      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: {message}
      });

      return Promise.resolve();
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

/**
   * 
   * title: '',
    genre: '',
    releaseDate: new Date().toLocaleDateString(),
    language: '',
    duration: 0,
    country: '',
    description: '',
    smallImage: null,
    largeImage: null
   */
export const updateMovie =
  (
    movieId,
    {
      title,
      description,
      duration,
      releaseDate,
      genre,
      smallImage,
      largeImage,
      language,
      country
    }
  ) =>
  async (dispatch) => {
    try {
      const res = await MovieService.update(movieId, {
        title,
        description,
        duration,
        releaseDate,
        genre,
        smallImage,
        largeImage,
        language,
        country
      });

      dispatch({
        type: MovieType.UPDATE_MOVIE,
        payload: res.data
      });

      message = 'Update successfully'

      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: {message}
      });

      return Promise.resolve();
    } catch (err) {
      return handleCommonError(err, dispatch);
    }
  };

export const getMoviesPage =
  (title = '', pageNumber = 1) =>
  async (dispatch) => {
    try {
      const res = await MovieService.getAll(title, pageNumber);

      dispatch({
        type: MovieType.GET_PAGE_MOVIE,
        payload: res.data
      });

      return Promise.resolve();
    } catch (err) {
      const message =
        err.response?.data?.message || err.message || err.toString();

      dispatch({
        type: MessageType.SET_MESSAGE,
        payload: {message}
      });
    }
  };

export const findMovieById = (movieId) => async (dispatch) => {
  try {
    const res = await MovieService.findById(movieId);
    dispatch({
      type: MovieType.FIND_BY_ID,
      payload: res.data
    });
    return Promise.resolve(res.data);
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};

export const deleteMovie = (id) => async (dispatch) => {
  try {
    await MovieService.delete(id);

    dispatch({
      type: MovieType.DELETE_MOVIE,
      payload: { movieId: id }
    });
  } catch (err) {
    return handleCommonError(err, dispatch);
  }
};
