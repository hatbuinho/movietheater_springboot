import { MessageType } from './types';

const handleCommonError = (err, dispatch, throwError = true) => {
  const message = err.response?.data?.message || err.message || err.toString();
  const status = err.response?.status;

  console.log('message', message)
  console.log('status', status)

  dispatch({
    type: MessageType.SET_MESSAGE,
    payload: { message, status }
  });
  if (throwError) return Promise.reject();
};

export { handleCommonError };
