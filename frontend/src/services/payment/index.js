import http from '../../utils/http-api';
import authHeader from '../auth/auth-header';

const findByYearAndMonth = (year, month) => {
  return http.get('/payments', {
    params: {
      year,
      month
    },
    headers: authHeader()
  });
};

const PaymentService = {
  findByYearAndMonth
};
export default PaymentService;
