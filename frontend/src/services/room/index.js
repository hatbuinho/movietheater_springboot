import http from '../../utils/http-api';
import authHeader from '../auth/auth-header';

const RoomService = {
  getAll: () => http.get('/public/rooms'),

  create: (room = {}) => {
    return http.post('/rooms', room, {
      headers: authHeader()
    });
  },

  update: (id, room = {}) => {
    return http.put(`/rooms/${id}`, room, {
      headers: authHeader()
    });
  },

  deleteById: (id) => {
    return http.delete(`/rooms/${id}`, {
      headers: authHeader()
    });
  },

  deleteAll: () => {
    return http.delete('/rooms', {
      headers: authHeader()
    });
  }
};

export default RoomService;
