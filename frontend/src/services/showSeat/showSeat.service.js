import http from '../../utils/http-api';
import authHeader from '../auth/auth-header';

const ShowSeatService = {
  findByShow: (showId) => {
    return http.get(`/shows/${showId}/seats`, {
      headers: authHeader()
    });
  }
};

export default ShowSeatService;