import http from '../../utils/http-api';
import authHeader from '../auth/auth-header';

const CinemaService = {
  // getPage: (name = '', pageNumber: 1) => {
  //   return http.get('/cinemas', {
  //     params: {
  //       query: name,
  //       pageNumber
  //     },
  //     headers: authHeader
  //   });
  // },



  create: (cinema = {}) => {
    return http.post('/cinemas', cinema, {
      headers: authHeader
    });
  },

  update: (id, cinema = {}) => {
    return http.put(`/cinemas/${id}`, cinema, {
      headers: authHeader
    });
  },

  deleteById: (id) => {
    return http.delete(`/cinemas/${id}`, {
      headers: authHeader
    });
  },

  deleteAll: () => {
    return http.delete('/cinemas', {
      headers: authHeader
    });
  }
};

export default CinemaService;
