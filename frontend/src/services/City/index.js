import http from '../../utils/http-api';
import authHeader from '../auth/auth-header';

const CityService = {
  create: (city = {}) => {
    return http.post('/cities', city, {
      headers: authHeader
    });
  },

  update: (id, city = {}) => {
    return http.put(`/cities/${id}`, city, {
      headers: authHeader
    });
  },

  deleteById: (id) => {
    return http.delete(`/cities/${id}`, {
      headers: authHeader
    });
  },

  deleteAll: () => {
    return http.delete('/cities', {
      headers: authHeader
    });
  }
};

export default CityService;
