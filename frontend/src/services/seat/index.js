import http from '../../utils/http-api';
import authHeader from '../auth/auth-header';

const SeatService = {
  getAll: () => http.get('/seats'),

  create: (seat = {}) => {
    return http.post('/seats', seat, {
      headers: authHeader()
    });
  },

  update: (id, seat = {}) => {
    return http.put(`/seats/${id}`, seat, {
      headers: authHeader()
    });
  },

  deleteById: (id) => {
    return http.delete(`/seats/${id}`, {
      headers: authHeader()
    });
  },

  deleteAll: () => {
    return http.delete('/seats', {
      headers: authHeader()
    });
  }
};

export default SeatService;
