import http from '../../utils/http-api';
import authHeader from '../auth/auth-header';

class MovieService {
  getAll(title, pageNumber) {
    return http.get('/public/movies', {
      params: {
        title,
        pageNumber
      }
    });
  }

  get(id) {
    return http.get(`/public/movies/${id}`);
  }

  create(movie = {}) {
    const formData = new FormData();

    Object.entries(movie).forEach(([key, value]) => {
      formData.append(key, value);
    });
    const auth = authHeader();
    const header = {
      ...auth
    };

    return http.post('/movies', formData, {
      headers: header
    });
  }

  update(movieId, movie) {
    const formData = new FormData();

    Object.entries(movie).forEach(([key, value]) => {
      formData.append(key, value);
    });
    return http.put(`/movies/${movieId}`, formData, {
      headers: authHeader()
    });
  }

  delete(id) {
    console.log(authHeader());
    return http.delete(`/movies/${id}`, {
      headers: authHeader()
    });
  }

  deleteAll() {
    return http.delete('/movies', {
      headers: authHeader()
    });
  }

  findById(id) {
    return http.get(`/public/movies/${id}`, {
      headers: authHeader()
    });
  }
}

export default new MovieService();
