import http from '../../utils/http-api';
import authHeader from '../auth/auth-header';

const bookingApi = '/bookings';

const book = (booking = {}) => {
  return http.post(bookingApi, booking, {
    headers: authHeader()
  });
};

const findAll = () => {
  return http.get(bookingApi, {
    headers: authHeader()
  });
};

const cancel = (bookingId) => {
  return http.post(`${bookingApi}/cancel/${bookingId}`, {
    headers: authHeader()
  });
};

const checkIn = (bookingId) => {
  return http.post(
    `bookings/${bookingId}/check-in`,
    {},
    {
      headers: authHeader()
    }
  );
};

const confirm = (payment) => {
  return http.post(`/payments`, payment, {
    headers: authHeader()
  });
};

const findUserBooking = (userId) => {
  return http.get(`/users/${userId}/bookings`,{
    headers: authHeader()
  });
};

const findByBookingCode = (bookingCode) => {
  console.log(authHeader())
  return http.get(`${bookingApi}/code`, {
    headers: authHeader(),
    params: {
      bookingCode
    }
  });
};

const filterBooking = (
  movieId,
  params = { createdDate: '', status: '', pageNumber: 0 }
) => {
  return http.get(`movies/${movieId}/bookings`, {
    params,
    headers: authHeader()
  });
};

const BookingService = {
  book,
  findAll,
  cancel,
  checkIn,
  confirm,
  findUserBooking,
  findByBookingCode,
  filterBooking
};

export default BookingService;
