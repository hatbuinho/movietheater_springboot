import http from '../../utils/http-api';
import authHeader from '../auth/auth-header';

const api = '/shows';

const ShowService = {
  findAll: () => http.get(`${api}`,{
    headers: authHeader()
  }),

  findAllByMovie: (movieId) => http.get(`/public/movies/${movieId}/shows`),

  findByMovieAndDate: (movieId, date) => {
    return http.get(`/public${api}/find`, {
      params: {
        movieId,
        date
      },
      headers: authHeader()
    });
  },

  findById: (id) => http.get(`/public${api}/${id}`),

  create: (show = {}) => {
    return http.post(api, show, {
      headers: authHeader()
    });
  },

  update: (id, show = {}) => {
    return http.put(`${api}/${id}`, show, {
      headers: authHeader()
    });
  },

  deleteById: (id) => {
    return http.delete(`${api}/${id}`, {
      headers: authHeader()
    });
  },

  deleteAll: () => {
    return http.delete(api, {
      headers: authHeader()
    });
  }
};

export default ShowService;
