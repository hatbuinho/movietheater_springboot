import http from '../../utils/http-api';
import authHeader from '../auth/auth-header';

const userApi = '/users';

const findAll = () => {
  return http.get(userApi, {
    headers: authHeader()
  });
};

const findById = (id) => {
  return http.get(`${userApi}/${id}`, {
    headers: authHeader()
  });
};

const create = (user) => {
  return http.post(userApi, user, {
    headers: authHeader()
  });
};

const update = (id, user) => {
  return http.put(`${userApi}/${id}`, user, {
    headers: authHeader()
  });
};

const deleteById = (id) => {
  return http.delete(`${userApi}/${id}`, {
    headers: authHeader()
  });
};

const UserService = {
  create,
  update,
  findAll,
  findById,
  deleteById
};

export default UserService;
