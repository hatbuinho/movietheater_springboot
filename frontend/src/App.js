import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import SaveMovie from './components/Movie/Save';
import MovieDetails from './components/Movie/Details';
import MovieManagement from './components/Movie/Manage';
import AppNavBar from './components/AppNavBar';
import SaveUser from './components/User/Save';
import HistoryListener from './utils/HistoryListener';
import Login from './components/User/Login';
import { SeatManagement, CreateSeat } from './components/Seat';
import Home from './components/Home';
import SaveShow from './components/Show/SaveShow';
import ShowDate from './components/Show/ShowDate';
import Footer from './components/footer/footer';
import ShowSeatMap from './components/Show/ShowSeatMap';
import './App.css';
import ShowManagement from './components/Show/Manage';
import Register from './components/User/Register';
import CreateUser from './components/User/Create';
import UserManagement from './components/User/Manage';
import UserDetails from './components/User/Details';
import ConfirmBooking from './components/Booking/ConfirmBooking';
import HistoryBooking from './components/Booking/HistoryBooking';
import CheckinBooking from './components/Booking/CheckinBooking';
import Statistics from './components/statistics//Statistics';
import PrintTickets from './components/Booking/PrintTickets';
import Tickets from './components/Booking/Tickets';
import Ticket from './components/Booking/Ticket';
import AccessDenied from './components/AccessDenied';

const App = () => {
  return (
    <Router>
      <HistoryListener>
        <AppNavBar />

        <Switch>
          <Route path="/" exact={true} component={Home} />
          <Route path="/movies" exact={true} component={MovieManagement} />
          <Route path="/movies/create" component={SaveMovie} />
          <Route path="/movies/update/:movieId" component={SaveMovie} />

          <Route path="/movies/:movieId" component={MovieDetails} />

          <Route path="/register" component={Register} />
          <Route path="/login" component={Login} />
          <Route path="/users" exact={true} component={UserManagement} />
          <Route path="/users/create" component={CreateUser} />
          <Route path="/users/:userId" component={UserDetails} />

          <Route path="/seats" exact={true} component={SeatManagement} />
          <Route path="/seats/create" component={CreateSeat} />

          <Route path="/shows" exact={true} component={ShowManagement} />
          <Route path="/shows/create" component={SaveShow} />
          <Route path="/shows/:showId" component={SaveShow} />

          <Route
            path="/booktickets/:movieInfo/:date"
            exact={true}
            component={ShowDate}
          />

          <Route
            path="/booktickets/:movieInfo/:date/layout-:showId"
            component={ShowSeatMap}
          />

          <Route path="/confirm-booking" component={ConfirmBooking} />

          <Route path="/myprofile/booking-history" component={HistoryBooking} />

          <Route path="/check-in" component={CheckinBooking} />

          <Route path="/statistics" component={Statistics} />
          {/* <Route path="/print" component={PrintTickets} /> */}

          <Route path="/un-authorized" component={AccessDenied} />

          <Route path="*">
            <div>Page 404</div>
          </Route>
        </Switch>
        <Route component={Footer} />
        <Route path="/ticket" component={Ticket} />

      </HistoryListener>
    </Router>
  );
};

export default App;
