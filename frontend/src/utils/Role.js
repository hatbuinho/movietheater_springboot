const ADMIN = 'ROLE_ADMIN';
const EMPLOYEE = 'ROLE_EMPLOYEE';

const Role = {
  ADMIN,
  EMPLOYEE
};

export default Role;
