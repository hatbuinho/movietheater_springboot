const getDateString = (date = new Date()) => {
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const twoDigitsMonth = month > 10 ? month : `0${month}`;
  const twoDigitsDate = date > 10 ? day : `0${day}`;

  return `${year}-${twoDigitsMonth}-${twoDigitsDate}`;
};

const monthIndexs = new Array(12).fill().map((_, index) => index + 1);
const getMonthFromDateString = (dateString) => dateString.split`-`[1];
const getLiteralMonthString = (monthIndex) =>
  new Date(`-${monthIndex}-`).toDateString().split` `[1];

const MonthStringArray = monthIndexs.map((index) =>
  getLiteralMonthString(index)
);

const getMonth = (dateString) =>
  getLiteralMonthString(getMonthFromDateString(dateString));

const getYear = (dateString) => dateString.split`-`[0];

const DateTimeUtils = {
  getDateString,
  MonthStringArray,
  getLiteralMonthString,
  getMonthFromDateString,
  getMonth,
  getYear
};
export default DateTimeUtils;
