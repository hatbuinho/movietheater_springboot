import { isEmail} from 'validator';

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger text-center" role="alert">
        This field is required!
      </div>
    );
  }
};
const validUsername = (value) => {
  if (value.length < 3 || value.length > 20) {
    return (
      <div className="alert alert-danger text-center" role="alert">
        The username must be between 3 and 20 characters.
      </div>
    );
  }
};

const validPassword = (value) => {
  if (value.length < 6 || value.length > 40) {
    return (
      <div className="alert alert-danger text-center" role="alert">
        The password must be between 6 and 40 characters.
      </div>
    );
  }
};

const validEmail = (value) => {
  if (!isEmail(value)) {
    return (
      <div className="alert alert-danger text-center" role="alert">
        This is not a valid email.
      </div>
    );
  }
};

const validPhone = (value) => {
  if (!value.match(/^0\d{9}$/)) {
    return (
      <div className="alert alert-danger text-center" role="alert">
        Phone must be start with 0 and follow by 9 digits.
      </div>
    );
  }
};

const isPositive = (value) => {
  if (value < 1) {
    return (
      <div className="alert alert-danger text-center" role="alert">
        This field must be a positive integer
      </div>
    );
  }
};

const Validation = {
  required,
  validEmail,
  validPassword,
  validPhone,
  validUsername,
  isPositive
};

export default Validation;
