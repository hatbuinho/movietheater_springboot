const insertDot = (money = '') => {
  const arr = money.split``.reverse();
  return arr.map((e, i) => (i > 0 && i % 3 === 0 ? ' ' + e : e)).join``
    .split``.reverse().join``;
};

const getDateString = (date = new Date()) =>  date.toISOString().split`T`[0];


const Convertor = {
  insertDot,
  getDateString
};


export default Convertor;
