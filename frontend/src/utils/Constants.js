const Mobile = { name: 'Mobile', value: 'MOBILE' };
const CreditCard = { name: 'Credit Card', value: 'CREDIT_CARD' };
const PaymentMethod = {
  Mobile,
  CreditCard
};
const PaymentMethodList = [Mobile, CreditCard];
const WHOLE_PAGE = '0';
const ALL_MOVIE = ' ';

export { PaymentMethod, PaymentMethodList, WHOLE_PAGE, ALL_MOVIE };
