const CLASSIC = 'CLASSIC';
const ROYAL = 'ROYAL';
const RESERVED = 'RESERVED';
const AVAILABLE = 'AVAILABLE';

const Seat = {
  Type: { CLASSIC, ROYAL },
  Status: { RESERVED, AVAILABLE }
};
export default Seat;
