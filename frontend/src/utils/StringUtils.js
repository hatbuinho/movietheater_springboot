const getGoBackUrl = (currentUrl) =>
  currentUrl.replace(/\/$/, '').split`/`.slice(0, -1).join`/`;

const StringUtils = {
  getGoBackUrl
};

export default StringUtils;
