import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { clearMessage } from '../actions/message.action';
import { useDispatch } from 'react-redux';
import { logout } from '../actions/auth.action';

const HistoryListener = ({ children }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  const user = JSON.parse(localStorage.getItem('user'));

  useEffect(() => {
    return history.listen((location) => {
      if (user) {
        console.log('check token');
        const { token } = user;
        const tokenBody = token.split`.`[1];
        const { exp } = JSON.parse(window.atob(tokenBody));
        if (exp * 1000 < new Date().getTime()) {
          dispatch(logout());
          history.go(0);
          history.push('/login');
          history.go(0);
        }
      }

      dispatch(clearMessage());
    });
  }, [history, dispatch, user]);

  return children;
};

export default HistoryListener;
