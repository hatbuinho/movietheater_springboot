import { useState } from 'react';
import { Button, Modal } from 'react-bootstrap';

function Confirm({
  children,
  title,
  action,
  confirmName,
  buttonName,
  value,
  variant = 'primary',
  isLoading
}) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleConfirm = (e) => {
    action(value);
  };

  return (
    <>
      <Button value={value} variant={variant} onClick={handleShow}>
        {buttonName}
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{children}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleConfirm}>
            {isLoading && (
              <span className="spinner-border spinner-border-sm"></span>
            )}{' '}
            {confirmName}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Confirm;
