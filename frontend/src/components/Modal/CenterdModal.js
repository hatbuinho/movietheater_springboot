import { Modal } from 'react-bootstrap';
import './modal.css';

const CenterdModal = (props) => {
  return (
    <Modal {...props} centered className="hello" onHide={props.onHide}>
      <Modal.Body>{props.children}</Modal.Body>
    </Modal>
  );
};

export default CenterdModal;
