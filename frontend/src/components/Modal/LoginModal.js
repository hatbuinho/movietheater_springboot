import LoginForm from '../User/LoginForm';
import CenterdModal from './CenterdModal';

const LoginModal = ({ forwardTo, show, className, onHide, ...rest }) => {
  return (
    <CenterdModal {...rest} show={show} onHide={onHide}>
      <LoginForm fowardTo={forwardTo} className={className} />
    </CenterdModal>
  );
};

export default LoginModal;
