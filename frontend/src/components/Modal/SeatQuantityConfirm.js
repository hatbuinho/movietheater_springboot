import { nanoid } from 'nanoid';
import CenterdModal from './CenterdModal';
import { Button } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import Convertor from '../../utils/Convertor';

const SeatQuantityConfirm = ({ maxQuantity = 10, onShow, onHide ,onChange, ...rest }) => {
  const numbers = new Array(maxQuantity).fill().map((_, i) => i + 1);
  const [show] = useSelector((state) => state.shows);

  const handleClick = (number) => (e) => {
      onChange(number);
  };
  return (
    <CenterdModal {...rest} className="d-flex flex-column" show={onShow}>
      {/* title */}
      <div className="text-center">How Many Seats?</div>

      {/* number */}
      <div className="d-flex justify-content-evenly my-3">
        {numbers.map((number) => {
          let key = nanoid();
          return (
            <button
              key={key}
              className={`fs-6  btn rounded-circle mx-1 ${
                number === rest.quantity ? 'btn-danger' : ''
              }`}
              onClick={handleClick(number)}
            >
              {number}
            </button>
          );
        })}
      </div>

      {/* Price */}
      <div className="d-flex justify-content-evenly mt-4 mb-2">
        <div>
          <Button variant="success">Classic</Button>
          <div className="price">
            {`${Convertor.insertDot(show?.listPrice + '')} VND`}
          </div>
        </div>
        <div>
          <Button variant="warning">Royal</Button>
          <div className="price">
            {`${Convertor.insertDot(show?.listPrice * 1.5 + '')} VND`}
          </div>
        </div>
      </div>

      <div className="d-flex">
        <button
          className="btn btn-danger rounded-3 text-center flex-fill"
          onClick={onHide}
        >
          Select
        </button>
      </div>
    </CenterdModal>
  );
};

export default SeatQuantityConfirm;
