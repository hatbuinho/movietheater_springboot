import React from 'react';

const Logo = ({className}) => {
  return (
    <div>
      <span className={` rounded-pill bg-dark p-2  ${className}`}>
        <a href="/" className="text-white text-decoration-none" >Book <span className="badge fs-7 bg-danger fst-italic">My</span> Show</a>
      </span>
    </div>
  );
};

export default Logo;
