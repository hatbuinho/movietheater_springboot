import { Row, Table } from 'react-bootstrap';
import './room.css';
import ScreenImage from '../../resources/images/screen.png';
import { nanoid } from 'nanoid';
import Seat from '../../utils/Seat';

const SeatMap = ({ room = {}, seats, SeatType }) => {
  const layout = new Array(room.rowSize)
    .fill()
    .map((row) => new Array(room.colSize).fill(0));

  seats?.forEach((seat) => {
    if (seat) {
      const row = seat.row - 1;
      const col = seat.col;
      layout[row] = layout[row] || [];
      layout[row][col] = seat;
    }
  });

  return (
    <>
      <div className="container postiton-relative" >
        <table className="mx-auto">
          <tbody>
            {layout.map((row, rowIndex) => {
              let seatIndex = 1;
              let rowId = nanoid();
              return (
                <tr key={rowId}>
                  <div className="d-flex">
                    {row.map((seat, index) => {
                      let seatId = nanoid();
                      if (index === 0) {
                        return (
                          <div
                            className="SeatI m-2 align-middle postiton-absolute end-100"
                            style={{ transform: 'translateX(-100%' }}
                            key={seatId}
                          >
                            {String.fromCodePoint(65 + rowIndex)}
                          </div>
                        );
                      }

                      if (seat && typeof seat === 'object') {
                        let key = nanoid();
                        const isReseved = seat.status === Seat.Status.RESERVED;
                        return (
                          <SeatType
                            seat={seat}
                            key={key}
                            seatIndex={seatIndex++}
                            className={`seatI m-2 ${
                              isReseved ? 'btn-secondary' : ''
                            }`}
                            variant={
                              isReseved
                                ? ''
                                : seat.type === Seat.Type.ROYAL
                                ? 'warning'
                                : 'success'
                            }
                            disabled={isReseved ? true : false}
                          />
                        );
                      }
                      return <div className="SeatI m-2" key={seatId}></div>;
                    })}
                  </div>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <div className="Screeen">
        <div className="Screen d-flex justify-content-center mt-3">
          <img
            src={ScreenImage}
            alt="screen"
            className="img-fluid"
            style={{ maxWidth: '30%', marginLeft: '5%' }}
          />
        </div>
        <div className="text-center ms-5 mb-3">All eyes this way please!</div>
      </div>
    </>
  );
};

export default SeatMap;
