import { useDispatch, useSelector } from 'react-redux';
import { clearSelectedSeat, selectSeat } from '../../actions/booking.action';
import { Button } from 'react-bootstrap';

const CustomerSeat = ({ className, seatIndex, seat, variant, disabled }) => {
  const dispatch = useDispatch();
  const { selectedSeat, quantity } = useSelector((state) => state.preBooking);
  const handleClick = (seat) => (e) => {
    dispatch(selectSeat(seat));

    if (selectedSeat.length >= quantity) {
      dispatch(clearSelectedSeat());
      dispatch(selectSeat(seat));
    }
  };

  return (
    <Button
      variant={`${
        selectedSeat.find((e) => e.showSeatId === seat.showSeatId)
          ? variant
          : `outline-${variant}`
      }`}
      onClick={handleClick(seat)}
      className={className}
      disabled={disabled}
    >
      {seatIndex}
    </Button>
  );
};

export default CustomerSeat;
