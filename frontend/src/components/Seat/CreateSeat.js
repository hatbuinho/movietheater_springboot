import SaveSeat from './SaveSeat';

const CreateSeat = () => {
  return (
    <div className="container py-3" style={{minHeight: '95vh'}}>
      <h1>Create Seat</h1>
      <hr />
      <SaveSeat />
    </div>
  );
};

export default CreateSeat;
