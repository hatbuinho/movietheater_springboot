import { OverlayTrigger, Popover, Button } from 'react-bootstrap';
import SaveSeat from './SaveSeat';

const AdminSeat = ({ seat, className, seatIndex, variant }) => {
  return (
    <OverlayTrigger
      trigger="click"
      placement={'top'}
      overlay={
        <Popover>
          <Popover.Header as="h3">{'Update seat'}</Popover.Header>
          <Popover.Body className="overflow-auto" style={{ height: '150px' }}>
            <SaveSeat Seat={seat} />
          </Popover.Body>
        </Popover>
      }
    >
      <Button variant={`outline-${variant}`} className={className}>
        {seatIndex}
      </Button>
    </OverlayTrigger>
  );
};

export default AdminSeat;
