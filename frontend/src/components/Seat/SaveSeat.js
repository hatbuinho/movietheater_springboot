import React, { useState, useRef } from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import CheckButton from 'react-validation/build/button';
import Select from 'react-validation/build/select';

import Validation from '../../utils/Validation';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import Type from './type';
import { Button } from 'react-bootstrap';
import { createSeat, updateSeat } from '../../actions/seat.action';

const SaveSeat = ({ Seat, className, isUpdate }) => {
  const rooms = useSelector((state) => state.rooms);
  const checkBtn = useRef();
  const form = useRef();
  const history = useHistory();
  const dispatch = useDispatch();
  const [isAdding, setIsAdding] = useState(false);

  const room = rooms[0];

  const initSeat = Seat || {
    row: '',
    col: '',
    type: Type.CLASSIC
  };
  const [seat, setSeat] = useState(initSeat);

  const [isSuccess, setIsSuccess] = useState(true);
  const { message } = useSelector((state) => state.message);

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    setSeat({
      ...seat,
      [name]: value
    });
  };

  const submit = (seat) =>
    Seat ? dispatch(updateSeat(Seat.seatId, seat)) : dispatch(createSeat(seat));

  const handleSubmit = (e) => {
    e.preventDefault();
    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      setIsAdding(true);

      // dispatch(createSeat({ ...seat, roomId: room.roomId }))
      submit({ ...seat, roomId: room.roomId })
        .then(() => {
          setIsAdding(false);
          setTimeout(() => {
            history.go(0);
          }, 0);
        })
        .catch(() => {
          setIsSuccess(false);
          setIsAdding(false);
        });
    }
  };

  return (
    
    <Form ref={form} onSubmit={handleSubmit}>

      <div className={className}>
        {/* row */}
        <div className="me-3 mb-3">
          <label htmlFor="rowcol" className="form-label">
            Row
          </label>
          <Input
            name="row"
            type="number"
            min="1"
            max={room?.rowSize}
            className="form-control"
            id="row"
            onChange={handleInputChange}
            value={seat?.row}
            validations={[Validation.required, Validation.isPositive]}
          />
          <span className="badge bg-danger m-2">Max row: {room?.rowSize}</span>
        </div>

        {/* column */}
        <div className="me-3 mb-3">
          <label htmlFor="col" className="form-label">
            Column
          </label>
          <Input
            name="col"
            type="number"
            min="1"
            max={room?.colSize}
            className="form-control"
            id="col"
            onChange={handleInputChange}
            value={seat?.col}
            validations={[Validation.required, Validation.isPositive]}
          />
          <span className="badge bg-danger m-2">
            Max column: {room?.colSize}
          </span>
        </div>

        {/* Seat Type */}
        <div className="me-3 mb-3">
          <label htmlFor="col" className="form-label">
            Seat type
          </label>
          <Select
            name="type"
            className="form-select mb-3 mb-md-0"
            onChange={handleInputChange}
            value={seat?.type}
          >
            <option value="CLASSIC">Classic</option>
            <option value="ROYAL" defaultValue>
              Royal
            </option>
          </Select>
        </div>

        {/* action button */}
        <div className="my-auto">
          <Button type="submit">
            {isAdding && (
              <span className="spinner-border spinner-border-sm"></span>
            )}{' '}
            {Seat ? 'Update' : 'Add Seat'}
          </Button>
        </div>
      </div>

      {message && !isUpdate && (
        <div
          className={` ${
            isSuccess ? 'alert alert-success' : 'alert alert-danger'
          }`}
          role="alert"
        >
          {message}
        </div>
      )}

      <CheckButton style={{ display: 'none' }} ref={checkBtn} />
    </Form>

  );
};

export default SaveSeat;
