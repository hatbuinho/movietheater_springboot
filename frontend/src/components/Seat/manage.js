import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Container, Button, FormControl, InputGroup } from 'react-bootstrap';
import { Link, useHistory } from 'react-router-dom';
import Loading from '../FullPageLoading/Loading';
import './room.css';
import { getAllRooms, updateRoom } from '../../actions/room.action';
import AdminSeat from './AdminSeat';
import SeatMap from './SeatMap';
import SaveSeat from './SaveSeat';

const SeatManagement = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const rooms = useSelector((state) => state.rooms);
  const [isUpdating, setIsUpdating] = useState(false);
  const [isUpdate, setIsUpdate] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(true);
  const { message } = useSelector((state) => state.message);

  console.log('render');

  // fetch all seats
  useEffect(() => {
    setIsLoading(true);
    dispatch(getAllRooms())
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
        }, 300);
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [dispatch]);

  const [room, setRoom] = useState([]);

  useEffect(() => {
    setRoom({
      ...rooms[0]
    });
  }, [rooms]);

  const seats = room.seats;

  const handleInput = (e) => {
    const { value, name } = e.target;
    setRoom({
      ...room,
      [name]: value
    });
  };

  const handleUpdateRoom = (e) => {
    setIsUpdating(true);
    setIsUpdate(true);
    dispatch(updateRoom(room.roomId, room))
      .then(() => {
        setIsUpdating(false);
        setTimeout(() => {
          history.go(0);
        }, 1500);
      })
      .catch(() => {
        setIsSuccess(false);
        setIsUpdating(false);
      });
  };

  return (
    <div className="position-relative">
      <Container className="position-relative">
        <h1 className="my-3">Seat management</h1>
        <hr />
        <div className="row">
          <div className="col">
            {/* row size */}
            <InputGroup className="flex-nowrap mb-2">
              <InputGroup.Text id="rowSize">Row size</InputGroup.Text>
              <FormControl
                name="rowSize"
                type="number"
                min="1"
                onChange={handleInput}
                value={room?.rowSize}
              />
            </InputGroup>

            {/* col size */}
            <InputGroup className="flex-nowrap">
              <InputGroup.Text>Column size</InputGroup.Text>
              <FormControl
                name="colSize"
                type="number"
                min="1"
                onChange={handleInput}
                value={room?.colSize}
              />
            </InputGroup>
            <button
              type="button"
              className="btn btn-primary my-2"
              onClick={handleUpdateRoom}
            >
              {isUpdating && (
                <span className="spinner-border spinner-border-sm"></span>
              )}{' '}
              Update size
            </button>
            {message && isUpdate && (
              <div
                className={` ${
                  isSuccess ? 'alert alert-success' : 'alert alert-danger'
                }`}
                role="alert"
              >
                {message}
              </div>
            )}
          </div>

          <div className="col">
            <SaveSeat className="d-flex" isUpdate={isUpdate} />
            {/* <button
              type="button"
              className="btn btn-primary me-2 mb-2"
              onClick={handleUpdateRoom}
            >
              {isUpdating && (
                <span className="spinner-border spinner-border-sm"></span>
              )}{' '}
              Update size
            </button> */}
            {/* <Button as={Link} to="/seats/create" className="mb-2 me-2">
              Add seat
            </Button> */}
          </div>
        </div>

        <SeatMap room={room} seats={seats} SeatType={AdminSeat} />
      </Container>

      {isLoading && <Loading />}
    </div>
  );
};

export default SeatManagement;
