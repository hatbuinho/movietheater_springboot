import Logo from '../Logo';
import './footer.css';

const Footer = () => {
  return (
    <div className="container-fluid bg-footer">
      <div className="d-flex justify-content-center align-items-center w-100 py-3">
        <div className="bg-secondary footer-line"></div>
        <Logo />
        <div className="bg-secondary footer-line"></div>
      </div>

      <div className="contact d-flex justify-content-center pb-3">
        <a href="/" className="btn btn-secondary rounded-circle mx-2">
          <i className="fab fa-facebook"></i>
        </a>
        <a href="/" className="btn btn-secondary rounded-circle mx-2">
          <i className="fab fa-twitter-square"></i>
        </a>
        <a href="/" className="btn btn-secondary rounded-circle mx-2">
          <i className="fab fa-instagram"></i>
        </a>
        <a href="/" className="btn btn-secondary rounded-circle mx-2">
          <i className="fab fa-youtube"></i>
        </a>
      </div>
    </div>
  );
};

export default Footer;
