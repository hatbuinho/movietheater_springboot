import { useSelector } from 'react-redux';

const AccessDenied = () => {
  const { message } = useSelector((state) => state.message);
  return (<div style={{minHeight: '95vh'}}>
   { message && (
      <div className={'text-center alert alert-danger'} role="alert">
        {message}
      </div>
    )}
  </div>
    
  );
};

export default AccessDenied;
