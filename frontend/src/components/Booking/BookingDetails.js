import { useState } from 'react';
import { FormControl } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { checkinBooking } from '../../actions/booking.action';
import { clearMessage, setMessage } from '../../actions/message.action';
import Role from '../../utils/Role';
import CenterdModal from '../Modal/CenterdModal';
import { BookingStatus } from './BookingStatus';
import PrintTickets from './PrintTickets';
import Tickets from './Tickets';

const BookingDetails = ({ show, booking, ...rest }) => {
  const dispatch = useDispatch();
  const [bookingCode, setBookingCode] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const { message } = useSelector((state) => state.message);
  const {
    user: { roles }
  } = useSelector((state) => state.auth);
  const isEmployee = roles.includes(Role.EMPLOYEE);
  const {
    user: {
      contact: { fullName },
      username
    },
    show: { movie, date, startTime },
    bookingId,
    status,
    showSeats: tickets
  } = booking;

  const handleInputChange = (e) => {
    const { value } = e.target;
    setBookingCode(value);
  };

  const handleCheckIn = () => {
    setIsLoading(true);
    if (bookingCode === booking.bookingCode) {
      setIsSuccess(true);
      return;

      dispatch(checkinBooking(bookingId))
        .then(() => {
          setIsSuccess(true);
        })
        .catch(() => {
          setIsSuccess(false);
          setIsLoading(false);
        });
    } else {
      setIsSuccess(false);
      setIsLoading(false);
      dispatch(setMessage('Booking code not match'));
      setTimeout(() => {
        dispatch(clearMessage());
      }, 2000);
    }
  };
  return (
    <CenterdModal show={show} {...rest}>
      <ul className="list-group mb-2">
        <li className="list-group-item">
          <span className="badge bg-success fs-4 me-2 mb-2">{movie.title}</span>
          <div>
            <span className="badge bg-secondary fs-6 me-2 mb-2">{date}</span>
            <span className="badge bg-secondary fs-6">{startTime}</span>
          </div>
        </li>
        <li className="list-group-item">
          <span className="badge bg-secondary me-2">User:</span>
          {`${fullName} (${username})`}
        </li>
        {/* show seat details */}
        <li className="list-group-item">
          {tickets.map((ticket) => {
            const { seat } = ticket;
            const seatName = String.fromCodePoint(seat.row + 64) + seat.col;
            return (
              <span
                key={ticket.showSeatId}
                className="badge bg-primary me-1 fs-5"
              >
                {seatName}
              </span>
            );
          })}
        </li>
      </ul>

      {/* message */}
      {message && (
        <div
          className={
            'text-center position-fixed top-0 end-0 m-4 ' +
            (isSuccess ? 'alert alert-success' : 'alert alert-danger')
          }
          role="alert"
        >
          {message}
        </div>
      )}

      {isEmployee && status === BookingStatus.CONFIRMED && (
        <>
          {/* action button */}
          <div className="d-flex mt-2">
            <FormControl
              onChange={handleInputChange}
              placeholder="Enter booking code"
            />
            <button
              className="btn btn-danger rounded-3 text-center flex-shrink-0 ms-2"
              onClick={handleCheckIn}
            >
              {isLoading && (
                <span className="spinner-border spinner-border-sm"></span>
              )}{' '}
              Check-in
            </button>
          </div>

          {/* printTicket */}
          <div className="d-flex justify-content-center d-none">
            <PrintTickets
              booking={booking}
              handleCheckIn={handleCheckIn}
              isSuccess={isSuccess}
            />
          </div>
        </>
      )}
    </CenterdModal>
  );
};

export default BookingDetails;
