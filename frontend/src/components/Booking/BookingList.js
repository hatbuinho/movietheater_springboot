import { BookingStatus } from './BookingStatus';
import { Button, Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { useState } from 'react';
import BookingDetails from './BookingDetails';
import Role from '../../utils/Role';

const BookingList = ({ bookings }) => {
  const { user: currentUser } = useSelector((state) => state.auth);

  const [booking, setBooking] = useState();
  const [showDetails, setShowDetails] = useState(false);

  const showCheckIn = (booking) => (e) => {
    setBooking(booking);
    setShowDetails(true);
  };

  const { roles } = currentUser;
  const isEmployee = roles?.includes(Role.EMPLOYEE);

  return (
    <>
      <Table striped bordered hover className="text-center rounded">
        <thead className="bg-dark text-white">
          <tr>
            <td>Movie</td>
            <td>BookingCode</td>
            <td>Number Of Seat</td>

            <td>CreatedDate</td>
            <td>Status</td>
            <td>Actions</td>
          </tr>
        </thead>
        <tbody className="align-middle">
          {bookings?.length ? (
            bookings?.map((booking) => {
              const {
                status,
                show: { movie },
                numberOfSeat,
                createdDate,
                user
              } = booking;

              const checkable = status === BookingStatus.CONFIRMED;

              return (
                <tr key={booking.bookingId}>
                  <td>
                    <span className="badge fs-6 bg-success ">
                      {movie?.title}
                    </span>
                  </td>
                  {currentUser.id === user.userId ? (
                    <td>
                      <span className="badge fs-6  bg-primary align-middle">
                        {booking.bookingCode}
                      </span>
                    </td>
                  ) : (
                    <td>
                      <span className="badge bg-secondary">Hidden</span>
                    </td>
                  )}

                  <td>{numberOfSeat}</td>
                  <td>{createdDate}</td>
                  <td>
                    <span
                      className={`badge align-middle  bg-${
                        status === BookingStatus.CONFIRMED
                          ? 'primary'
                          : status === BookingStatus.CHECKED_IN
                          ? 'success'
                          : 'secondary'
                      } `}
                    >
                      {status.replace('_', ' ')}
                    </span>
                  </td>
                  <td className="align-middle text-nowrap">
                    {isEmployee ? (
                      checkable ? (
                        <Button
                          onClick={showCheckIn(booking)}
                          variant="danger"
                          className="me-2"
                        >
                          Check-in
                        </Button>
                      ) : (
                        <Button
                          onClick={showCheckIn(booking)}
                          variant="info"
                          className="me-2"
                        >
                          View
                        </Button>
                      )
                    ) : (
                      <Button
                        onClick={showCheckIn(booking)}
                        variant="info"
                        className="me-2"
                      >
                        View
                      </Button>
                    )}
                  </td>
                </tr>
              );
            })
          ) : (
            <tr className="text-center text-dark">
              <td colSpan={5}>No booking be found</td>
            </tr>
          )}
        </tbody>
      </Table>

      {/* booking details */}
      {booking && (
        <BookingDetails
          show={showDetails}
          booking={booking}
          onHide={() => setShowDetails(false)}
        />
      )}
    </>
  );
};

export default BookingList;
