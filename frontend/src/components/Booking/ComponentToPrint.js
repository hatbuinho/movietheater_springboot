import { PureComponent } from 'react';

export default class ComponentToPrint extends PureComponent {
  render() {
    return <div>{this.props.children}</div>;
  }
}
