import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import { Redirect } from 'react-router';
import { Link } from 'react-router-dom';
import { findBookingByUser } from '../../actions/booking.action';
import Loading from '../FullPageLoading/Loading';
import { Button, Table } from 'react-bootstrap';
import BookingList from './BookingList';

const HistoryBooking = () => {
  const dispatch = useDispatch();
  const bookings = useSelector((state) => state.bookings);

  const { user, isLoggedIn } = useSelector((state) => state.auth);
  const [isLoading, setIsLoading] = useState(true);
  const [myBookings, setMyBookings] = useState(bookings);

  useEffect(() => {
    if (!isLoggedIn) return <Redirect to="/" />;
    dispatch(findBookingByUser(user.id))
      .then(() => {
        setTimeout(() => {
          setIsLoading(false);
        }, 1000);
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [dispatch, user, isLoggedIn]);

  useEffect(() => {
    setMyBookings(bookings);
  }, [bookings]);

  return (
    <div
      className="container-fluid container-md"
      style={{ minHeight: ' 75vh' }}
    >
      <div className="py-3">
        <h1 className="">History booking</h1>
        <hr />

        <div>
          <BookingList bookings={myBookings} />
        </div>

        {isLoading && <Loading />}
      </div>
    </div>
  );
};

export default HistoryBooking;
