import { useEffect, useState } from 'react';
import { nanoid } from 'nanoid';
import { useSelector } from 'react-redux';
import MovieCard from '../Movie/MovieCard';
import { Button, ButtonGroup, ToggleButton } from 'react-bootstrap';
import Convertor from '../../utils/Convertor';
import { PaymentMethodList, PaymentMethod } from '../../utils/Constants';
import { useDispatch } from 'react-redux';
import { confirmBooking } from '../../actions/booking.action';
import { Redirect, useHistory } from 'react-router';
import Type from '../Seat/type';

const seatByType = {};

const ConfirmBooking = () => {
  const { selectedSeat, quantity } = useSelector((state) => state.preBooking);

  const [show] = useSelector((state) => state.shows);
  const booking = useSelector((state) => state.booking);
  const { user } = useSelector((state) => state.auth);
  const { message } = useSelector((state) => state.message);

  const dispatch = useDispatch();

  const history = useHistory();

  useEffect(() => {
  

    const seatWithCharName = selectedSeat.map((seat) => {
      const name = `${String.fromCodePoint(64 + +seat.row)}${seat.col}`;
      return { ...seat, name };
    });

    seatWithCharName.forEach((seat) => {
      if (seatByType[seat.type]) seatByType[seat.type].push(seat);
      else seatByType[seat.type] = [seat];
    });

    setTicketsByType(seatByType);
  }, [selectedSeat]);

  const toTotal = (seats) => {
    return seats
      .map((e) => e.listPrice)
      .reduce((total, price) => +total + +price, 0)
      .toFixed(0);
  };

  const amount = toTotal(selectedSeat);

  const initialPayment = {
    amount,
    paymentMethod: PaymentMethod.CreditCard.value,
    bookingId: booking.bookingId
  };

  const [isConfirming, setIsConfirming] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const [ticketsByType, setTicketsByType] = useState({});
  const [payment, setPayment] = useState(initialPayment);

  if (!selectedSeat.length) return <Redirect to="/" />;

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setPayment({
      ...payment,
      [name]: value
    });
  };

  /* handle confirm */
  const handleConfirm = (e) => {
    setIsConfirming(true);
    dispatch(confirmBooking(payment))
      .then(() => {
        setIsSuccess(true);
        setTimeout(() => {
          setIsConfirming(false);
          history.replace('/myprofile/booking-history');
          history.go(0);
        }, 2000);
      })
      .catch(() => {
        setIsSuccess(false);
        setIsConfirming(false);
      });
  };

  return (
    <div
      className="container-fluid container-md"
      style={{ minHeight: ' 95vh' }}
    >
      <h1 className="mt-2">Booking summary</h1>
      <hr />

      <div className="booking-summary row ">
        {/* Film info */}
        <div className="col col-md-4 col-lg-3">
          <MovieCard movie={show.movie} />
        </div>

        {/* booking summary */}
        <div className="col col-md-8 col-lg-9">
          <ul className="list-group">
            <li className="list-group-item d-flex">
              <div>{`${user?.fullName} (${user?.username})`}</div>
              <div></div>
            </li>
            {Object.keys(ticketsByType).map((type) => {
              let key = nanoid();
              const ticketType = ticketsByType[type];
              const numberOfType = ticketType.length;
              return (
                <li key={key} className="list-group-item ">
                  <div className="d-flex justify-content-between">
                    {/* seat by type */}
                    <div className="d-flex">
                      <div className="pe-2">
                        <span
                          className={`badge bg-${
                            type === Type.ROYAL ? 'warning' : 'success'
                          }`}
                        >
                          {type}
                        </span>

                        {/* price with quantity */}
                        <div>{`(${numberOfType} x ${Convertor.insertDot(
                          `${ticketType[0]?.listPrice}`
                        )})`}</div>
                      </div>
                      <div>
                        {/* seat position */}
                        <span className="me-2">
                          {ticketType.map((seat) => (
                            <span
                              key={seat.seatId}
                              className="badge bg-secondary me-1"
                            >
                              {seat.name}
                            </span>
                          ))}
                        </span>
                      </div>
                    </div>

                    {/* sub-total */}
                    <span className="text-nowrap">
                      {`${Convertor.insertDot(
                        toTotal(ticketsByType[type])
                      )} VND`}
                    </span>
                  </div>
                </li>
              );
            })}
            <hr />
            {/* Total */}
            <li className="list-group-item d-flex justify-content-between">
              <span>
                {`Total (${quantity} ${quantity > 1 ? 'tickets' : 'ticket'})`}{' '}
              </span>
              <span>{`${Convertor.insertDot(amount)} VND`}</span>
            </li>
          </ul>

          {/* Payment method */}
          <div className="d-flex flex-column border rounded mt-4 p-4 p-lg-3  bg-light">
            <label htmlFor="dateOfBirth" className="form-label">
              Payment medthod
            </label>
            <ButtonGroup>
              {PaymentMethodList.map((pm, idx) => (
                <ToggleButton
                  key={pm.value}
                  id={`radio-${idx}`}
                  type="radio"
                  variant="outline-primary"
                  name="paymentMethod"
                  value={pm.value}
                  checked={payment.paymentMethod === pm.value}
                  onChange={handleInputChange}
                >
                  {pm.name}
                </ToggleButton>
              ))}
            </ButtonGroup>
            <div className="mt-3">
              <div className="row row-cols-lg-2">
                <div>
                  <div className="input-group input-group-sm mb-3">
                    <span className="input-group-text">Card number</span>
                    <input type="text" className="form-control" name />
                  </div>
                </div>
                <div>
                  <div className="input-group input-group-sm mb-3">
                    <span className="input-group-text">Name on card</span>
                    <input type="text" className="form-control" name />
                  </div>
                </div>
              </div>
              <div className="mt-2 row row-cols-lg-2">
                <div>
                  <div className="input-group input-group-sm mb-3">
                    <span className="input-group-text">MM/YY</span>
                    <input type="text" className="form-control" name />
                  </div>
                </div>
                <div>
                  <div className="input-group input-group-sm mb-3">
                    <span className="input-group-text">CVV</span>
                    <input type="text" className="form-control" name />
                  </div>
                </div>
              </div>
            </div>

            {/* message */}
            {message && (
              <div
                className={
                  'text-center ' +
                  (isSuccess ? 'alert alert-success' : 'alert alert-danger')
                }
                role="alert"
              >
                {message}
              </div>
            )}

            {/* action button */}
            <div className=" mt-3">
              <Button onClick={handleConfirm}>
                {isConfirming && (
                  <span className="spinner-border spinner-border-sm"></span>
                )}{' '}
                Checkout
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ConfirmBooking;
