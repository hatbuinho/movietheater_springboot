import { useEffect, useState } from 'react';
import useSearch from '../hooks/useSearch';

const searchBookingApi = (movieId) => `/movies/${movieId}/bookings`;

const useBookingFilter = (
  params = { movieId: '', pageNumber: 1, status: '' }
) => {
  const [bookings, setBookings] = useState();

  const { result, isSearching, error } = useSearch(
    params,
    'NO_NEED_ACTION',
    searchBookingApi(params.movieId)
  );

  useEffect(()=>{
    setBookings(result)
  },[result])

  console.log(bookings)

  return { bookings, isSearching, error };
};

export default useBookingFilter;
