import { useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import Loading from '../FullPageLoading/Loading';
import BookingDetails from './BookingDetails';
import { BookingStatus, BookingStatusList } from './BookingStatus';
import useBookingFilter from './useBookingFilter';
import useSearchBookingByCode from './useSearchBookingByCode';
import { nanoid } from 'nanoid';
import BookingList from './BookingList';
import DateTimeUtils from '../../utils/DateTimeUtils';
import Role from '../../utils/Role';
import useMyHistory from '../hooks/useMyHistory';

const CheckinBooking = () => {
  const searchResultBox = useRef();
  const { message } = useSelector((state) => state.message);
  const [booking, setBooking] = useState();
  const [showDetails, setShowDetails] = useState(false);

  // FILTER BOOKING
  const initialParams = {
    pageNumber: 1,
    status: BookingStatus.CONFIRMED,
    movieId: ' ',
    createdDate: DateTimeUtils.getDateString(new Date())
  };
  const [filterParams, setFilterParams] = useState(initialParams);
  const { bookings: filterResult, isSearching: isFiltering } =
    useBookingFilter(filterParams);

  const handleFilterChange = (e) => {
    const { name, value } = e.target;
    if (name === 'status' && !(value in BookingStatus)) {
      setFilterParams({
        ...filterParams,
        [name]: ''
      });
    }
    setFilterParams({
      ...filterParams,
      [name]: value
    });
  };

  // SEARCH BOOKING BY CODE
  const [searchParams, setSearchParams] = useState({ bookingCode: '' });
  const [isShowResult, setIsShowResult] = useState(false);
  const { booking: searchBooking, isSearching } =
    useSearchBookingByCode(searchParams);
  const handleSearch = (e) => {
    const { value } = e.target;

    setSearchParams({
      ...searchParams,
      bookingCode: value
    });
    setIsShowResult(true);
    searchResultBox.current?.classList.remove('d-none');
  };
  const handleBlurSearch = (e) => {
    setTimeout(() => {
      searchResultBox.current?.classList.add('d-none');
      setIsShowResult(false);
    }, 200);
  };

  const showCheckIn = (booking) => (e) => {
    setBooking(booking);
    setShowDetails(true);
  };

  const { goHome } = useMyHistory();
  const {
    user: { roles }
  } = useSelector((state) => state.auth);
  const isEmployee = roles?.includes(Role.EMPLOYEE);
  // if (!isEmployee) {
  //   goHome();
  // }

  return (
    <div className="container" style={{ minHeight: ' 75vh' }}>
      <div className="py-3">
        <h1 className="">Check-in booking</h1>
        <hr />
        {/* filter */}
        <div className="d-flex justify-content-end mb-2">
          <div className="d-flex justify-content-end me-2">
            <div>
              <span className="flex-grow-0 badge bg-secondary">Filter</span>
            </div>
          </div>
          {/* createdDate */}
          <div className="mb-3 mb-md-0 me-2">
            <label htmlFor="createdDate" className="form-label">
              Created Date
            </label>
            <input
              name="createdDate"
              type="date"
              className="form-control form-control-sm"
              id="createdDate"
              value={filterParams.createdDate}
              onChange={handleFilterChange}
            />
          </div>
          <div className="d-flex flex-column ">
            <label htmlFor="status" className="form-label">
              Booking status
            </label>
            <select
              name="status"
              id="status"
              className="form-select form-select-sm"
              onChange={handleFilterChange}
              value={filterParams.status}
            >
              {BookingStatusList.map((status) => {
                let key = nanoid();
                return (
                  <option key={key} value={status}>
                    {status.replace('_', ' ')}
                  </option>
                );
              })}
            </select>
          </div>
        </div>

        {/* search by booking */}
        <div className="mb-3 position-relative">
          <input
            type="search"
            placeholder="Enter booking code"
            className="form-control"
            id="search"
            onChange={handleSearch}
            onBlur={handleBlurSearch}
          />

          {/* search result */}
          {isShowResult && (
            <div
              id="search-result"
              className="w-100 h-100  bg-light py-3"
              ref={searchResultBox}
            >
              {searchBooking ? (
                <BookingList
                  bookings={[searchBooking]}
                  showCheckIn={showCheckIn}
                />
              ) : (
                <div className="text-center">No booking match</div>
              )}
            </div>
          )}
        </div>

        <div>
          {!isShowResult && (
            <BookingList bookings={filterResult} showCheckIn={showCheckIn} />
          )}
        </div>

        {/* message */}
        {message && (
          <div className="text-center alert alert-danger mt-3" role="alert">
            {message}
          </div>
        )}

        {/* booking details */}
        {booking && (
          <BookingDetails
            show={showDetails}
            booking={booking}
            onHide={() => setShowDetails(false)}
          />
        )}

        {/* {isInitializing && <Loading />} */}
      </div>
    </div>
  );
};

export default CheckinBooking;
