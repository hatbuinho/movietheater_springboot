import { useRef } from 'react';
import ReactToPrint, { useReactToPrint } from 'react-to-print';
import ComponentToPrint from './ComponentToPrint';
import Tickets from './Tickets';
import { useHistory } from 'react-router';

const PrintTickets = ({ booking, isSuccess }) => {
  const componentRef = useRef();
  const history = useHistory();

  const handleAfterPrint = () => {
    history.go();
  };

  const handleTrigger = (ele) => {
    if (isSuccess) {
      console.log(ele);
      ele.click();
    }
  };

  return (
    <>
      <div className="d-none">
        <ComponentToPrint ref={componentRef}>
          <Tickets booking={booking} />
        </ComponentToPrint>
      </div>

      <ReactToPrint
        trigger={() => (
          <button ref={handleTrigger} className="btn btn-success">
            Print tickets
          </button>
        )}
        content={() => componentRef.current}
        onPrintError={() => console.log('print error')}
        onAfterPrint={handleAfterPrint}
      />
    </>
  );
};

export default PrintTickets;
