const REQUESTED = 'REQUESTED';
const CONFIRMED = 'CONFIRMED';
const CHECKED_IN = 'CHECKED_IN';
const CANCELLED = 'CANCELLED';
const ALL = 'ALL';

const BookingStatus = {
  CONFIRMED,
  CHECKED_IN,
  CANCELLED,
  REQUESTED
};

const BookingStatusList = [ALL, CONFIRMED, CHECKED_IN, CANCELLED, REQUESTED];

const BookingsStatusColor = {
  CONFIRMED: '#0d6efd',
  CHECKED_IN: '#198754',
  CANCELLED: '#dc3545',
  REQUESTED: '#ffc106'
};

const BookingStatusName = {
  CONFIRMED: 'New booking',
  CHECKED_IN: 'Checked in',
  CANCELLED: 'Cancelled',
  REQUESTED: 'Reserved'
}

export { BookingStatus, BookingStatusList, BookingsStatusColor, BookingStatusName };
