import './ticket.css';

const Ticket = ({ ticket, movieTitle, fullName, seatCode, startTime }) => {
  return (
    <div className="ticketWrap">
      <div className="ticket ticketLeft">
        <h1>
          Startup <span>Cinema</span>
        </h1>
        <div className="title">
          <h2>{movieTitle}</h2>
          <span>movie</span>
        </div>
        <div className="name">
          <h2>{fullName}</h2>
          <span>name</span>
        </div>
        <div className="seat">
          <h2>{seatCode}</h2>
          <span>seat</span>
        </div>
        <div className="time">
          <h2>{startTime.slice(0, 5)}</h2>
          <span>time</span>
        </div>
      </div>
      <div className="ticket ticketRight">
        <div className="eye"></div>
        <div className="number">
          <h3>{seatCode}</h3>
          <span>seat</span>
        </div>
        <div className="barcode"></div>
      </div>
    </div>
  );
};

export default Ticket;
