import Ticket from './Ticket';

const Tickets = ({ booking, className }) => {
  const {
    user: {
      contact: { fullName }
    },
    showSeats: tickets,
    show: {
      startTime,
      movie: { title }
    }
  } = booking;
  return (
    <div className={`container ${className}`}>
      {tickets.map((ticket) => {
        const { row, col } = ticket.seat;
        console.log(row, col);
        const seatCode = `${String.fromCodePoint(row + 64)}${col}`;
        return (
          <Ticket
            key={ticket.showSeatId}
            ticket={ticket}
            movieTitle={title}
            fullName={fullName}
            seatCode={seatCode}
            startTime={startTime}
          />
        );
      })}
    </div>
  );
};

export default Tickets;
