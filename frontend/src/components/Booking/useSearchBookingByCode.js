import { useEffect, useState } from 'react';
import useSearch from '../hooks/useSearch';

const searchByBookingCodeApi = 'bookings/code';

const useSearchBookingByCode = (params) => {
  const [booking, setBooking] = useState();

  const { result, error, isSearching } = useSearch(
    params,
    'NO__ACTION',
    searchByBookingCodeApi
  );
  useEffect(() => {
    setBooking(result);
  }, [result]);


  return { booking, error, isSearching };
};

export default useSearchBookingByCode;
