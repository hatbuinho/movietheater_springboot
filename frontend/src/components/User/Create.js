import SaveUser from './Save';

const CreateUser = () => {
  return (
    <div className="container-md p-2">
      <h1>Create User</h1>
      <hr />
      <SaveUser />
    </div>
  );
};

export default CreateUser;
