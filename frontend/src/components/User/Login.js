import React, { useEffect, useRef, useState } from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import CheckButton from 'react-validation/build/button';
import Validation from '../../utils/Validation';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { login } from '../../actions/auth.action';
import loginImage from '../../resources/images/login@4x.png';
import LoginForm from './LoginForm';

const Login = () => {
  return (
    <div
      className={`bg-light  row row-cols-2  row-cols-lg-3  justify-content-evenly align-items-center mx-0`}
      style={{ minHeight: '95vh' }}
    >
      <LoginForm fowardTo="/" />

      {/* image */}
      <div className="col d-none  d-lg-block ">
        <div className="p-2 w-100">
          <img
            src={loginImage}
            alt="login"
            style={{ maxWidth: '100%', height: ' auto' }}
          />
        </div>
      </div>
    </div>
  );
};

export default Login;
