import React, { useEffect, useRef, useState } from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import CheckButton from 'react-validation/build/button';
import Validation from '../../utils/Validation';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { login } from '../../actions/auth.action';

const LoginForm = ({ fowardTo = '/', className }) => {
  const initUser = {
    username: '',
    password: ''
  };
  console.log('forward', fowardTo);
  const checkBtn = useRef();
  const form = useRef();
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector((state) => state.auth);
  const { message } = useSelector((state) => state.message);
  const history = useHistory();
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);

  useEffect(() => {
    if (isLoggedIn) {
      history.push("/");
      history.go(0);
    }
  }, [isLoggedIn, history, fowardTo]);

  const [user, setUser] = useState(initUser);
  console.log('re render');
  const handleInputChange = (e) => {
    const { name, value } = e.target;

    setUser({
      ...user,
      [name]: value
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      setIsLoading(true);
      dispatch(login(user.username, user.password))
        .then(() => {
          setIsLoading(false);
          setIsSuccess(true);
          history.push(fowardTo);
          history.go(0);
        })
        .catch(() => {
          setIsLoading(false);
          setIsSuccess(false);
        });
    }
  };

  return (
    <div className={className}>
      <div className="col justify-content-center">
        <h1 className="text-success mb-3 fs-2">
          <span className="badge round-pill bg-success">Log</span>in
        </h1>
        <Form
          ref={form}
          onSubmit={handleSubmit}
          className="d-flex justify-content-center flex-column align-items-stretch rounded p-4 shadow-sm bg-white"
        >
          {/* Username */}
          <div className="mb-3">
            <label htmlFor="username" className="form-label">
              Username
            </label>
            <Input
              name="username"
              type="text"
              className="form-control"
              id="username"
              onChange={handleInputChange}
              validations={[Validation.required, Validation.validUsername]}
            />
          </div>
          {/* Password */}
          <div className="mb-3">
            <label htmlFor="passowrd" className="form-label">
              Password
            </label>
            <Input
              name="password"
              type="password"
              className="form-control"
              id="passowrd"
              onChange={handleInputChange}
              validations={[Validation.required, Validation.validPassword]}
            />
          </div>

          {/* message */}
          {message && (
            <div
              className={
                'text-center ' +
                (isSuccess ? 'alert alert-success' : 'alert alert-danger')
              }
              role="alert"
            >
              {message}
            </div>
          )}

          {/* action button */}
          <div className="d-flex justify-content-center ">
            <button type="submit" className="btn btn-primary me-2 flex-grow-1">
              {isLoading && (
                <span className="spinner-border spinner-border-sm"></span>
              )}{' '}
              Login
            </button>
          </div>

          <div className="mt-2">
            <Link to="/register">Register an account</Link>
          </div>

          <CheckButton style={{ display: 'none' }} ref={checkBtn} />
        </Form>
      </div>
    </div>
  );
};

export default LoginForm;
