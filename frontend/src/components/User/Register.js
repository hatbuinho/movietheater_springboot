import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import SaveUser from './Save';

const Register = () => {
  const { isLoggedIn } = useSelector((state) => state.auth);
  const history = useHistory();
  if (isLoggedIn) {
    history.replace('/');
    history.go(0);
  }

  return (
    <div className="container-md p-2">
      <h1>Sign up</h1>
      <hr />
      <SaveUser />
    </div>
  );
};

export default Register;
