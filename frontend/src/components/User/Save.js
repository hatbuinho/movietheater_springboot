import React, { useState, useRef, useCallback, useEffect } from 'react';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Textarea from 'react-validation/build/textarea';
import CheckButton from 'react-validation/build/button';

import Validation from '../../utils/Validation';
import { useSelector, useDispatch } from 'react-redux';
import { register } from '../../actions/auth.action';
import { useHistory, useParams } from 'react-router-dom';
import { ButtonGroup, ToggleButton } from 'react-bootstrap';
import Role from '../../utils/Role';
import { nanoid } from 'nanoid';
import UserAction from '../../actions/user.action';
import { handleCommonError } from '../../actions/handleError';

const roles = ['member', 'employee', 'admin'];
const genders = [
  { name: 'Male', value: 'MALE' },
  { name: 'Female', value: 'FEMALE' }
];

const SaveUser = () => {
  const { isLoggedIn } = useSelector((state) => state.auth);
  const { userId } = useParams();
  const history = useHistory();
  const { user: currentUser } = useSelector((state) => state.auth);
  const isAdmin = currentUser?.roles.includes(Role.ADMIN);

  const checkBtn = useRef();
  const form = useRef();
  const password = useRef();
  const dispatch = useDispatch();

  const [isLoading, setIsLoaing] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);

  const { message } = useSelector((state) => state.message);

  const initUser = {
    fullName: '',
    username: '',
    password: '',
    email: '',
    phone: '',
    dateOfBirth: '',
    address: '',
    gender: 'MALE',
    role: 'member'
  };

  const [user, setUser] = useState(initUser);
  password.current = user.password;

  useEffect(() => {
    if (!userId) return;
    dispatch(UserAction.findUserById(userId))
      .then((user) => {
        const { address, phone, dateOfBirth, gender } = user.contact;
        setUser({
          ...user,
          fullName: user.contact.fullName,
          role: roles[user.roles.length - 1],
          address,
          phone,
          dateOfBirth,
          gender
        });
      })
      .catch((err) => {
        handleCommonError(err, dispatch, false);
      });
  }, [dispatch, userId]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    setUser({
      ...user,
      [name]: value
    });
  };

  const confirmPassword = (value) => {
    if (value !== password.current) {
      console.log(value !== password.current);
      return (
        <div className="alert alert-danger" role="alert">
          Password not match.
        </div>
      );
    }
  };

  const submit = userId
    ? (user) => dispatch(UserAction.updateUser(userId, user))
    : isAdmin
    ? (user) => dispatch(UserAction.createUser(user))
    : (user) => dispatch(register(user));

  const handleSubmit = (e) => {
    e.preventDefault();
    form.current.validateAll();
    if (checkBtn.current.context._errors.length === 0) {
      setIsLoaing(true);
      submit(user)
        .then(() => {
          setIsSuccess(true);
          setIsLoaing(false);
          if (isLoggedIn) {
            setTimeout(() => {
              history.push('/users');
              history.go(0);
            }, 1500);
          } else {
            setTimeout(() => {
              history.push('/login');
              history.go(0);
            }, 1500);
          }
        })
        .catch(() => {
          setIsSuccess(false);
          setIsLoaing(false);
        });
    }
  };
  console.log(user);
  return (
    <div className="container-fluid  mt-md-3 mb-5 p-3  p-md-4 bg-light rounded shadow">
      <Form ref={form} onSubmit={handleSubmit}>
        {/* group 1 */}
        <div className="row row-cols-md-3 mb-md-3">
          {/* Username */}
          <div className="mb-3 mb-md-0">
            <label htmlFor="username" className="form-label">
              Username
            </label>
            <Input
              name="username"
              type="text"
              className="form-control"
              id="username"
              value={user.username}
              onChange={handleInputChange}
              validations={[Validation.required, Validation.validUsername]}
            />
          </div>

          {/* Password */}
          <div className="mb-3 mb-md-0">
            <label htmlFor="passowrd" className="form-label">
              Password
            </label>
            <Input
              name="password"
              type="password"
              className="form-control"
              id="passowrd"
              onChange={handleInputChange}
              validations={[Validation.required, Validation.validPassword]}
            />
          </div>

          {/* confirmpassword */}
          <div className="mb-3 mb-md-0">
            <label htmlFor="confirmPassword" className="form-label">
              Confirm password
            </label>
            <Input
              name="password"
              type="password"
              className="form-control"
              id="confirmPassword"
              validations={[Validation.required, confirmPassword]}
            />
          </div>
        </div>

        {/* group 2 */}
        <div
          className={`row mb-md-3 ${
            isAdmin ? 'row-cols-md-3' : 'row-cols-md-2'
          }`}
        >
          {/* Full name */}
          <div className="mb-3 mb-md-0">
            <label htmlFor="fullName" className="form-label">
              Full name
            </label>
            <Input
              name="fullName"
              type="text"
              className="form-control"
              id="fullName"
              value={user.fullName}
              onChange={handleInputChange}
              validations={[Validation.required]}
            />
          </div>

          {/* Email */}
          <div className="mb-3 mb-md-0">
            <label htmlFor="email" className="form-label">
              Email
            </label>
            <Input
              name="email"
              type="email"
              className="form-control"
              id="email"
              onChange={handleInputChange}
              value={user.email}
              validations={[Validation.required, Validation.validEmail]}
            />
          </div>

          {/* Roles */}
          {isAdmin && (
            <div className="d-flex flex-column">
              <label htmlFor="role" className="form-label">
                Role
              </label>
              <select
                name="role"
                id="role"
                className="form-select"
                onChange={handleInputChange}
                value={user.role}
              >
                {roles.map((role) => {
                  let key = nanoid();
                  return (
                    <option key={key} value={role}>
                      {role.toUpperCase()}
                    </option>
                  );
                })}
              </select>
            </div>
          )}
        </div>

        {/* group-3 */}
        <div className="row row-cols-md-3 mb-md-3">
          {/* Phone */}
          <div className="mb-3 mb-md-0">
            <label htmlFor="phone" className="form-label">
              Phone
            </label>
            <Input
              name="phone"
              type="number"
              className="form-control"
              id="phone"
              value={user.phone}
              onChange={handleInputChange}
              validations={[Validation.required, Validation.validPhone]}
            />
          </div>

          {/* dateOfBirth */}
          <div className="mb-3 mb-md-0">
            <label htmlFor="dateOfBirth" className="form-label">
              Date of birth
            </label>
            <Input
              name="dateOfBirth"
              type="date"
              className="form-control"
              id="dateOfBirth"
              value={user.dateOfBirth}
              onChange={handleInputChange}
              validations={[Validation.required]}
            />
          </div>

          {/* gender */}
          <div className="d-flex flex-column">
            <label htmlFor="dateOfBirth" className="form-label">
              Gender
            </label>
            <ButtonGroup>
              {genders.map((gd, idx) => (
                <ToggleButton
                  key={gd.value}
                  id={`radio-${idx}`}
                  type="radio"
                  variant="outline-primary"
                  name="gender"
                  value={gd.value}
                  checked={user.gender === gd.value}
                  onChange={handleInputChange}
                >
                  {gd.name}
                </ToggleButton>
              ))}
            </ButtonGroup>
          </div>
        </div>

        {/* address */}
        <div className="mb-3">
          <label htmlFor="address" className="form-label">
            Address
          </label>
          <Textarea
            name="address"
            type="date"
            className="form-control"
            id="address"
            onChange={handleInputChange}
            value={user.address}
            validations={[Validation.required]}
          />
        </div>

        {message && (
          <div
            className={isSuccess ? 'alert alert-success' : 'alert alert-danger'}
            role="alert"
          >
            {message}
          </div>
        )}

        {/* action button */}
        <div className="d-flex justify-content-center">
          <button type="submit" className="btn btn-primary me-2">
            {isLoading && (
              <span className="spinner-border spinner-border-sm"></span>
            )}{' '}
            {userId ? 'Update' : isAdmin ? 'Create User' : 'Sign up'}
          </button>
        </div>

        <CheckButton style={{ display: 'none' }} ref={checkBtn} />
      </Form>
    </div>
  );
};

export default SaveUser;
