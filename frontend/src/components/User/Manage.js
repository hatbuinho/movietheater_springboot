import React, { useEffect, useState } from 'react';
import { FormControl, Row, Col, Button, Table } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import Confirm from '../Modal/Confirm';
import UserAction from '../../actions/user.action';
import Loading from '../FullPageLoading/Loading';
import { nanoid } from 'nanoid';
import Role from '../../utils/Role';

const roles = ['member', 'employee', 'admin'];

const UserManagement = () => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(true);
  const history = useHistory();
  const {
    user: { roles }
  } = useSelector((state) => state.auth);

  /* get all users */
  useEffect(() => {
    dispatch(UserAction.findALlUser())
      .then(() => {
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [dispatch]);

  const _users = useSelector((state) => state.users);
  console.log(_users);
  const [users, setUsers] = useState(_users);

  useEffect(() => {
    setUsers(_users);
  }, [_users]);

  // for delete
  const { message } = useSelector((state) => state.message);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isDeleting, setIsDeleting] = useState(false);
  const handleDelete = (user) => (e) => {
    setIsDeleting(true);
    dispatch(UserAction.deleteUserById(user.userId))
      .then(() => {
        setIsSuccess(true);
        setTimeout(() => {
          setIsDeleting(false);
          history.push('/users');
          history.go(0);
        }, 1000);
      })
      .catch(() => {
        setIsDeleting(false);
        setIsSuccess(false);
      });
  };

  const isAdmin = roles?.includes(Role.ADMIN);
  if (!isAdmin) {
    history.push('/');
    history.go();
  }

  return (
    <>
      <div className="container">
        <h1 className="mb-3">Manage Users</h1>
        <Row className="mb-3 justify-content-center">
          <Col xs={6} md={6}>
            <FormControl
              type="search"
              placeholder="Search for users"
              // onChange={handleSearch}
            />
          </Col>
          <Col xs={3} md={3} lg={2}>
            <Button as={Link} to="/users/create">
              Add user
            </Button>
          </Col>
        </Row>
        <div>
          <Table striped bordered hover className="text-center rounded">
            <thead className="bg-success text-white">
              <tr>
                <td>Full name</td>
                <td>Username</td>
                <td>Email</td>
                <td>Role</td>
                <td>Actions</td>
              </tr>
            </thead>
            <tbody>
              {users?.map((user) => {
                console.log(users);
                const role = roles[user.roles?.length - 1];

                return (
                  <tr key={user.userId}>
                    <td>{user.contact?.fullName}</td>
                    <td>{user.username}</td>
                    <td>{user.email}</td>
                    <td>{role}</td>
                    <td className="align-middle text-nowrap">
                      <Button
                        as={Link}
                        to={`/users/${user.userId}`}
                        className="me-2"
                      >
                        View detail
                      </Button>
                      <Confirm
                        confirmName="Block"
                        buttonName="Block"
                        action={handleDelete(user)}
                        title="Confirm action"
                        value={user.userId}
                        variant="danger"
                        isLoading={isDeleting}
                      >
                        Do you want to bock this user?
                      </Confirm>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </div>
        {/* message */}
        {message && (
          <div
            className={
              'text-center position-fixed top-0 end-0 m-4 ' +
              (isSuccess ? 'alert alert-success' : 'alert alert-danger')
            }
            role="alert"
          >
            {message}
          </div>
        )}
        {isLoading && <Loading />}
      </div>
    </>
  );
};

export default UserManagement;
