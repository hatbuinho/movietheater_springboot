import React from 'react';
import SaveUser from './Save';

const UserDetails = () => {
  return (
    <div className="container-md p-2">
      <h1>User details</h1>
      <SaveUser />
    </div>
  );
};

export default UserDetails;
