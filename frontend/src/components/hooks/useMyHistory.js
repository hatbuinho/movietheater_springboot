import {useHistory} from 'react-router-dom' 

const useMyHistory = () => {
  const history = useHistory();

  const goBack = () => {
    history.go(-1);
  }

  const goHome = ()=>{
    history.push('/');
    history.go(0);
  }

  const forwadTo403 = ()=>{
    history.push('/un-authorized');
    history.go();
  }

  return {goBack, goHome, forwadTo403}
}

export default useMyHistory;
