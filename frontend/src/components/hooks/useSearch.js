import axios from 'axios';
import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { handleCommonError } from '../../actions/handleError';
import http from '../../utils/http-api.js';
import authHeader from '../../services/auth/auth-header'; 

const useSearch = (
  params = { query: '', pageNumber: 1 },
  actionType = '',
  api = ''
) => {
  const [isSearching, setIsSearching] = useState(true);
  const [error, setError] = useState(false);
  const [hasMore, setHasMore] = useState(false);
  const [result, setResult] = useState();
  const dispatch = useDispatch();

  useEffect(() => {
    let cancel;

    // params.query = params.query.split` `.join`%`;

    http
      .get(api, {
        params,
        headers: authHeader(),
        cancelToken: new axios.CancelToken((c) => (cancel = c))
      })
      .then((res) => {
        dispatch({
          type: actionType,
          payload: res.data
        });
        setTimeout(() => {
          setIsSearching(false);
        }, 500);
        setHasMore(res.data.lenth > 0);
        setResult(res.data);
      })
      .catch((e) => {
        if (axios.isCancel(e)) return;
        setError(true);
        setIsSearching(false);
        handleCommonError(e, dispatch, false);
      });
    return () => cancel();
  }, [params, dispatch, actionType, api]);

  return { isSearching, hasMore, error, result };
};

export default useSearch;
