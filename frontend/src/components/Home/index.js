import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { MovieType } from '../../actions/types';
import useSearch from '../hooks/useSearch';
import MovieCard from '../Movie/MovieCard';
import { nanoid } from 'nanoid';
import Loading from '../FullPageLoading/Loading';

const MovieAPI = '/public/movies';

const Home = () => {
  const initialParams = {
    query: '',
    pageNumber: 1
  };

  const _movies = useSelector((state) => state.movies);
  const [params, setParams] = useState(initialParams);
  const [movies, setMovies] = useState(_movies);
  const { message } = useSelector((state) => state.message);
  const { isSearching, hasMore, error } = useSearch(
    params,
    MovieType.GET_PAGE_MOVIE,
    MovieAPI
  );
  const [isInitializing, setIsInitializing] = useState(isSearching);

  const style = {
    minHeight: '95vh'
  };

  useEffect(() => {
    setMovies(_movies);
    if (_movies.length) {
      setTimeout(() => {
        setIsInitializing(false);
      }, 1500);
    }
    if (error) {
      setIsInitializing(false);
    }
  }, [_movies, error]);

  return (
    <>
      {message && (
        <div className="alert alert-danger text-center mt-2 mx-3">
          {message}
        </div>
      )}
      <div
        className="container-fluid container-md row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 justify-content-evenly mx-auto mt-4"
        style={style}
      >
        {movies.map((movie) => {
          let key = nanoid();
          return <MovieCard movie={movie} key={key} />;
        })}
      </div>
      {isInitializing && <Loading />}
    </>
  );
};

export default Home;
