import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useRouteMatch, useHistory } from 'react-router';
import { createBooking, setQuantity } from '../../actions/booking.action';
import { getAllRooms } from '../../actions/room.action';
import ShowSeatService from '../../services/showSeat/showSeat.service';
import Loading from '../FullPageLoading/Loading';
import SeatQuantityConfirm from '../Modal/SeatQuantityConfirm';
import CustomerSeat from '../Seat/CustomerSeat';
import SeatMap from '../Seat/SeatMap';
import { Button } from 'react-bootstrap';
import { findShowById } from '../../actions/show.action';
import { setMessage } from '../../actions/message.action';
import Convertor from '../../utils/Convertor';
import StringUtils from '../../utils/StringUtils';
import useMyHistory from '../hooks/useMyHistory';

const ShowSeatMap = () => {
  const { url } = useRouteMatch();
  const { showId } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const { selectedSeat, quantity } = useSelector((state) => state.preBooking);
  const { user } = useSelector((state) => state.auth);
  const { message } = useSelector((state) => state.message);
  const [movie] = useSelector((state) => state.movies);

  const [room, setRoom] = useState([]);
  const [showSeats, setShowSeats] = useState([]);
  const [qtt, setQtt] = useState(2);
  const [isLoading, setIsLoading] = useState(true);
  const [isBooking, setIsBooking] = useState(false);
  const [modalShow, setModalShow] = useState(true);
  const [isSuccess, setIsSuccess] = useState(false);
  const [isConflict, setIsConflict] = useState(false);

  const { goBack } = useMyHistory();

  // fetch all seats from room
  useEffect(() => {
    dispatch(getAllRooms())
      .then((rooms) => {
        setRoom(rooms[0]);
        setTimeout(() => {
          setIsLoading(false);
        }, 1000);
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [dispatch]);

  useEffect(() => {
    try {
      /* fetch show */
      dispatch(findShowById(showId))
        .then((show) => {})
        .catch(() => {
          console.log('error when fetch show');
        });

      /* fetch show seats */
      ShowSeatService.findByShow(showId).then((res) => {
        setShowSeats(res.data);
      });
    } catch {
      history.push('/');
      history.go(0);
    }
  }, [showId, dispatch, isConflict, history]);

  useEffect(() => {
    dispatch(setQuantity(qtt));
  }, [qtt, dispatch]);

  if (!movie) {
    const showDateUrl = StringUtils.getGoBackUrl(url);
    history.replace(showDateUrl);
    history.go(0);
  }

  const actualShowSeats = showSeats.map((s) => ({
    showSeatId: s.showSeatId,
    row: s.seat.row,
    col: s.seat.col,
    status: s.status,
    type: s.seat.type,
    listPrice: s.listPrice
  }));

  const totalPrice = selectedSeat
    ?.reduce((total, { listPrice }) => total + +listPrice, 0)
    .toFixed(0);

  const handleBooking = (e) => {
    setIsBooking(true);
    dispatch(
      createBooking(
        quantity,
        selectedSeat.map((seat) => seat.showSeatId),
        user.id,
        showId
      )
    )
      .then(() => {
        setIsSuccess(true);
        setTimeout(() => {
          setIsBooking(false);
          history.replace('/confirm-booking');
        }, 2000);
      })
      .catch(() => {
        setIsSuccess(false);
        setIsBooking(false);
        setIsConflict(true);
        setTimeout(() => {
          dispatch(setMessage(''));
        }, 4000);
        console.log('err when booking');
      });
  };

  return (
    <>
      <div>
        {/* select quantity */}
        <SeatQuantityConfirm
          onShow={modalShow}
          quantity={qtt}
          onChange={(qtt) => setQtt(qtt)}
          onHide={() => setModalShow(false)}
          backdrop="static"
        />
        {/* top bar */}
        <div className="py-2 pe-5 bg-success rounded d-flex justify-content-between mb-4 position-sticky top-0">
          <div className="d-flex">
            {/* back button */}
            <button
              className="btn btn-outline-success text-white me-4 fs-5  px-4"
              onClick={goBack}
            >
              <i className="fas fa-chevron-left"></i>
            </button>
            {/* movie title */}
            <div className="text-white fs-3">{movie.title}</div>
          </div>
          {/* quanity select button */}
          <div className="">
            <div
              className="btn btn-info flex-grow-0"
              onClick={() => setModalShow(true)}
            >
              {`${qtt} ${qtt > 1 ? 'tickets' : 'ticket'}`}{' '}
              <i className="fas fa-caret-down"></i>
            </div>
          </div>{' '}
        </div>

        {/* seat map */}
        <SeatMap room={room} SeatType={CustomerSeat} seats={actualShowSeats} />

        {/* Seat Note */}
        <div className="d-flex justify-content-center position-fixed bottom-0 bg-light p-2 w-100">
          <div className="d-flex align-items-center">
            <div
              className="bg-secondary p-2 rounded"
              style={{ width: '1rem', height: '1rem' }}
            ></div>
            <div className="ms-2 text-secondary">Sold</div>
          </div>
          <div className="d-flex align-items-center ms-4">
            <div
              className="p-2 rounded border border-success"
              style={{ width: '1rem', height: '1rem' }}
            ></div>
            <div className="ms-2 text-secondary">Available</div>
          </div>
          <div className="d-flex align-items-center ms-4">
            <div
              className="bg-success p-2 rounded"
              style={{ width: '1rem', height: '1rem' }}
            ></div>
            <div className="ms-2 text-secondary">Selected</div>
          </div>
        </div>

        {/* pay */}
        {selectedSeat.length === qtt && (
          <div className="bg-light position-fixed bottom-0 d-flex border-top justify-content-center w-100 py-3">
            <Button onClick={handleBooking} className="btn btn-danger  px-5">
              {isBooking && (
                <span className="spinner-border spinner-border-sm"></span>
              )}{' '}
              {`Pay ${Convertor.insertDot(totalPrice)} VND`}
            </Button>
          </div>
        )}

        {/* message */}
        {message && (
          <div
            className={
              'text-center position-fixed left-0 bottom-0 m-5 ' +
              (isSuccess ? 'alert alert-success' : 'alert alert-danger')
            }
            role="alert"
          >
            {message}
          </div>
        )}
        {/* padding block */}

        {isLoading && <Loading type="ThreeDots" />}
      </div>
    </>
  );
};

export default ShowSeatMap;
