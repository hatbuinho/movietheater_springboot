import { useParams, useRouteMatch } from 'react-router';
import { Link, useHistory } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import { nanoid } from 'nanoid';
import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

import NotFound from '../NotFound';
import ShowNotFound from './ShowNotFound';
import Loading from '../FullPageLoading/Loading';
import Convertor from '../../utils/Convertor';
import { findMovieById } from '../../actions/movies.action';
import StringUtils from '../../utils/StringUtils';
import { findShowByMovieAndDate } from '../../actions/show.action';
import { useSelector } from 'react-redux';
import { setMessage } from '../../actions/message.action';
import LoginModal from '../Modal/LoginModal';

const today = new Date();
const ONE_DATE_MILISECONDS = 24 * 60 * 60 * 1000;

const fourWeekDay = new Array(4)
  .fill()
  .map((_, index) =>
    Convertor.getDateString(
      new Date(today.getTime() + ONE_DATE_MILISECONDS * index)
    )
  );

const ShowDate = () => {
  const { url: currentUrl } = useRouteMatch();
  const { isLoggedIn } = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const { date, movieInfo } = useParams();
  const history = useHistory();

  const [shows, setShows] = useState([]);
  const [movie, setMovie] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const { url } = useRouteMatch();
  const genericUrl = url.split`/`.slice(0, -1).join`/`;
  const movieId = movieInfo.split`-`.reverse()[0];

  useEffect(() => {
    dispatch(findShowByMovieAndDate(movieId, date))
      .then((shows) => {
        setShows(shows);
      })
      .catch(() => {
        history.replace('/');
        history.go(0);
      });
  }, [date, movieId, dispatch, history]);

  useEffect(() => {
    dispatch(findMovieById(movieId))
      // MovieService.findById(movieId)
      .then((movie) => {
        setMovie(movie);
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      })
      .catch(() => {
        return <NotFound />;
      });
  }, [movieId, dispatch]);

  /* Handle select showtime*/
  const [showModal, setShowModal] = useState(false);
  const handleSelectShow = (show) => (e) => {
    if (isLoggedIn) {
      history.push(`${url}/layout-${show.showId}`);
    } else {
      setShowModal(true);
      dispatch(setMessage({ message: 'You need log in to book this show!' }));
    }
  };

  /* Redirect if invalid date */
  // if (!Date.parse(date)) return <NotFound />;

  // /* Redirect to today if enter past day */
  // const urlDate = new Date(date);
  // const todayDateString = Convertor.getDateString(new Date());
  // const today = new Date(todayDateString);
  // if (urlDate.getTime() < today.getTime()) {
  //   const todayUrl = StringUtils.getGoBackUrl(url);
  //   Convertor.getDateString(new Date());
  //   history.replace(todayUrl);
  //   history.go(0);
  // }

  return (
    <>
      <div className="container" style={{ minHeight: '90vh' }}>
        <h1 className="my-2 text-center">{movie.title}</h1>

        {/* movie image */}
        <div className="w-50 mx-auto mt-3 ">
          <img
            src={movie.largeImage}
            alt={movie.title}
            className="img-fluid rounded"
          />
        </div>

        {/* date navigation */}
        <div className="d-flex justify-content-center mt-3">
          {fourWeekDay.map((day, index) => {
            let key = nanoid();

            return (
              <div key={key} className="m-2">
                <Button
                  as={Link}
                  to={`${genericUrl}/${day}`}
                  variant={
                    Date.parse(day) === Date.parse(date)
                      ? 'danger'
                      : 'outline-danger'
                  }
                  className="fw-bold"
                >
                  {index === 0
                    ? 'Today'
                    : new Date(day).toDateString().split` `.slice(1, 3).join` `}
                </Button>
              </div>
            );
          })}
        </div>
        <hr />

        {/* Show time */}
        {shows.length ? (
          shows.map((show) => {
            let key = nanoid();
            const timeOption = {
              hour: '2-digit',
              minute: '2-digit'
            };
            const showTime = new Date(
              show.date + ' ' + show.startTime
            ).toLocaleTimeString([], timeOption);

            return (
              <Button
                variant="outline-success"
                className="rounded px-4 py-3 fs-5 fw-normal m-2"
                key={key}
                onClick={handleSelectShow(show)}
              >
                {showTime}
              </Button>
            );
          })
        ) : (
          <ShowNotFound />
        )}
      </div>
      <LoginModal
        show={showModal}
        forwardTo={currentUrl}
        className=""
        onHide={() => setShowModal(false)}
      />
      {isLoading && <Loading />}
    </>
  );
};

export default ShowDate;
