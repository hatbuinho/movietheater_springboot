import { useEffect, useMemo, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Input from 'react-validation/build/input';
import CheckButton from 'react-validation/build/button';
import Form from 'react-validation/build/form';
import {
  createShow,
  findShowById,
  updateShow
} from '../../actions/show.action';
import Validation from '../../utils/Validation';
import useSearch from '../hooks/useSearch';
import { MovieType } from '../../actions/types';
import { nanoid } from 'nanoid';
import { useHistory, useParams } from 'react-router-dom';
import useSearchMovie from '../Movie/useSearchMovie';
import useSelectedMovie from '../Movie/useSelectedMovie';
import Convertor from '../../utils/Convertor';
import MovieSearch from '../Movie/MovieSearch';

/**
 * movieId
 * date
 * startTime
 * endTime : auto
 * price
 */
const options = { hour12: false };

const SaveShow = () => {
  const currentDate = new Date().toISOString().split`T`[0];
  const currentTime = new Date()
    .toLocaleTimeString('en-US', options)
    .slice(0, 5);

  const dispatch = useDispatch();

  const { showId } = useParams();

  const initialState = {
    movieId: '',
    date: currentDate,
    startTime: currentTime,
    listPrice: ''
  };

  const [show, setShow] = useState(initialState);

  const form = useRef();
  const checkBtn = useRef();

  const { message } = useSelector((state) => state.message);

  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);

  // for search
  const initialSearchParams = {
    query: '',
    pageNumber: 1
  };
  const searchResult = useRef();
  const [params, setParams] = useState(initialSearchParams);
  const [isShowResult, setIsShowResult] = useState(false);
  const { movies } = useSearchMovie(params);

  const [endTime, setEndTime] = useState();

  const history = useHistory();
  const [movie, setMovie] = useState();

  // handle update show
  useEffect(() => {
    if (!showId) return;
    dispatch(findShowById(showId))
      .then((data) => {
        setShow({
          movieId: data.movie.movieId,
          ...data
        });
        setMovie(data.movie);
      })
      .catch(() => {
        console.log('error when find show by id');
      });
  }, [dispatch, showId, setMovie]);

  // handle end time
  useEffect(() => {
    if (show?.startTime && movie) {
      let sDate = new Date('1970-01-01 ' + show.startTime);
      const sHours = sDate.getHours();
      const eMinutes = sDate.getMinutes() + +movie.duration;

      const hours = (sHours + Math.floor(eMinutes / 60)) % 24;

      const minutes = Math.floor(eMinutes % 60);

      setEndTime(
        `${hours < 10 ? '0' + hours : hours}:${
          minutes < 10 ? '0' + minutes : minutes
        }`
      );
    }
  }, [movie, show]);

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    if (name === 'listPrice') {
      setShow({
        ...show,
        [name]: Convertor.insertDot(value.replaceAll(' ', ''))
      });
    } else {
      setShow({
        ...show,
        [name]: value
      });
    }
  };
// handle select movie
  const handleSelectMovie = (movie) => (e) => {
    setMovie(movie);
    setShow({
      ...show,
      movieId: movie.movieId
    });
  };

  // submit show
  const handleSubmit = (e) => {
    e.preventDefault();
    form.current.validateAll();

    if (checkBtn.current?.context?._errors.length === 0) {
      show.listPrice = show.listPrice.toString().replace(/\s/g, '');
      show.startTime = show.startTime.slice(0, 5);

      const action = show.showId
        ? updateShow(show.showId, show)
        : createShow(show);

      setIsLoading(true);
      dispatch(action)
        .then(() => {
          setIsLoading(false);
          setIsSuccess(true);
          setTimeout(() => {
            history.replace('/shows');
            history.go(0);
          }, 1000);
        })
        .catch(() => {
          setIsLoading(false);
          setIsSuccess(false);
        });
    }
  };
  return (
    <div>
      <Form
        ref={form}
        onSubmit={handleSubmit}
        className="d-flex justify-content-center flex-column align-items-stretch rounded  p-4 shadow-sm bg-white"
      >
        {/* Search movie */}
        {!showId && (
          <MovieSearch handleSelectMovie={handleSelectMovie} className="mb-3" />
        )}

        {/* Selected movie */}
        <div className="mb-3">
          <label htmlFor="movie" className="form-label">
            Movie
          </label>
          <Input
            type="text"
            className="form-control"
            id="movie"
            disabled
            validations={[Validation.required]}
            value={movie?.title || movie?.title}
          />
        </div>

        {/* date */}
        <div className="mb-3">
          <label htmlFor="date" className="form-label">
            Date
          </label>
          <Input
            name="date"
            type="date"
            className="form-control"
            id="date"
            onChange={handleInputChange}
            validations={[Validation.required]}
            value={show.date}
          />
        </div>

        {/* Start time */}
        <div className="mb-3">
          <label htmlFor="date" className="form-label">
            Start time
          </label>
          <Input
            name="startTime"
            type="time"
            className="form-control"
            id="date"
            value={show.startTime.slice(0, 5)}
            onChange={handleInputChange}
            validations={[Validation.required]}
          />
        </div>

        {/* End time */}
        <div className="mb-3">
          <label className="form-label">End time</label>
          <Input
            type="time"
            className="form-control"
            validations={[Validation.required]}
            value={endTime}
            disabled
          />
        </div>

        {/* price */}
        <div className="mb-3">
          <label className="form-label">Price</label>
          <Input
            type="text"
            name="listPrice"
            className="form-control"
            validations={[Validation.required]}
            onChange={handleInputChange}
            value={show?.listPrice}
          />
        </div>

        {/* message */}
        {message && (
          <div
            className={
              'text-center ' +
              (isSuccess ? 'alert alert-success' : 'alert alert-danger')
            }
            role="alert"
          >
            {message}
          </div>
        )}

        {/* action button */}
        <div className="d-flex justify-content-center ">
          <button type="submit" className="btn btn-primary me-2 flex-grow-1">
            {isLoading && (
              <span className="spinner-border spinner-border-sm"></span>
            )}{' '}
            {show.showId ? 'Update' : 'Add Show'}
          </button>
        </div>

        <CheckButton style={{ display: 'none' }} ref={checkBtn} />
      </Form>
    </div>
  );
};

export default SaveShow;
