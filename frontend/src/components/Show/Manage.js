import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { getAllShows } from '../../actions/show.action';
import useSearch from '../hooks/useSearch';
import { Button, Table, FormControl, Row, Col } from 'react-bootstrap';
import Confirm from '../Modal/Confirm';
import useMyHistory from '../hooks/useMyHistory';
import Role from '../../utils/Role';

const showApi = '/shows';

const ShowManagement = () => {
  const dispatch = useDispatch();
  const [query, setQuery] = useState();
  const [pageNumber, setPageNumber] = useState();

  useEffect(() => {
    dispatch(getAllShows())
      .then(() => {})
      .catch(() => {
        console.log('error when get all shows');
      });
  }, [dispatch]);

  const shows = useSelector((state) => state.shows);

  const { goHome } = useMyHistory();
  const { user: {roles} } = useSelector((state) => state.auth);
  const isEmployee = roles?.includes(Role.EMPLOYEE);
  if (!isEmployee) {
    goHome();
  }
  return (
    <div className="container">
      <h1 className="mb-3">Manage shows</h1>
      <Row className="mb-3 justify-content-center">
        <Col xs={6} md={6}>
          <FormControl
            type="search"
            placeholder="Search for shows"
            // onChange={handleSearch}
          />
        </Col>
        <Col xs={3} md={3} lg={2}>
          <Button as={Link} to="/shows/create">
            Add show
          </Button>
        </Col>
      </Row>
      <div>
        <Table striped bordered hover className="text-center rounded">
          <thead className="bg-success text-white">
            <tr>
              <td>Movie</td>
              <td>Date</td>
              <td>Start time</td>
              <td>End time</td>
              <td>Actions</td>
            </tr>
          </thead>
          <tbody>
            {shows.map((show) => {
              return (
                <tr key={show.showId}>
                  <td>{show.movie.title}</td>
                  <td>{show.date}</td>
                  <td>{show.startTime}</td>
                  <td>{show.endTime}</td>
                  <td className="align-middle text-nowrap">
                    <Button
                      as={Link}
                      to={`/shows/${show.showId}`}
                      className="me-2"
                    >
                      View
                    </Button>
                    <Confirm
                      confirmName="Cancel"
                      buttonName="Cancel"
                      // action={handleDelete}
                      title="Confirm action"
                      value={show.showId}
                      variant="danger"
                    >
                      Do you want to cancel this show?
                    </Confirm>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    </div>
  );
};

export default ShowManagement;
