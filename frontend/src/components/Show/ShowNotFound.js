import React from 'react';

const ShowNotFound = () => {
  return (
    <div className="container">
      <div className="text-center text-secondary mt-5 fs-3">Sorry. No show is presenting</div>
    </div>
  );
};

export default ShowNotFound;
