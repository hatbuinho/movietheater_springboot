import { useEffect, useState } from 'react';
import DateTimeUtils from '../../utils/DateTimeUtils';
import { WHOLE_PAGE, ALL_MOVIE } from '../../utils/Constants';
import { BookingStatus, BookingStatusList } from '../Booking/BookingStatus';
import BookingService from '../../services/booking';
import TotalChart from './TotalPieChart';
import Convertor from '../../utils/Convertor';
import MovieSearch from '../Movie/MovieSearch';
import PaymentService from '../../services/payment';
import RevenueChart from './RevenueBarChart';
// import Loading from '../FullPageLoading/Loading';

const categorize = (bookings = []) => {
  const bookingByStatus = {};
  BookingStatusList.forEach((status) => {
    if (status !== 'ALL' && status !== 'REQUESTED') bookingByStatus[status] = 0;
  });

  if (bookings?.length) {
    bookings.forEach((b) => {
      bookingByStatus[b.status] += 1;
    });
  }
  return bookingByStatus;
};

const categorizeByMonth = (data) => {
  const paymentByMonth = {};
  const yearArray = [];
  DateTimeUtils.MonthStringArray.forEach(
    (month) => (paymentByMonth[month] = {})
  );

  data.forEach((payment) => {
    const month = DateTimeUtils.getMonth(payment.createdDate);
    const year = DateTimeUtils.getYear(payment.createdDate);

    if (!yearArray.includes(year)) yearArray.push(year);

    if (DateTimeUtils.MonthStringArray.includes(month)) {
      paymentByMonth[month][year] = paymentByMonth[month][year] ?? 0;
      paymentByMonth[month][year] += payment.amount;
    }
  });

  const result = Object.keys(paymentByMonth).map((month) => ({
    name: month,
    ...paymentByMonth[month]
  }));

  return [result, yearArray];
};

const Statistics = () => {
  // filter all bookings
  const [filterBookings, setFilterBookings] = useState([]);
  const initialParams = {
    movieId: ALL_MOVIE,
    pageNumber: WHOLE_PAGE,
    createdDate: ''
  };
  const [params, setParams] = useState(initialParams);

  // handle select movie
  const [selectedMovie, setSelectedMovie] = useState();

  const handleSelectMovie = (movie) => (e) => {
    setSelectedMovie(movie);
    const { movieId } = movie;
    setParams({
      ...params,
      movieId
    });
  };

  // handle remove selected movie
  const clearFilterBooking = (e = new Event()) => {
    setSelectedMovie({
      movieId: ALL_MOVIE,
      title: 'All movies'
    });
    setParams({
      ...params,
      movieId: ALL_MOVIE,
      createdDate: ''
    });
  };

  const [isFilterting, setIsFiltering] = useState(false);

  useEffect(() => {
    setIsFiltering(true);
    BookingService.filterBooking(params.movieId, params)
      .then((res) => {
        setFilterBookings(res.data);
        setIsFiltering(false);
      })
      .catch((err) => {
        console.log('err when filter');
        setIsFiltering(false);
      });
  }, [params]);

  const handleDateChange = (e) => {
    const { value } = e.target;
    setParams({
      ...params,
      createdDate: value
    });
  };

  const handleMovieChange = (e) => {
    setParams({
      ...params,
      movieId: selectedMovie.movieId
    });
  };

  const totalRevenue = filterBookings
    .map((booking) => booking.payment)
    .map((payment) => (payment ? payment.amount : 0))
    .reduce((total, amount) => total + amount, 0);

  // analysis revenue by year, month
  const [selectedYear, setSelectedYear] = useState(2021);
  const [payments, setPayments] = useState([]);
  useEffect(() => {
    PaymentService.findByYearAndMonth()
      .then((res) => {
        setPayments(res.data);
      })
      .catch((err) => {
        console.log('fetch payment err', err);
      });
  }, [selectedYear]);

  const currentYear = new Date().getFullYear();
  const handleYearChange = (e) => {
    const { value } = e.target;
    setSelectedYear(value);
  };
  const [data, yearList] = categorizeByMonth(payments);

  const initialYears = new Array(3)
    .fill()
    .map((_, index) => currentYear + index - 1);
  initialYears.unshift('All years');

  const [years, setYears] = useState(initialYears);

  const clearYearFilter = (e) => {
    setSelectedYear('All years');
  };

  console.log(filterBookings);

  return (
    <div className="container">
      <h1 className="my-3">Statistic</h1>

      <div className="row row-cols-lg-2">
        {/* today */}
        <div className="row row-cols-lg-2  gx-lg-3 g-3  align-content-evenly align-items-center">
          {/* Revenue  */}
          <div>
            <div className="card ">
              <div className="card-body border-start border-5 border-primary rounded">
                <div className="fs-4">
                  {Convertor.insertDot(totalRevenue + '')}{' '}
                  <small className="badge fs-6 bg-success">VND</small>
                </div>
                <p className="card-title">Revenue</p>
              </div>
            </div>
          </div>

          {/* new booking */}
          <div>
            <div className="card ">
              <div className="card-body border-start border-5 border-warning rounded">
                <div className="fs-4">
                  {categorize(filterBookings)[BookingStatus.CONFIRMED] || 0}
                </div>
                <p className="card-title">New Booking</p>
              </div>
            </div>
          </div>

          {/* checkin booking */}
          <div>
            <div className="card">
              <div className="card-body border-start border-5 border-success rounded">
                <div className="fs-4">
                  {categorize(filterBookings)[BookingStatus.CHECKED_IN] || 0}
                </div>
                <p className="card-title">Checked in</p>
              </div>
            </div>
          </div>

          {/* cancelled booking */}
          <div>
            <div className="card ">
              <div className="card-body border-start border-5 border-danger  rounded">
                <div className="fs-4">
                  {categorize(filterBookings)[BookingStatus.CANCELLED] || 0}
                </div>
                <p className="card-title">Cancelled</p>
              </div>
            </div>
          </div>
        </div>

        {/* all */}
        <div className="">
          <div className="card bg-light d-flex justify-content-center">
            {/* filter section */}
            <div className="d-flex align-items-end justify-content-center pt-2">
              <div className="m-1">
                <label
                  htmlFor="createdDate"
                  className="form-label me-2 badge bg-secondary"
                >
                  Filter
                </label>

                {/* clear filter */}
                <div
                  className="btn btn-sm  btn-secondary mb-2"
                  onClick={clearFilterBooking}
                >
                  Clear filter
                </div>

                <input
                  name="createdDate"
                  type="date"
                  className="form-control"
                  id="createdDate"
                  value={params.createdDate}
                  onChange={handleDateChange}
                />
              </div>
              <div className="m-1">
                <MovieSearch
                  handleSelectMovie={handleSelectMovie}
                  placeholder="Filter by movie"
                />

                {/* Selected movie */}
                <div className="mt-2 position-relative">
                  <input
                    type="text"
                    className="form-control"
                    id="movie"
                    disabled
                    name="movieId"
                    value={selectedMovie?.title || 'All movies'}
                    onChange={handleMovieChange}
                  />
                </div>
              </div>
            </div>

            <div className="card-body">
              <h5 className="card-title w-100 text-center">
                Bookings analysis
              </h5>
            </div>
            <TotalChart bookings={categorize(filterBookings)} />
          </div>
        </div>
      </div>

      <div className="row mt-5 justify-content-center align-items-center bg-light rounded p-3">
        <div className="d-flex justify-content-between mb-3">
          <div className="fs-3">Revenue analysis</div>

          <div className="d-flex">
            <div>
              <label
                htmlFor="createdDate"
                className="form-label me-2 badge bg-secondary"
              >
                Filter
              </label>
            </div>

            <div>
              <select
                name="year"
                value={selectedYear}
                className="form-select"
                onChange={handleYearChange}
              >
                {years.map((year) => (
                  <option key={year}>{year}</option>
                ))}
              </select>
            </div>

            <div className="d-flex align-items-end m-1">
              <button
                className="btn btn-sm  btn-secondary"
                onClick={clearYearFilter}
              >
                Clear Filter
              </button>
            </div>
          </div>
        </div>

        <div className="flex-fill">
          <RevenueChart
            data={data}
            selectedYear={selectedYear}
            yearList={yearList}
          />
        </div>
      </div>

      {/* {isFilterting && <Loading />} */}
    </div>
  );
};

export default Statistics;
