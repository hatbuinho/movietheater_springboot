import React from 'react';
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer
} from 'recharts';
import { randomColor } from '../../utils/Generator';


export default function RevenueChart({ data, selectedYear, yearList }) {
  return (
    <ResponsiveContainer width="100%" height={300}>
      <BarChart width={500} height={300} data={data}>
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="name" />
        <YAxis />
        <Tooltip />
        <Legend />
        {selectedYear === 'All years' ? (
          yearList.map((year) => (
            <Bar dataKey={year} fill={`#${randomColor}`} />
          ))
        ) : (
          <Bar dataKey={selectedYear} fill={`#${randomColor}`} />
        )}
      </BarChart>
    </ResponsiveContainer>
  );
}
