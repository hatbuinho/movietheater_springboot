import {
  BarChart,
  Tooltip,
  ResponsiveContainer,
  CartesianGrid,
  XAxis,
  YAxis,
  Legend,
  Bar
} from 'recharts';
import {
  BookingsStatusColor,
  BookingStatus,
  BookingStatusName
} from '../Booking/BookingStatus';

const renderLegend = (props) => {
  const { payload } = props;

  return (
    <ul>
      {payload.map((entry, index) => (
        <li key={`item-${index}`}>{BookingStatusName[entry.value]}</li>
      ))}
    </ul>
  );
};

const FilterBarChart = ({ payments }) => {
  const status = Object.keys(payments);
  console.log(status);
  return (
    <div>
      <div className="text-center fw-bold">Bookings by date</div>
      <ResponsiveContainer width="100%" height={350}>
        <BarChart data={[payments]} height={300}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="date" />
          <YAxis />
          {/* <Tooltip /> */}
          <Legend formatter={(value) => BookingStatusName[value]} />
          {status.map((stt) => {
            return (
              <Bar
                key={stt}
                dataKey={stt}
                fill={BookingsStatusColor[stt]}
                label
              />
            );
          })}
        </BarChart>
      </ResponsiveContainer>
    </div>
  );
};

export default FilterBarChart;
