import Loader from 'react-loader-spinner';
import './loading.css';

const Loading = ({ type = 'Oval' }) => {
  return (
    <div
      className="FullPageLoading position-fixed fixed-top"
      style={{ minHeight: '100vh', zIndex: 2000 }}
    >
      <Loader
        className="Loader"
        type={type}
        color="hsl(152, 67%, 32%)"
        height={50}
        width={50}
      />
    </div>
  );
};

export default Loading;
