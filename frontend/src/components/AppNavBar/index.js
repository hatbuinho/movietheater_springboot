import { nanoid } from 'nanoid';
import React, { useCallback, useRef, useState } from 'react';
import { useEffect } from 'react';
import {
  Dropdown,
  Button,
  Container,
  Navbar,
  Nav,
  Form
} from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useHistory } from 'react-router-dom';
import { logout } from '../../actions/auth.action';
import Role from '../../utils/Role';
import Logo from '../Logo';
import MovieSearch from '../Movie/MovieSearch';
import useSearchMovie from '../Movie/useSearchMovie';

const AppNavBar = () => {
  const { user: currentUser } = useSelector((state) => state.auth);

  const [isAdmin, setisAdmin] = useState(false);
  const [isEmployee, setIsEmployee] = useState(false);
  const { isLoggedIn } = useSelector((s) => s.auth);
  const { user } = useSelector((s) => s.auth);
  const dispatch = useDispatch();
  const history = useHistory();



  useEffect(() => {
    if (currentUser) {
      setisAdmin(currentUser.roles.includes(Role.ADMIN));
      setIsEmployee(currentUser.roles.includes(Role.EMPLOYEE));
    }
  }, [currentUser]);

  const logOut = useCallback(() => {
    dispatch(logout());
    history.replace('/');
    history.go(0);
  }, [dispatch, history]);

  const handleSelectMovie = (movie) => (e) => {
    history.push(`/movies/${movie.movieId}`);
  };

  return (
    <>
      <Navbar bg="dark" expand="lg" className="shadow-sm navbar-dark">
        <Container fluid>
          <Logo className="fs-5" />

          {/* button toggle menu */}
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="flex-grow-1 mx-2 align-items-evenly mt-3 mt-lg-0">
              <div className="d-flex mb-2 mb-lg-0">
                {/* employee */}
                {isEmployee && (
                  <Dropdown>
                    <Dropdown.Toggle
                      variant="btn btn-light"
                      id="dropdown-basic"
                      className="me-2"
                    >
                      Employee
                    </Dropdown.Toggle>
                    <Dropdown.Menu className="position-absolute">
                      <Dropdown.Item as={Link} to="/movies">
                        Manage movies
                      </Dropdown.Item>
                      <Dropdown.Item as={Link} to="/shows">
                        Manage shows
                      </Dropdown.Item>
                      <Dropdown.Item as={Link} to="/check-in">
                        Check-in
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                )}
                {/* admin */}
                {isAdmin && (
                  <Dropdown>
                    <Dropdown.Toggle
                      variant=""
                      id="dropdown-basic"
                      className="me-2 btn btn-light"
                    >
                      Admin
                    </Dropdown.Toggle>
                    <Dropdown.Menu className="position-absolute">
                      <Dropdown.Item as={Link} to="/users">
                        Manage users
                      </Dropdown.Item>
                      <Dropdown.Item as={Link} to="/seats">
                        Manage seats
                      </Dropdown.Item>
                      <Dropdown.Item as={Link} to="/statistics">
                        Statistics
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                )}
              </div>

              <div className="d-flex flex-grow-1 ">
                <MovieSearch
                  handleSelectMovie={handleSelectMovie}
                  className="flex-grow-1"
                />

                {isLoggedIn ? (
                  <Dropdown>
                    <Dropdown.Toggle
                      variant="outline-success rounded-pill"
                      id="dropdown-basic"
                      className="ms-2 text-white"
                    >
                      {user?.fullName.toUpperCase()}
                    </Dropdown.Toggle>
                    <Dropdown.Menu className="position-absolute">
                      <Dropdown.Item as={Link} to="/profile">
                        View profile
                      </Dropdown.Item>
                      <Dropdown.Item as={Link} to="/myprofile/booking-history">
                        History booking
                      </Dropdown.Item>
                      <Dropdown.Item as={Button} onClick={logOut}>
                        Logout
                      </Dropdown.Item>
                    </Dropdown.Menu>
                  </Dropdown>
                ) : (
                  <div className="d-flex">
                    <Link className="btn text-white" to="/login">
                      Sign in
                    </Link>
                    <Link className="btn btn-outline-light" to="/register">
                      Sign up
                    </Link>
                  </div>
                )}
              </div>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      {/* padding when fixed top */}
      {/* <div className="w-100" style={{ height: '6rem' }}></div> */}
    </>
  );
};

export default AppNavBar;
