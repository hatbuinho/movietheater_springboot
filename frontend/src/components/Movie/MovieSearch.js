import { useRef, useState } from 'react';
import { Form } from 'react-bootstrap';
import useSearchMovie from './useSearchMovie';

const MovieSearch = ({
  handleSelectMovie,
  className,
  placeholder = 'Search for Movies'
}) => {
  // handle search init
  const searchResult = useRef();
  const initialSearchParams = {
    query: '',
    pageNumber: 1
  };
  const [params, setParams] = useState(initialSearchParams);
  const [isShowResult, setIsShowResult] = useState(false);

  // fetch result of search
  const { movies } = useSearchMovie(params);

  // handle search event
  const handleSearch = (e) => {
    setIsShowResult(true);
    const { value } = e.target;
    setParams({
      ...params,
      query: value
    });
    searchResult.current?.classList.remove('d-none');
  };

  const handleBlurSearch = (e) => {
    setTimeout(() => {
      searchResult.current?.classList.add('d-none');
    }, 200);
  };

  return (
    <>
      {/* search box */}
      <div className={`position-relative ${className}`}>
        <Form.Control
          size="md"
          type="search"
          placeholder={placeholder}
          name="query"
          onFocus={handleSearch}
          onChange={handleSearch}
          onBlur={handleBlurSearch}
          autoComplete="off"
        />
        {/* search result */}
        {isShowResult && (
          <div
            id="search-result"
            className="position-absolute w-100 h-200 overflow-auto"
            ref={searchResult}
            style={{
              maxHeight: '200px',
              cursor: 'pointer',
              zIndex: 999
            }}
          >
            {movies?.map((movie) => {
              return (
                <div
                  key={movie.movieId}
                  className="bg-dark text-light p-1 border border-white rounded"
                  onClick={handleSelectMovie(movie)}
                  style={{ cursor: 'pointer' }}
                >
                  {movie.title}
                </div>
              );
            })}
          </div>
        )}
      </div>
    </>
  );
};

export default MovieSearch;
