import { Link } from 'react-router-dom';
import './movie.css';

const MovieCard = ({ movie }) => {
  return (
    <div className="col">
      <div className="d-flex flex-column position-relative px-4 mb-5 ">
        <div
          className="d-flex position-relative justify-content-center"
          style={{ width: '100%', height: '100%' }}
        >
          <img
            style={{ borderRadius: '10px', maxWidth: '100%', height: '230px' }}
            src={movie.smallImage}
            alt={movie.title}
            className="Movie-card__img card-fluid"
          />
          {/* <div className="position-absolute bottom-0 start-50">
          {movie.releaseDate}
        </div> */}
        </div>
        <div className="d-flex flex-column  align-items-center">
          <h5
            className="d-flex  py-2 text-center"
            style={{ fontSize: 'min(max(16px, 1.5vw), 30px)' }}
          >
            {movie.title}
          </h5>
          <h6
            className="d-flex text-secondary"
            style={{ fontSize: 'min(max(16px, 1.25vw), 22px)' }}
          >
            {movie.genre}
          </h6>
        </div>
        <Link
          to={`/movies/${movie.movieId}`}
          className="w-100 h-100 d-block position-absolute top-0 botton-0 start-0 end-0"
        />
      </div>
    </div>
  );
};

export default MovieCard;
