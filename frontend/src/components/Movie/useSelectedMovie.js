import { useRef, useState, useEffect } from 'react';
import useSearchMovie from './useSearchMovie';
import Input from 'react-validation/build/input';
import { nanoid } from 'nanoid';

const useSelectedMovie = () => {
  const [mv, setMv] = useState();
  const SearchMovie = () => {
    const searchResult = useRef();

    const initialParams = {
      query: '',
      pageNumber: 1
    };
    const [params, setParams] = useState(initialParams);

    const [isShowResult, setIsShowResult] = useState(false);

    const [movies] = useSearchMovie(params);
    const [movie, setMovie] = useState();

    useEffect(() => {
      setMv(movie);
    }, [movie])

    const handleSearch = (e) => {
      setIsShowResult(true);
      const { value } = e.target;
      setParams({
        ...params,
        query: value
      });
      searchResult.current?.classList.remove('d-none');
    };

    const handleBlurSearch = (e) => {
      setTimeout(() => {
        searchResult.current?.classList.add('d-none');
      }, 200);
    };

    const handleSelectMovie = (movie) => (e) => {
      setMovie(movie);
    };

    return (
      <div className="mb-3 position-relative">
        <Input
          type="search"
          placeholder="Search for movie"
          className="form-control"
          id="search"
          // validations={[Validation.required]}
          onFocus={handleSearch}
          onChange={handleSearch}
          onBlur={handleBlurSearch}
        />
        {/* search result */}
        {isShowResult && (
          <div
            id="search-result"
            className="position-absolute w-100 h-200 overflow-auto"
            ref={searchResult}
            style={{ maxHeight: '200px', cursor: 'pointer' }}
          >
            {movies.map((movie) => {
              let key = nanoid();
              return (
                <div
                  key={key}
                  className="bg-dark text-light p-1 border border-white rounded"
                  onClick={handleSelectMovie(movie)}
                  style={{ cursor: 'pointer' }}
                >
                  {movie.title}
                </div>
              );
            })}
          </div>
        )}
      </div>
    );
  };
  return [SearchMovie, mv, setMv];
};

export default useSelectedMovie;
