import React, { useState } from 'react';
import { FormControl, Row, Col, Button, Table } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import { MovieType } from '../../actions/types';
import useSearch from '../hooks/useSearch';

const API = '/movies';

const MovieManagement = () => {
  // const dispatch = useDispatch();
  const initParams = {
    query: '',
    pageNumber: 1
  };

  const [params, setParmas] = useState(initParams);

  // fetch movie page
  useSearch(params, MovieType.GET_PAGE_MOVIE, API);
  const movies = useSelector((state) => state.movies);

  // const handleDelete = (id) => {
  //   dispatch(deleteMovie(id));
  // };

  const handleSearch = (e) => {
    const { name, value } = e.target;
    setParmas({
      ...params,
      [name]: value
    });
  };

  return (
    <>
      <div className="container">
        <h1 className="mb-3">Manage Movies</h1>
        <Row className="mb-3 justify-content-center">
          <Col xs={6} md={6}>
            <FormControl
              type="search"
              placeholder="Search for movies"
              name="query"
              onChange={handleSearch}
              autoComplete="off"
            />
          </Col>
          <Col xs={3} md={3} lg={2}>
            <Button as={Link} to="/movies/create">
              Add movie
            </Button>
          </Col>
        </Row>
        <div>
          <Table striped bordered hover className="text-center rounded">
            <thead className="bg-success text-white">
              <tr>
                <td>Title</td>
                <td>Genre</td>
                <td>Language</td>
                <td>Country</td>
                <td>Actions</td>
              </tr>
            </thead>
            <tbody>
              {movies.map((movie) => (
                <tr key={movie.movieId}>
                  <td>{movie.title}</td>
                  <td>{movie.genre}</td>
                  <td>{movie.language}</td>
                  <td>{movie.country}</td>
                  <td className="align-middle text-nowrap">
                    <Button
                      as={Link}
                      to={`/movies/update/${movie.movieId}`}
                      className="me-2"
                    >
                      View detail
                    </Button>
                    {/* <Confirm
                      confirmName="Delete"
                      buttonName="Delete"
                      action={handleDelete}
                      title="Confirm action"
                      value={movie.movieId}
                      variant="danger"
                    >
                      Do you want to delete this movie?
                    </Confirm> */}
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    </>
  );
};

export default MovieManagement;
