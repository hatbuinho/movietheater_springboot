import MovieManagement from './Manage';
import MovieDetails from './Details';
import CreateMovie from './Create';
import Movie from './Movie';

export { CreateMovie, Movie, MovieDetails, MovieManagement };
