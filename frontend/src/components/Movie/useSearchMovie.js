import { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { MovieType } from '../../actions/types';
import useSearch from '../hooks/useSearch';

const movieApi = '/public/movies';

const useSearchMovie = (
  params = {
    query: '',
    pageNumber: 1
  }
) => {
  // const _movies = useSelector((state) => state.movies);
  const [movies, setMovies] = useState();
  params.query = params.query.replace(/\s/g, '%');
  const { result, error, isSearching } = useSearch(
    params,
    'NO_ACTION',
    movieApi
  );

  // useEffect(() => {
  //   setMovies(_movies);
  // }, [_movies]);

  return { movies: result, error, isSearching };
};

export default useSearchMovie;
