import React, { useEffect, useRef, useState } from 'react';
import { createMovie, updateMovie } from '../../actions/movies.action';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import movieService from '../../services/movie/movie.service';
import { handleCommonError } from '../../actions/handleError';

const SaveMovie = () => {
  const { movieId } = useParams();

  const initialState = {
    title: '',
    genre: '',
    releaseDate: new Date().toLocaleDateString(),
    language: '',
    duration: 0,
    country: '',
    description: '',
    smallImage: null,
    largeImage: null
  };

  const dispatch = useDispatch();
  const [movie, setMovie] = useState(initialState);
  const form = useRef();
  const [isLoading, setIsLoading] = useState(false);
  const [isSuccess, setIsSuccess] = useState(false);
  const history = useHistory();
  const { message } = useSelector((state) => state.message);

  useEffect(() => {
    if (!movieId) return;
    movieService
      .findById(movieId)
      .then((res) => {
        const mv = res.data;
        setMovie(mv);
      })
      .catch((err) => {
        handleCommonError(err, dispatch, false);
      });
  }, [dispatch, movieId]);

  const srcToFile = (src, fileName, obj, name) => {
    return fetch(src)
      .then((res) => {
        return res.blob();
      })
      .then((blob) => {
        console.log('convert');
        const file = new File([blob], fileName, { type: 'image/jpeg' });
        obj[name] = file;
      })
      .catch((err) => console.log(err));
  };

  const handleInput = (event) => {
    const { name, value } = event.target;
    setMovie({
      ...movie,
      [name]: value
    });
  };

  const handleInputFile = (event) => {
    if (event.target.type === 'file') {
      const { name, files } = event.target;
      console.log(files[0]);
      setMovie({
        ...movie,
        [name]: files[0]
      });
    }
  };

  const goBack = (e) => {
    history.go(-1);
  };

  const submit = (movie) =>
    movieId
      ? dispatch(updateMovie(movieId, movie))
      : dispatch(createMovie(movie));

  const handleSubmit = (event) => {
    event.preventDefault();

    if (form.current.checkValidity()) {
      setIsLoading(true);

      if (typeof movie.smallImage === 'string') {
        srcToFile(movie.smallImage, 'small.jpg', movie, 'smallImage');
      }
      if (typeof movie.largeImage === 'string') {
        srcToFile(movie.largeImage, 'large.jpg', movie, 'largeImage');
      }

      submit(movie)
        .then(() => {
          setIsLoading(false);
          setIsSuccess(true);
          setTimeout(() => {
            history.replace('/movies');
            history.go(0);
          }, 1500);
        })
        .catch(() => {
          setIsLoading(false);
          setIsSuccess(false);
        });
    } else {
      form.current.classList.add('was-validated');
    }
  };

  return (
    <div className="container">
      <h1>{movieId ? 'Update Movie' : 'Create movie'}</h1>

      <form className="p-3 mb-4" onSubmit={handleSubmit} noValidate ref={form}>
        {/* group 1 */}
        <div className="row row-cols-md-3">
          {/*  title */}
          <div className="mb-3">
            <label htmlFor="title" className="form-label">
              Title
            </label>
            <input
              type="text"
              name="title"
              className="form-control"
              id="title"
              value={movie.title}
              onChange={handleInput}
              required
            />
          </div>
          {/* genre */}
          <div className="mb-3">
            <label htmlFor="genre" className="form-label">
              Genre
            </label>
            <input
              type="text"
              name="genre"
              className="form-control"
              id="genre"
              value={movie.genre}
              onChange={handleInput}
              required
            />
          </div>
          {/* releaseDate */}
          <div className="mb-3">
            <label htmlFor="releaseDate" className="form-label">
              Release Date
            </label>
            <input
              type="date"
              name="releaseDate"
              className="form-control"
              id="releaseDate"
              value={movie.releaseDate}
              onChange={handleInput}
              required
            />
          </div>
        </div>
        <div className="row row-cols-md-3">
          {/* language */}
          <div className="mb-3">
            <label htmlFor="language" className="form-label">
              Language
            </label>
            <input
              type="text"
              name="language"
              className="form-control"
              id="language"
              value={movie.language}
              onChange={handleInput}
              required
            />
          </div>

          {/* duration */}
          <div className="mb-3">
            <label htmlFor="duration" className="form-label">
              Duration
            </label>
            <input
              type="text"
              name="duration"
              pattern="\d+"
              className="form-control"
              id="duration"
              value={movie.duration}
              onChange={handleInput}
              required
            />
          </div>

          {/* country */}
          <div className="mb-3">
            <label htmlFor="country" className="form-label">
              Country
            </label>
            <input
              type="text"
              name="country"
              className="form-control"
              id="country"
              value={movie.country}
              onChange={handleInput}
              required
            />
          </div>
        </div>

        {/* group 2 */}
        <div className="row row-cols-md-2">
          {/* small image */}
          <div className="mb-3" style={{ maxHeight: '200px' }}>
            <label htmlFor="smallImage" className="form-label">
              Small image
            </label>
            <input
              type="file"
              name="smallImage"
              accept="image/*"
              className="form-control"
              id="smallImage"
              // value={movie.smallImage}
              onChange={handleInputFile}
              required={movie.smallImage ? false : true}
            />
            <div className="h-100 d-flex justify-content-center">
              <img
                src={movie.smallImage}
                alt=""
                style={{ height: '70%', maxWidth: '100%' }}
                className="my-2"
              />
            </div>
          </div>

          {/* small image */}
          <div className="mb-3">
            <label htmlFor="largeImage" className="form-label">
              Large image
            </label>
            <input
              type="file"
              accept="image/*"
              name="largeImage"
              className="form-control"
              id="largeImage"
              // value={movie.largeImage}
              onChange={handleInputFile}
              required={movie.largeImage ? false : true}
            />
            <div className="d-flex justify-content-center">
              <img
                src={movie.largeImage}
                alt=""
                style={{ height: '70%', maxWidth: '100%' }}
                className="my-2"
              />
            </div>
          </div>
        </div>

        {/* description */}
        <div className="mb-3">
          <label htmlFor="description" className="form-label">
            Description
          </label>
          <textarea
            name="description"
            className="form-control"
            id="description"
            onChange={handleInput}
            value={movie.description}
          />
        </div>

        {/* message */}
        {message && (
          <div
            className={
              'text-center ' +
              (isSuccess ? 'alert alert-success' : 'alert alert-danger')
            }
            role="alert"
          >
            {message}
          </div>
        )}

        <div className="d-flex">
          <Button variant="secondary" className="me-2" onClick={goBack}>
            Back
          </Button>
          <button
            type="submit"
            className="btn btn-primary"
            disabled={isLoading}
          >
            {isLoading && (
              <span className="spinner-border spinner-border-sm"></span>
            )}{' '}
            {movieId ? 'Update movie' : 'Create movie'}
          </button>
        </div>
      </form>
    </div>
  );
};

export default SaveMovie;
