import React, { useEffect, useState } from 'react';
import { useParams, useHistory, Link } from 'react-router-dom';
import movieService from '../../services/movie/movie.service';
import { Button } from 'react-bootstrap';
import './movie.css';
import Loading from '../FullPageLoading/Loading';

const MovieDetails = () => {
  const history = useHistory();
  const { movieId } = useParams();
  const goBack = (event) => {
    history.push('/movies');
  };
  const [movie, setMovie] = useState({});
  const [isLoading, setIsLoading] = useState(true);

  const getMovieById = (id) => {
    movieService
      .findById(id)
      .then((res) => {
        setMovie(res.data);
        setTimeout(() => {
          setIsLoading(false);
        }, 500);
      })
      .catch(() => {
        setIsLoading(false);
      });
  };

  useEffect(() => {
    getMovieById(movieId);
  }, [movieId]);

  const duration = `${Math.floor(movie.duration / 60)}h ${
    movie.duration % 60
  }m`;

  const releaseDate = Date.parse(movie.releaseDate);
  const isFuture = releaseDate > Date.now();

  const getDate = (dateString) => {
    let date = new Date(dateString).toString().split` `.slice(0, 4).join` `;
    return date;
  };

  const releaseDateString = getDate(movie.releaseDate);
  const movieTitle = movie?.title?.replace(/\s/g, '-') || 'mv';
  const currentDate = new Date().toISOString().split`T`[0];
  return (
    <>
      <div className="container-fluid px-0" style={{ minHeight: '81vh' }}>
        <div className="container-fluid" style={{ background: '#222222' }}>
          <section className="container w-100 h-100">
            <div
              className="bg-movie"
              style={{
                backgroundImage: ` linear-gradient(
                    90deg,
                    rgb(34, 34, 34) 24.97%,
                    rgb(34, 34, 34) 38.3%,
                    rgba(34, 34, 34, 0.04) 97.47%,
                    rgb(34, 34, 34) 100%
                  ), url(${movie.largeImage})`
              }}
            >
              {/* movie card */}
              <div className="d-flex flex-grow-1">
                <div className="card bg-dark rounded-3">
                  <img
                    src={movie.smallImage}
                    className="img-card-top rounded"
                    alt={movie.title}
                    style={{ maxHeight: '380px', width: 'auto' }}
                  />
                  <div className="card-body text-white text-center">
                    {isFuture ? `Release in ${releaseDateString}` : 'In cinema'}
                  </div>
                </div>

                {/* movie info */}
                <div
                  className="text-white ms-3 position-relative"
                  style={{ maxWidth: '500px', marginTop: '10%' }}
                >
                  <h1 className="display-3 text-bold">{movie.title}</h1>
                  <div className="mb-2">
                    <div className="badge bg-secondary me-1">
                      {movie.language}
                    </div>
                    <div className="badge bg-secondary me-1">{duration}</div>
                    <div className="badge bg-secondary">{movie.genre}</div>
                  </div>

                  <div className="">
                    <div className="badge bg-secondary me-1">
                      {movie.country}
                    </div>
                    {isFuture ? (
                      ''
                    ) : (
                      <div className="badge bg-secondary">
                        {releaseDateString}
                      </div>
                    )}
                  </div>
                  {isFuture ? (
                    ''
                  ) : (
                    <Button
                      as={Link}
                      to={`/booktickets/${movieTitle}-${movie.movieId}/${currentDate}`}
                      className="btn-danger position-absolute start-0 px-5 py-3` text-nowrap"
                      style={{ bottom: '10%' }}
                    >
                      Book tickets
                    </Button>
                  )}
                  {/* /booktickets/:movieTitle/:date */}
                </div>
              </div>
            </div>
          </section>
        </div>
        <div className="container-fluid mt-4">
          <div className="w-50">
            <h2>About the movie</h2>
            <p>{movie.description}</p>
          </div>
        </div>
      </div>
      {isLoading && <Loading type="TailSpin" />}
    </>
  );
};

export default MovieDetails;
