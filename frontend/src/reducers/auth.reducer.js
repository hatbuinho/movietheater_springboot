import { AuthType } from '../actions/types';

const user = JSON.parse(localStorage.getItem('user'));

console.log('reload in auth reducer');

const initialState = user
  ? { isLoggedIn: true, user }
  : { isLoggedIn: false, user: null };

const auth = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case AuthType.REGISTER_SUCCESS:
      return {
        ...state,
        isLoggedIn: false
      };
    case AuthType.REGISTER_FAIL:
      return {
        ...state,
        isLoggedIn: false
      };
    case AuthType.LOGIN_SUCCESS:
      return {
        ...state,
        isLoggedIn: true,
        user: payload
      };
    case AuthType.LOGIN_FAIL:
      return {
        ...state,
        isLoggedIn: false,
        user: null
      };
    case AuthType.LOGOUT:
      return {
        ...state,
        isLoggedIn: false,
        user: null
      };
    default:
      return state;
  }
};

export default auth;
