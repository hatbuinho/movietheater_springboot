import { CinemaType } from '../actions/types';

const initialState = [];

const cinemas = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CinemaType.CREATE:
      return [...state, payload];

    case CinemaType.UPDATE:
      return state.map((ele) => {
        if (ele.cinemaId == payload.id) {
          return {
            ...state,
            ...payload
          };
        }
        return ele;
      });

    case CinemaType.GET_PAGE:
      return payload;

    case CinemaType.DELETE:
      return state.filter((cinema) => cinema.cinemaId !== payload.id);

    case CinemaType.DELETE_ALL:
      return [];

    default:
      return state;
  }
};

export default cinemas;
