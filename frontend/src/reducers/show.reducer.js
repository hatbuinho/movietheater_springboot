import { ShowType } from '../actions/types';

const initialState = [];

const shows = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case ShowType.CREATE:
      return [...state, payload];

    case ShowType.UPDATE:
      return state.map((ele) => {
        if (ele.showId == payload.showId) {
          return {
            ...ele,
            ...payload
          };
        }
        return ele;
      });

    case ShowType.GET_ALL:
      return payload;

    case ShowType.GET_BY_ID:
      return state.find((show) => show.showId === payload.showId)
        ? state
        : [...state, payload];

    case ShowType.DELETE:
      return state.filter((show) => show.showId !== payload.showId);

    case ShowType.DELETE_ALL:
      return [];

    case ShowType.FIND_BY_MOVIE_AND_DATE:
      return payload;

    default:
      return state;
  }
};

export default shows;
