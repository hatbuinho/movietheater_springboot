import { SeatType } from '../actions/types';

const initialState = [];

const seats = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case SeatType.CREATE:
      return [...state, payload];

    case SeatType.UPDATE:
      return state.map((ele) => {
        if (ele.seatId == payload.id) {
          return {
            ...ele,
            ...payload
          };
        }
        return ele;
      });

    case SeatType.GET_ALL:
      return payload;

    case SeatType.DELETE:
      return state.filter((seat) => seat.seatId !== payload.id);

    case SeatType.DELETE_ALL:
      return [];

    default:
      return state;
  }
};

export default seats;
