import { RoomType, SeatType } from '../actions/types';

const initialState = [];

const rooms = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    // case SeatType.CREATE:
    //   return [...state, payload];

    case RoomType.UPDATE:
      return state.map((ele) => {
        if (ele.roomId == payload.id) {
          return {
            ...state,
            ...payload
          };
        }
        return ele;
      });

    case RoomType.GET_ALL:
      return payload;

    // case SeatType.DELETE:
    //   return state.filter((seat) => seat.seatId !== payload.id);

    // case SeatType.DELETE_ALL:
    //   return [];

    default:
      return state;
  }
};

export default rooms;
