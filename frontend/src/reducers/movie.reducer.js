import { MovieType } from '../actions/types';

const initialState = [];

function movies(movies = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case MovieType.CREATE_MOVIE:
      return [...movies, payload];

    case MovieType.UPDATE_MOVIE:
      return movies.map((movie) => {
        if (movie.movieId == payload.movieId) {
          return {
            ...movie,
            ...payload
          };
        }
        return movie;
      });

    case MovieType.GET_PAGE_MOVIE:
      return payload;

    case MovieType.DELETE_MOVIE:
      return movies.filter((movie) => movie.movieId !== payload.movieId);

    case MovieType.FIND_BY_ID:
      return [payload];
    // return movies.find((movie) => movie.movieId === payload.movieId)
    //   ? movies
    //   : [...movies, payload];

    default:
      return movies;
  }
}

export default movies;
