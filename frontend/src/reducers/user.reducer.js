
import { UserType } from '../actions/types';
const initialState = [];

const users = (state = initialState, action) => {
  const {type, payload} = action;

  switch(type){
    case UserType.FIND_ALL: 
    return payload;

    case UserType.CREATE: 
    return [
      ...state,
      payload
    ]

case UserType.DELETE_BY_ID:
  return state.filter(user => user.userId !== payload.userId);

  case UserType.UPDATE:
    return state.map((user) => {
      if (user.userId == payload.userId) {
        return {
          ...user,
          ...payload
        };
      }
      return user;
    });

    case UserType.FIND_BY_ID:
      return state.find((user) => user.userId == payload.userId)
        ? state
        : [...state, payload];

    default:
      return state;
  }
}

export default users;