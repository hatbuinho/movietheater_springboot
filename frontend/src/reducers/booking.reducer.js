import { BookingType } from '../actions/types';

const initialPreBooking = { selectedSeat: [], quantity: 0 };

const preBooking = (state = initialPreBooking, action) => {
  const { type, payload } = action;

  switch (type) {
    case BookingType.SELECT:
      return state.selectedSeat.find(
        (seat) => seat.showSeatId == payload.showSeatId
      )
        ? state
        : {
            ...state,
            ...{ selectedSeat: [...state.selectedSeat, payload] }
          };

    case BookingType.GET_PRE_BOOKING:
      return state;

    case BookingType.CLEAR_SELETED_SEAT:
      return {
        ...state,
        selectedSeat: []
      };

    case BookingType.SET_QUANTITY:
      return {
        ...state,
        quantity: payload
      };

    default:
      return state;
  }
};

const initialBookings = [];

const bookings = (state = initialBookings, action) => {
  const { type, payload } = action;
  switch (type) {
    case BookingType.CONFIRM_BOOKING:
    case BookingType.CANCEL:
    case BookingType.CHECK_IN:
      return state.map((bk) => {
        if (bk.bookingId === payload.bookingId) {
          return {
            ...state,
            ...payload
          };
        }
        return bk;
      });

    case BookingType.FIND_BY_USER:
      return payload;

    case BookingType.FIND_ALL:
      return payload;

    default:
      return state;
  }
};

const initialBooking = {};

const booking = (state = initialBooking, action) => {
  const { type, payload } = action;
  switch (type) {
    case BookingType.BOOK_TICKETS:
      return payload;

    default:
      return state;
  }
};

export { preBooking, bookings, booking };
