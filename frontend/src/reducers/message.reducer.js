import { MessageType } from '../actions/types';

const initialState = {message: '', status: 200};

const message = (message = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case MessageType.SET_MESSAGE:
      return {
        ...message,
        ...payload
      };
    case MessageType.CLEAR_MESSAGE:
      return {
        ...initialState
      };

    default:
      return message;
  }
};

export default message;
