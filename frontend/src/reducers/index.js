import { combineReducers } from 'redux';
import movies from './movie.reducer';
import auth from './auth.reducer';
import message from './message.reducer';
import seats from './seat.reducer';
import rooms from './room.reducer';
import { preBooking, booking } from './booking.reducer';
import shows from './show.reducer';
import users from './user.reducer';
import { bookings } from './booking.reducer';

export default combineReducers({
  movies,
  auth,
  message,
  seats,
  rooms,
  preBooking,
  booking,
  shows,
  users,
  bookings
});
