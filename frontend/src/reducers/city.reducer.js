import { CityType } from '../actions/types';

const initialState = [];

const cities = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case CityType.CREATE:
      return [...state, payload];

    case CityType.UPDATE:
      return state.map((ele) => {
        if (ele.cityId == payload.id) {
          return {
            ...state,
            ...payload
          };
        }
        return ele;
      });

    case CityType.GET_PAGE:
      return payload;

    case CityType.DELETE:
      return state.filter((city) => city.cityId !== payload.id);

    case CityType.DELETE_ALL:
      return [];

    default:
      return state;
  }
};

export default cities;
