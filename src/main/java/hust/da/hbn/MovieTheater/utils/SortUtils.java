package hust.da.hbn.MovieTheater.utils;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public class SortUtils {
    public static Pageable sortedByDateDescending(String pageNumber) {
        int intPageNumber = Convertor.convertPageNumber(pageNumber);
        return PageRequest
                .of(Constants.FIRST_PAGE,
                        Constants.PAGE_SIZE * intPageNumber,
                        Sort
                                .by("date")
                                .descending()
                );
    }

    public static Pageable sortedAndPagingByCreatedDateAndTimeDescending(String pageNumber) {
        int intPageNumber = Convertor.convertPageNumber(pageNumber);
        return PageRequest.of(
                Constants.FIRST_PAGE,
                Constants.PAGE_SIZE * intPageNumber,
                Sort.by(Sort.Order.desc("createdDate"), Sort.Order.desc("createdTime"))
        );
    }
}
