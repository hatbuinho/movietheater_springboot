package hust.da.hbn.MovieTheater.utils;

public class Constants {
    public static final int FIRST_PAGE = 0;
    public static final int PAGE_SIZE = 12;
    public static final int ONE_PAGE = 1;
    public static final int TIME_BEFORE_CANCEL_UNPAID_BOOKING = 10 * 1000 * 1000; // 10 minutes
    public static final String WHOLE_PAGE = "0";
}
