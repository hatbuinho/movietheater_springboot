package hust.da.hbn.MovieTheater.utils;

import hust.da.hbn.MovieTheater.enums.BookingStatus;
import hust.da.hbn.MovieTheater.exceptions.InvalidFormatException;
import org.apache.tomcat.jni.Local;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;


public class Convertor {
    public static LocalDate convertToLocalDate(String dateString) {
        String datePattern = "yyyy-MM-dd";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(datePattern);
        LocalDate date;
        try {
            date = LocalDate.parse(dateString, formatter);
        } catch (Exception e) {
            throw new InvalidFormatException("Invalid date");
        }
        return date;
    }

    public static LocalTime convertToLocalTime(String timeString) {
        String timePattern = "HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(timePattern);

        LocalTime time;
        try {
            time = LocalTime.parse(timeString, formatter);
        } catch (Exception e) {
            throw new InvalidFormatException("Invalid time format");
        }
        return time;
    }

    public static BigDecimal convertToBigDecimal(String moneyString) {
        return BigDecimal.valueOf(Double.parseDouble(moneyString));
    }

    public static int convertPageNumber(String pageNumber) {
        int intPageNumber;
        try {
            intPageNumber = Integer.parseInt(pageNumber);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid page number");
        }
        return intPageNumber;
    }
    

    public static BookingStatus convertToBookingStatus(String bookingStatus) {
        BookingStatus status;
        try {
            status = BookingStatus.valueOf(bookingStatus);
        } catch (Exception e) {
            throw new InvalidFormatException("Invalid booking status");
        }
        return status;
    }
}
