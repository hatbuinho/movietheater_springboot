package hust.da.hbn.MovieTheater.enums;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_EMPLOYEE,
    ROLE_MEMBER
}
