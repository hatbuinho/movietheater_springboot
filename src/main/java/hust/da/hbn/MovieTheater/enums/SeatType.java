package hust.da.hbn.MovieTheater.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum SeatType {
    CLASSIC(1),
    ROYAL(1.5);

    private double ratio;
}
