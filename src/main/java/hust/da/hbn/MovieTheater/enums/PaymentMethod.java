package hust.da.hbn.MovieTheater.enums;

public enum PaymentMethod {
    MOBILE,
    CREDIT_CARD
}
