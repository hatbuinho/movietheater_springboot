package hust.da.hbn.MovieTheater.enums;

public enum UserStatus {
    ACTIVE,
    BLOCKED
}
