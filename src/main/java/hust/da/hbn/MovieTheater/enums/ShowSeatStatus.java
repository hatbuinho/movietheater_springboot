package hust.da.hbn.MovieTheater.enums;

public enum ShowSeatStatus {
    AVAILABLE,
    RESERVED
}
