package hust.da.hbn.MovieTheater.enums;

public enum BookingStatus {
    REQUESTED,
    CONFIRMED,
    CHECKED_IN,
    CANCELLED
}
