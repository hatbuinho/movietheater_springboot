package hust.da.hbn.MovieTheater.repository.later;

import hust.da.hbn.MovieTheater.entities.later.Cinema;
import hust.da.hbn.MovieTheater.entities.later.City;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface CinemaRepository extends JpaRepository<Cinema, Long> {
    @Query("SELECT c FROM Cinema c WHERE c.name LIKE %?1%")
    List<Cinema> findByNameEnhance(String name, Pageable pageable);

    Optional<Cinema> findByNameAndCity(String name, City city);


}
