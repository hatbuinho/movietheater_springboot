package hust.da.hbn.MovieTheater.repository;

import hust.da.hbn.MovieTheater.entities.Role;
import hust.da.hbn.MovieTheater.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName name);
}

