package hust.da.hbn.MovieTheater.repository;

import hust.da.hbn.MovieTheater.entities.Movie;
import hust.da.hbn.MovieTheater.entities.Show;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ShowRepository extends JpaRepository<Show, Long> {
//    List<Show> findByMovieAndRoom(Movie movie, Room room, Pageable pageable);

    List<Show> findByMovie(Movie movie, Pageable pageable);

    Optional<Show> findByDateAndStartTime(LocalDate date, LocalTime startTime);

    List<Show> findByDateAndMovie(LocalDate date, Movie movie);
}
