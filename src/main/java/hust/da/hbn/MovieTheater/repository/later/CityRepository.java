package hust.da.hbn.MovieTheater.repository.later;

import hust.da.hbn.MovieTheater.entities.later.City;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


import java.util.List;
import java.util.Optional;

@Repository
public interface CityRepository extends JpaRepository<City, Long> {
    @Query("SELECT c FROM City c WHERE c.name LIKE %?1%")
    List<City> findByNameEnhance(String name, Pageable pageable);

    Optional<City> findByName(String name);
}

