package hust.da.hbn.MovieTheater.repository;

import hust.da.hbn.MovieTheater.entities.Booking;
import hust.da.hbn.MovieTheater.entities.Movie;
import hust.da.hbn.MovieTheater.entities.Show;
import hust.da.hbn.MovieTheater.entities.User;
import hust.da.hbn.MovieTheater.enums.BookingStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface BookingRepository extends JpaRepository<Booking, Long> {
    List<Booking> findByUserAndStatusNot(User user, BookingStatus status, Sort sort);

    List<Booking> findByStatusNot(BookingStatus status, Pageable pageable);

    Optional<Booking> findByBookingCode(String bookingCode);


    List<Booking> findByStatus(BookingStatus status, Sort sort);

    @Query("SELECT b FROM Booking b " +
            "JOIN b.show s " +
            "WHERE (:status is null or b.status = :status) " +
            "AND (:movie is null or s.movie = :movie) " +
            "AND (:createdDate is null or b.createdDate = :createdDate)")
    List<Booking> filterAndPagingByMovieAndStatusAndCreatedDate(
            @Param("movie") Movie movie,
            @Param("status") BookingStatus status,
            @Param("createdDate") LocalDate createdDate,
            Pageable pageable
    );
}
