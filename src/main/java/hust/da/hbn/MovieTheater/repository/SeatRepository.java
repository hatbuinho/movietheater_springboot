package hust.da.hbn.MovieTheater.repository;

import hust.da.hbn.MovieTheater.entities.Room;
import hust.da.hbn.MovieTheater.entities.Seat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SeatRepository extends JpaRepository<Seat, Long> {
//    List<Seat> findByRoom(Room room);

    Optional<Seat> findByRowAndColAndRoom(Integer row, Integer col,Room room);
}
