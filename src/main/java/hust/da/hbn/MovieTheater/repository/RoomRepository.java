package hust.da.hbn.MovieTheater.repository;

import hust.da.hbn.MovieTheater.entities.Room;
import hust.da.hbn.MovieTheater.entities.later.Cinema;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RoomRepository extends JpaRepository<Room, Long> {
//    @Query("SELECT r FROM Room r WHERE r.name LIKE %?1%")
//    List<Room> findByNameEnhance(String name, Pageable pageable);

//    Optional<Room> findByNameAndCinema(String name, Cinema cinema);
}
