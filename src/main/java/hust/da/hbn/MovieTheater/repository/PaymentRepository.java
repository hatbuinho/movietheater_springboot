package hust.da.hbn.MovieTheater.repository;

import hust.da.hbn.MovieTheater.entities.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
    @Query("select p from Payment p " +
            "where (:year is null or function('year', p.createdDate) = :year) " +
            "and (:month is null or function('month', p.createdDate) = :month)")
    List<Payment> findByYearAndMonth(@Param("year") Integer year, @Param("month") Integer month);
}
