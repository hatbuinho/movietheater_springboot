package hust.da.hbn.MovieTheater.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import hust.da.hbn.MovieTheater.entities.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    
    @Query("SELECT m FROM Movie m WHERE m.title LIKE %?1%")
    Page<Movie> findByTitleEnhance(String title, Pageable pageable);
}
