package hust.da.hbn.MovieTheater.repository;

import hust.da.hbn.MovieTheater.entities.Contact;
import hust.da.hbn.MovieTheater.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {
    Optional<Contact> findByUser(User user);
}
