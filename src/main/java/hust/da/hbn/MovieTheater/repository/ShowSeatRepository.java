package hust.da.hbn.MovieTheater.repository;

import hust.da.hbn.MovieTheater.entities.Show;
import hust.da.hbn.MovieTheater.entities.ShowSeat;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ShowSeatRepository extends JpaRepository<ShowSeat, Long> {
    List<ShowSeat> findByShow(Show show);
}
