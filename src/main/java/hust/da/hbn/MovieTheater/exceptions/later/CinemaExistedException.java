package hust.da.hbn.MovieTheater.exceptions.later;

public class CinemaExistedException extends RuntimeException {
    public CinemaExistedException() {
        super();
    }

    public CinemaExistedException(String message) {
        super(message);
    }
}
