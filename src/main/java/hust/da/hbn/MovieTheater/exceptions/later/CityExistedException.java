package hust.da.hbn.MovieTheater.exceptions.later;

public class CityExistedException extends RuntimeException {
    public CityExistedException() {
        super();
    }

    public CityExistedException(String message) {
        super(message);
    }
}
