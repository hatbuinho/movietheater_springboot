package hust.da.hbn.MovieTheater.exceptions;

public class ShowSeatExistedException extends RuntimeException{
    public ShowSeatExistedException() {
        super();
    }

    public ShowSeatExistedException(String message) {
        super(message);
    }
}
