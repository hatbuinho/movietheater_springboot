package hust.da.hbn.MovieTheater.exceptions;

public class BookingExistedException extends RuntimeException{
    public BookingExistedException() {
        super();
    }

    public BookingExistedException(String message) {
        super(message);
    }
}
