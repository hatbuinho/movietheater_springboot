package hust.da.hbn.MovieTheater.exceptions;

public class UserExistedException extends RuntimeException {
    public UserExistedException() {
        super();
    }

    public UserExistedException(String message) {
        super(message);
    }
}
