package hust.da.hbn.MovieTheater.exceptions.advice;

import hust.da.hbn.MovieTheater.exceptions.*;
import hust.da.hbn.MovieTheater.exceptions.later.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import hust.da.hbn.MovieTheater.dto.response.ResponseMessage;

@RestControllerAdvice
public class EntityControllerAdvice {

    // handle cinema
    @ExceptionHandler(CinemaNotFoundException.class)
    public ResponseEntity<ResponseMessage> handleCinemaNotFound(CinemaNotFoundException ex) {
        String message = "This Cinema is not be found";
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
    }

    @ExceptionHandler(CinemaExistedException.class)
    public ResponseEntity<ResponseMessage> handleCinemaExisted(CinemaExistedException ex) {
        String message = "This Cinema is existed";
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage(message));
    }

    // handle room
    @ExceptionHandler(RoomExistedException.class)
    public ResponseEntity<ResponseMessage> handleRoomExisted(RoomExistedException ex) {
        String message = "This Room is existed";
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage(message));
    }

    @ExceptionHandler(RoomNotFoundException.class)
    public ResponseEntity<ResponseMessage> handleRoomNotFound(RoomNotFoundException ex) {
        String message = "This Room is not be found";
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
    }

    // handle city
    @ExceptionHandler(CityExistedException.class)
    public ResponseEntity<ResponseMessage> handleCityExisted(CityExistedException ex) {
        String message = "This City is existed";
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage(message));
    }

    @ExceptionHandler(CityNotFoundException.class)
    public ResponseEntity<ResponseMessage> handleCityNotFound(CityNotFoundException ex) {
        String message = "This City is not be found";
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
    }

    // handle show
    @ExceptionHandler(ShowExistedException.class)
    public ResponseEntity<ResponseMessage> handleShowExisted(ShowExistedException ex) {
        String message = "This Show is existed";
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage(message));
    }

    @ExceptionHandler(ShowNotFoundException.class)
    public ResponseEntity<ResponseMessage> handleShowNotFound(ShowNotFoundException ex) {
        String message = "This Show is not be found";
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
    }

    // handle movie
    @ExceptionHandler(MovieExistedException.class)
    public ResponseEntity<ResponseMessage> handleMovieExisted(MovieExistedException ex) {
        String message = "This Movie is existed";
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage(message));
    }

    @ExceptionHandler(MovieNotFoundException.class)
    public ResponseEntity<ResponseMessage> handleMovieNotFound(MovieNotFoundException ex) {
        String message = "This Movie is not be found";
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
    }

    // handle seat
    @ExceptionHandler(SeatExistedException.class)
    public ResponseEntity<ResponseMessage> handleMovieNotFound(SeatExistedException ex) {
        String message = "This Seat is existed";
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
    }

    @ExceptionHandler(SeatNotFoundException.class)
    public ResponseEntity<ResponseMessage> handleIIllegalArgument(SeatNotFoundException ex) {

        String message = "This Seat is not be found";
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage(message));
    }

    // handle booking
    @ExceptionHandler(BookingExistedException.class)
    public ResponseEntity<ResponseMessage> handleMovieNotFound(BookingExistedException ex) {
        String message = "This Booking is existed";
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
    }

    @ExceptionHandler(BookingNotFoundException.class)
    public ResponseEntity<ResponseMessage> handleIIllegalArgument(BookingNotFoundException ex) {

        String message = "This Booking is not be found";
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage(message));
    }

    // handle Show seat
    @ExceptionHandler(ShowSeatExistedException.class)
    public ResponseEntity<ResponseMessage> handleMovieNotFound(ShowSeatExistedException ex) {
        String message = ex.getLocalizedMessage();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
    }

    @ExceptionHandler(ShowSeatNotAvailableException.class)
    public ResponseEntity<ResponseMessage> handleShowSeatNotAvailableException(ShowSeatNotAvailableException ex) {
        String message = ex.getLocalizedMessage();
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage(message));
    }

    // handle user
    @ExceptionHandler(UserExistedException.class)
    public ResponseEntity<ResponseMessage> handleMovieNotFound(UserExistedException ex) {
        String message = ex.getLocalizedMessage() != null ? ex.getLocalizedMessage() : "This User is existed";
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<ResponseMessage> handleIIllegalArgument(UserNotFoundException ex) {

        String message = "This User is not be found";
        return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage(message));
    }


    //=====================================INTERNAL EXCEPTION==================================================

    // handle internal
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ResponseMessage> handleCommonException(RuntimeException ex) {
        ex.printStackTrace();
        String message = "Some error has occurred in our scripts";
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ResponseMessage(message));
    }

}
