package hust.da.hbn.MovieTheater.exceptions;

public class MovieExistedException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MovieExistedException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public MovieExistedException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
}
