package hust.da.hbn.MovieTheater.exceptions;

public class ShowExistedException extends RuntimeException{
    public ShowExistedException() {
        super();
    }

    public ShowExistedException(String message) {
        super(message);
    }
}
