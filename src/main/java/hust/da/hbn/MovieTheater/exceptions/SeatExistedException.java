package hust.da.hbn.MovieTheater.exceptions;

public class SeatExistedException extends RuntimeException{
    public SeatExistedException() {
        super();
    }

    public SeatExistedException(String message) {
        super(message);
    }
}
