package hust.da.hbn.MovieTheater.exceptions.advice;

import hust.da.hbn.MovieTheater.dto.response.ResponseMessage;

import hust.da.hbn.MovieTheater.exceptions.InvalidFormatException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


import org.springframework.security.access.AccessDeniedException;


@RestControllerAdvice
public class CommonControllerAdvice {


    // handle invalid argument
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ResponseMessage> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        // TODO Auto-generated method stub
        String errorMessage = ex.getBindingResult().getFieldError().getDefaultMessage();
        return ResponseEntity
                .status(HttpStatus.EXPECTATION_FAILED)
                .body(new ResponseMessage(errorMessage));
    }

    // handle invalid format
    @ExceptionHandler(InvalidFormatException.class)
    public ResponseEntity<ResponseMessage> handleInvalidFormatException(InvalidFormatException ex) {
        String message = ex.getLocalizedMessage();
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseMessage(message));
    }

    // handle Illegal Argument
    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ResponseMessage> handleIllegalArgumentException(IllegalArgumentException ex) {
        String message = ex.getLocalizedMessage();
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
    }


    // handle username password incorrect
    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ResponseMessage> handleBadCredentialException(BadCredentialsException ex) {
        String message = "Username or password is incorrect";
        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
    }


    @ExceptionHandler(LockedException.class)
    public ResponseEntity<ResponseMessage> handleBadCredentialException(LockedException ex) {
        String message = "Your account is blocked";
        return ResponseEntity.status(HttpStatus.LOCKED).body(new ResponseMessage(message));
    }


    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ResponseMessage> handleAccessDeniedException(AccessDeniedException ex) {
        String message = "Error: Unauthorized";
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ResponseMessage(message));
    }
}
