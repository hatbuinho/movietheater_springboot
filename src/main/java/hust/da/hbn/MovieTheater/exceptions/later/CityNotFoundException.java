package hust.da.hbn.MovieTheater.exceptions.later;

public class CityNotFoundException extends RuntimeException {
    public CityNotFoundException() {
        super();
    }

    public CityNotFoundException(String message) {
        super(message);
    }
}
