package hust.da.hbn.MovieTheater.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hust.da.hbn.MovieTheater.enums.ShowSeatStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "show_seat")
public class ShowSeat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long showSeatId;


    @Column
    @Enumerated(EnumType.STRING)
    private ShowSeatStatus status = ShowSeatStatus.AVAILABLE;

    @Column(name = "list_price", columnDefinition = "money")
    private BigDecimal listPrice;

    @ManyToOne
    @JoinColumn(name = "cinema_seat_id", referencedColumnName = "cinema_seat_id", nullable = false)
    private Seat seat;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "show_id", referencedColumnName = "show_id", nullable = false)
    private Show show;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "booking_id", referencedColumnName = "booking_id")
    private Booking booking;

}
