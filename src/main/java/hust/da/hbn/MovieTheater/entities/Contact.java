package hust.da.hbn.MovieTheater.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

import hust.da.hbn.MovieTheater.enums.Gender;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "contact")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "contact_id")
    private Long contactId;

    @NonNull
    @Column
    private String phone;

    @NonNull
    @Column
    private String fullName;

    @NonNull
    @Column
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @NonNull
    @Column(name = "date_of_birth")
    private LocalDate dateOfBirth;

    @NonNull
    @Column
    private String address;


    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false, unique = true)
    private User user;
}
