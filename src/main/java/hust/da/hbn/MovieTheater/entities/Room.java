package hust.da.hbn.MovieTheater.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "cinema_room")
public class Room {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cinema_room_id")
    private Long roomId;

    @Column
    private String name;

    @Column(columnDefinition = "tinyint", nullable = false)
    private Integer rowSize;

    @Column(columnDefinition = "tinyint", nullable = false)
    private Integer colSize;

//    @JsonIgnore
//    @ManyToOne
//    @JoinColumn(name = "cinema_id", referencedColumnName = "cinema_id", nullable = false)
//    private Cinema cinema;
//
    @OneToMany(mappedBy = "room", fetch = FetchType.EAGER)
    private Set<Seat> seats;

//    @ManyToMany(mappedBy = "room")
//    private Set<Show> shows;
}
