package hust.da.hbn.MovieTheater.entities.later;

import lombok.*;

import javax.persistence.*;
import java.util.Set;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@EqualsAndHashCode
@Table(name = "city")
public class City {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "city_id")
    private Long cityId;

    @Column(nullable = false)
    private String name;

    @Column(name = "zip_code", nullable = false)
    private String zipCode;

    @OneToMany(mappedBy = "city")
    private Set<Cinema> cinemas;
}
