package hust.da.hbn.MovieTheater.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hust.da.hbn.MovieTheater.enums.BookingStatus;
import hust.da.hbn.MovieTheater.utils.Generator;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "booking")
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "booking_id")
    private Long bookingId;

    @Column(name = "number_of_seat", nullable = false)
    private Integer numberOfSeat;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private BookingStatus status;

    @Setter(AccessLevel.NONE)
    @Column(columnDefinition = "date")
    private LocalDate createdDate = LocalDate.now();

    @Setter(AccessLevel.NONE)
    @Column(columnDefinition = "time")
    private LocalTime createdTime = LocalTime.now();

    @Setter(AccessLevel.NONE)
    @Column
    private String bookingCode = Generator.generateRandomString("BK");


    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
    private User user;


    @ManyToOne
    @JoinColumn(name = "show_id", referencedColumnName = "show_id", nullable = false)
    private Show show;


    @OneToOne(mappedBy = "booking")
    private Payment payment;

    @OneToMany(mappedBy = "booking", cascade = CascadeType.PERSIST)
    private Set<ShowSeat> showSeats;
}
