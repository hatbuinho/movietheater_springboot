package hust.da.hbn.MovieTheater.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hust.da.hbn.MovieTheater.enums.UserStatus;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;


@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "users")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id")
    private Long userId;

    @NonNull
    @Column(unique = true, nullable = false)
    private String username;

    @JsonIgnore
    @NonNull
    @Column(nullable = false)
    private String password;

    @NonNull
    @Email
    @Column(unique = true, nullable = false)
    private String email;

    @Enumerated(EnumType.STRING)
    @Column
    private UserStatus status = UserStatus.ACTIVE;

    @Setter(AccessLevel.NONE)
    @Column(columnDefinition = "date")
    private LocalDate createdDate = LocalDate.now();

    @Setter(AccessLevel.NONE)
    @Column(columnDefinition = "time")
    private LocalTime createdTime = LocalTime.now();

    @ManyToMany
    @JoinTable(
            name = "user_role",
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "role_id"
            ),
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "user_id"
            )
    )
    private Set<Role> roles;

    @JsonIgnore
    @OneToMany(mappedBy = "user")
    private Set<Booking> bookings;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Contact contact;

}
