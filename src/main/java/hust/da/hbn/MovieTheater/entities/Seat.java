package hust.da.hbn.MovieTheater.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import hust.da.hbn.MovieTheater.enums.SeatType;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "cinema_seat", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"row", "col", "cinema_room_id"})
})
public class Seat {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cinema_seat_id")
    private Long seatId;

    //    @Column
//    private String name;

    @Column(nullable = false)
    private Integer row;

    @Column(nullable = false)
    private Integer col;

    @Enumerated(EnumType.STRING)
    @Column
    private SeatType type;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "cinema_room_id", referencedColumnName = "cinema_room_id", nullable = false)
    private Room room;

    @JsonIgnore
    @OneToMany(mappedBy = "seat", cascade = CascadeType.ALL)
    private Set<ShowSeat> showSeats;
}
