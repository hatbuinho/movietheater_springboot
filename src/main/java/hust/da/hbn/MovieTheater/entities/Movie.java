package hust.da.hbn.MovieTheater.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "movie")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private Long movieId;


    @Column(nullable = false)
    private String title;


    @Column(length = 512)
    private String description;


    @Column(nullable = false)
    private Integer duration;


    @Column(name = "release_date", columnDefinition = "date")
    private LocalDate releaseDate;


    @Column(nullable = false)
    private String genre;


    @Column(name = "small_image", nullable = false)
    private String smallImage;


    @Column(name = "large_image", nullable = false)
    private String largeImage;


    @Column
    private String language;


    @Column
    private String country;

    @JsonIgnore
    @OneToMany(mappedBy = "movie")
    private Set<Show> shows;


}
