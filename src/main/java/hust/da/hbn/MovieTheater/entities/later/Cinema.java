package hust.da.hbn.MovieTheater.entities.later;

import com.fasterxml.jackson.annotation.JsonIgnore;

import hust.da.hbn.MovieTheater.entities.Room;
import hust.da.hbn.MovieTheater.entities.later.City;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "cinema")
public class Cinema {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cinema_id")
    private Long cinemaId;

    @Column(nullable = false)
    private String name;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "city_id", referencedColumnName = "city_id", nullable = false)
    private City city;

//    @OneToMany(mappedBy = "cinema")
//    private Set<Room> rooms;

}
