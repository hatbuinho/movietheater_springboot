package hust.da.hbn.MovieTheater.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "show")
public class Show {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "show_id")
    private Long showId;

    @Column(columnDefinition = "date", nullable = false)
    private LocalDate date;

    @Column(columnDefinition = "time", nullable = false)
    private LocalTime startTime;

    @Column(columnDefinition = "time", nullable = false)
    private LocalTime endTime;


    @ManyToOne
    @JoinColumn(name = "movie_id", referencedColumnName = "movie_id", nullable = false)
    private Movie movie;

    @Column(name = "list_price", columnDefinition = "money")
    private BigDecimal listPrice;

//    @ManyToMany(mappedBy = "shows")
//    private Set<Room> rooms;

    @JsonIgnore
    @OneToMany(mappedBy = "show")
    private Set<Booking> bookings;

    @JsonIgnore
    @OneToMany(mappedBy = "show", cascade = CascadeType.ALL)
    private Set<ShowSeat> showSeats;
}
