package hust.da.hbn.MovieTheater.controller;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import hust.da.hbn.MovieTheater.file.upload.model.FileInfo;
import hust.da.hbn.MovieTheater.services.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api")
public class FilesController {
    @Autowired
    FileStorageService fileStorageService;

    @PostMapping("/upload")
    public ResponseEntity<?> upload(@RequestParam(value = "file", required = false) MultipartFile file) {
        try {
            if (file == null) {
                throw new IllegalArgumentException("File is null!");
            }
            String fileName = fileStorageService.save(file);

            String url = MvcUriComponentsBuilder.
                    fromMethodName(FilesController.class, "getFile", fileName)
                    .build().toString();
            FileInfo fileInfo = new FileInfo(fileName, url);


            return ResponseEntity.ok(fileInfo);

/*            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return ResponseEntity.ok(new ResponseMessage(message));*/
        } catch (Exception e) {
           /* message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));*/
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new FileInfo());
        }
    }

    @GetMapping("/files")
    public ResponseEntity<List<FileInfo>> getAllFiles() {
        List<FileInfo> fileInfos = fileStorageService.loadAll().map(path -> {
            String fileName = path.getFileName().toString();
            String url = MvcUriComponentsBuilder.
                    fromMethodName(FilesController.class, "getFile", fileName.toString())
                    .build().toString();
            return new FileInfo(fileName, url);
        }).collect(Collectors.toList());
        return ResponseEntity.ok(fileInfos);
    }

    @GetMapping("/files/{filename:.+}")
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        try {

            Resource file = fileStorageService.load(filename);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

    }
}
