package hust.da.hbn.MovieTheater.controller;

import hust.da.hbn.MovieTheater.entities.ShowSeat;
import hust.da.hbn.MovieTheater.services.ShowSeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class ShowSeatController {
    @Autowired
    private ShowSeatService showSeatService;

    @PreAuthorize("hasRole('MEMBER')")
    @GetMapping("/shows/{showId}/seats")
    public ResponseEntity<?> findShowSeatByShow(@PathVariable(name = "showId") String showId) {
        List<ShowSeat> showSeatList = showSeatService.findByShowId(showId);
        return ResponseEntity.ok(showSeatList);
    }
}
