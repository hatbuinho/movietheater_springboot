package hust.da.hbn.MovieTheater.controller;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import hust.da.hbn.MovieTheater.dto.request.MovieRequest;
import hust.da.hbn.MovieTheater.dto.response.ResponseMessage;
import hust.da.hbn.MovieTheater.entities.Movie;
import hust.da.hbn.MovieTheater.exceptions.MovieNotFoundException;
import hust.da.hbn.MovieTheater.services.FileStorageService;
import hust.da.hbn.MovieTheater.services.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/")
public class MoviesController {
    @Autowired
    private MovieService movieService;

    @Autowired
    private FileStorageService fileStorageService;


    @GetMapping("/public/movies")
    public ResponseEntity<?> getAllMovies(@RequestParam(required = false, name = "query") String title,
                                          @RequestParam(required = false, name = "pageNumber") String pageNumber) {
        List<Movie> movies = movieService.getAll(title, pageNumber);
        return ResponseEntity.ok(movies);
    }

    @GetMapping("/public/movies/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable("id") String id) {
        Movie movie = movieService.findById(id);
        return ResponseEntity.ok(movie);
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @PostMapping("/movies")
    public ResponseEntity<?> createMovie(@ModelAttribute @Valid MovieRequest movieRequest) {
        try {
            Movie movie = movieService.create(movieRequest);
            return ResponseEntity
                    .status(HttpStatus.CREATED)
                    .body(movie);
        } catch (IOException e) {
            return ResponseEntity
                    .status(HttpStatus.EXPECTATION_FAILED)
                    .body(new ResponseMessage("Missing image"));
        }
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @PutMapping("/movies/{id}")
    public ResponseEntity<?> updateMovie(@PathVariable("id") String id, @ModelAttribute MovieRequest movieRequest) {
        try {
            Movie _movie = movieService.update(id, movieRequest);
            return ResponseEntity.ok(_movie);
        } catch (IOException e) {
            return ResponseEntity
                    .status(HttpStatus.EXPECTATION_FAILED)
                    .body(new ResponseMessage("Missing image"));
        }
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @DeleteMapping("/movies/{id}")
    public ResponseEntity<?> deleteMovie(@PathVariable("id") String id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @DeleteMapping("/movies")
    public ResponseEntity<?> deleteAllMovies() {
        movieService.deleteAll();
        return ResponseEntity.noContent().build();
    }
}
