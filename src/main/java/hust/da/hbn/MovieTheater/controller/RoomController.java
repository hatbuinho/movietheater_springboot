package hust.da.hbn.MovieTheater.controller;

import hust.da.hbn.MovieTheater.dto.request.RoomRequest;
import hust.da.hbn.MovieTheater.entities.Room;
import hust.da.hbn.MovieTheater.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/api/")
@RestController
public class RoomController {
    @Autowired
    private RoomService roomService;

    @PostMapping("/rooms")
    public ResponseEntity<?> create(@RequestBody @Valid RoomRequest roomRequest) {
        Room room = roomService.create(roomRequest);
        return ResponseEntity.ok(room);
    }


    @GetMapping("/public/rooms")
    public ResponseEntity<?> findByName(@RequestParam(required = false, name = "query") String name,
                                        @RequestParam(required = false, name = "pageNumber") String numberOfPage) {
        return ResponseEntity.ok(roomService.findAll(name, numberOfPage));
    }

    @GetMapping("/rooms/{id}")
    public ResponseEntity<?> findById(@PathVariable String id) {
        Room city = roomService.findById(id);
        return ResponseEntity.ok(city);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/rooms/{id}")
    public ResponseEntity<?> updateById(@RequestBody @Valid RoomRequest roomRequest,
                                        @PathVariable String id) {
        Room room = roomService.update(id, roomRequest);
        return ResponseEntity.ok(room);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/rooms/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") String id) {
        roomService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/rooms")
    public ResponseEntity<?> deleteAllCities() {
        roomService.deleteAll();
        return ResponseEntity.noContent().build();
    }
}
