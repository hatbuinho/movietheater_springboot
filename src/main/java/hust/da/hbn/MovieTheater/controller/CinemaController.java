package hust.da.hbn.MovieTheater.controller;


import hust.da.hbn.MovieTheater.dto.request.CinemaRequest;
import hust.da.hbn.MovieTheater.entities.later.Cinema;
import hust.da.hbn.MovieTheater.services.CinemaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/api")
public class CinemaController {
    @Autowired
    private CinemaService cinemaService;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/cinemas")
    public ResponseEntity<?> createCinema(@RequestBody @Valid CinemaRequest cinemaRequest) {
        Cinema cinema = cinemaService.create(cinemaRequest);
        return ResponseEntity.ok(cinema);
    }

    @GetMapping("/cinemas")
    public ResponseEntity<?> findByName(@RequestParam(required = false, name = "query") String name,
                                        @RequestParam(required = false, name = "pageNumber") String numberOfPage) {
        return ResponseEntity.ok(cinemaService.findAll(name, numberOfPage));

    }

    @GetMapping("/cinemas/{id}")
    public ResponseEntity<?> findById(@PathVariable String id) {
        Cinema cinema = cinemaService.findById(id);
        return ResponseEntity.ok(cinema);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/cinemas/{id}")
    public ResponseEntity<?> updateById(@RequestBody @Valid CinemaRequest cinemaRequest,
                                        @PathVariable String id) {
        Cinema city = cinemaService.update(id, cinemaRequest);
        return ResponseEntity.ok(city);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/cinemas/{id}")
    public ResponseEntity<?> deleteCity(@PathVariable("id") String id) {
        cinemaService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/cinemas")
    public ResponseEntity<?> deleteAllCities() {
        cinemaService.deleteAll();
        return ResponseEntity.noContent().build();
    }
}
