package hust.da.hbn.MovieTheater.controller;


import hust.da.hbn.MovieTheater.dto.request.PaymentRequest;
import hust.da.hbn.MovieTheater.entities.Payment;
import hust.da.hbn.MovieTheater.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class PaymentController {
    @Autowired
    private PaymentService paymentService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/payments")
    public ResponseEntity<?> findPaymentByYearAndMonth(@RequestParam(name = "year", required = false) String year,
                                                       @RequestParam(name = "month", required = false) String month) {
        List<Payment> paymentList = paymentService.findByYearAndMonth(year, month);
        return ResponseEntity.ok(paymentList);
    }

    @PreAuthorize("hasRole('MEMBER')")
    @PostMapping("/payments")
    public ResponseEntity<?> createPayment(@RequestBody @Valid PaymentRequest req) {
        Payment payment = paymentService.create(req);
        return ResponseEntity.ok(payment);
    }
}
