package hust.da.hbn.MovieTheater.controller;


import hust.da.hbn.MovieTheater.dto.request.CityRequest;
import hust.da.hbn.MovieTheater.entities.later.City;
import hust.da.hbn.MovieTheater.services.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/api")
public class CityController {
    @Autowired
    private CityService cityService;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/cities")
    public ResponseEntity<?> createCity(@RequestBody @Valid CityRequest cityRequest) {
        City city = cityService.create(cityRequest);
        return ResponseEntity.ok(city);

    }

    @GetMapping("/cities")
    public ResponseEntity<?> findByName(@RequestParam(required = false, name = "query") String name,
                                        @RequestParam(required = false, name = "pageNumber") String numberOfPage) {
        return ResponseEntity.ok(cityService.findAll(name, numberOfPage));

    }

    @GetMapping("/cities/{id}")
    public ResponseEntity<?> findById(@PathVariable String id) {
        City city = cityService.findById(id);
        return ResponseEntity.ok(city);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/cities/{id}")
    public ResponseEntity<?> updateById(@RequestBody @Valid CityRequest cityRequest,
                                        @PathVariable String id) {
        City city = cityService.update(id, cityRequest);
        return ResponseEntity.ok(city);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/cities/{id}")
    public ResponseEntity<?> deleteCity(@PathVariable("id") String id) {
        cityService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/cities")
    public ResponseEntity<?> deleteAllCities() {
        cityService.deleteAll();
        return ResponseEntity.noContent().build();
    }
}
