package hust.da.hbn.MovieTheater.controller;

import hust.da.hbn.MovieTheater.dto.request.SigninRequest;
import hust.da.hbn.MovieTheater.dto.request.SignupRequest;
import hust.da.hbn.MovieTheater.dto.response.LoginResponse;
import hust.da.hbn.MovieTheater.dto.response.ResponseMessage;
import hust.da.hbn.MovieTheater.repository.UserRepository;
import hust.da.hbn.MovieTheater.security.jwt.JwtUtils;
import hust.da.hbn.MovieTheater.services.AuthService;
import hust.da.hbn.MovieTheater.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthService authService;

    @Autowired
    private UserService userService;

    @Autowired
    JwtUtils jwtUtils;

    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody SigninRequest loginRequest) {
        LoginResponse response = authService.login(loginRequest);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        authService.signup(signUpRequest);
        return ResponseEntity.ok(new ResponseMessage("Register successfully"));
    }
}