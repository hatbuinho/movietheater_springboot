package hust.da.hbn.MovieTheater.controller;

import hust.da.hbn.MovieTheater.dto.request.SeatRequest;
import hust.da.hbn.MovieTheater.dto.request.ShowRequest;
import hust.da.hbn.MovieTheater.entities.Seat;
import hust.da.hbn.MovieTheater.entities.Show;
import hust.da.hbn.MovieTheater.services.ShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class ShowController {
    @Autowired
    private ShowService showService;

    @GetMapping("/public/movies/{movieId}/shows")
    public ResponseEntity<?> findByMovie(@PathVariable(required = false, name = "movieId") String movieId,
                                         @RequestParam(required = false, name = "pageNumber") String pageNumber) {
        List<Show> shows = showService.findAll(movieId, pageNumber);
        return ResponseEntity.ok(shows);
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @GetMapping("/shows")
    public ResponseEntity<?> findAll() {
        List<Show> shows = showService.findAll();
        return ResponseEntity.ok(shows);
    }


    @GetMapping("/public/shows/find")
    public ResponseEntity<?> findByDateAndMovie(@RequestParam(name = "movieId") String movieId,
                                                @RequestParam(name = "date") String date) {
        List<Show> shows = showService.findByDateAndMovie(date, movieId);
        return ResponseEntity.ok(shows);
    }

    @GetMapping("/public/shows/{showId}")
    public ResponseEntity<?> findById(@PathVariable String showId) {
        Show show = showService.findById(showId);
        return ResponseEntity.ok(show);
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @PostMapping("/shows")
    public ResponseEntity<?> createShow(@RequestBody @Valid ShowRequest showRequest) {
        Show show = showService.create(showRequest);
        return ResponseEntity.ok(show);
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @PutMapping("/shows/{id}")
    public ResponseEntity<?> updateById(@RequestBody @Valid ShowRequest showRequest,
                                        @PathVariable String id) {
        Show show = showService.update(id, showRequest);
        return ResponseEntity.ok(show);
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @DeleteMapping("/shows")
    public ResponseEntity<?> deleteAllShow() {
        showService.deleteAll();
        return ResponseEntity.noContent().build();
    }
}
