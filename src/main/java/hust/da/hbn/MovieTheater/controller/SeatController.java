package hust.da.hbn.MovieTheater.controller;

import hust.da.hbn.MovieTheater.dto.request.SeatRequest;
import hust.da.hbn.MovieTheater.entities.Seat;
import hust.da.hbn.MovieTheater.services.SeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class SeatController {
    @Autowired
    private SeatService seatService;

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/seats")
    public ResponseEntity<?> create(@RequestBody @Valid SeatRequest seatRequest) {
        Seat seat = seatService.create(seatRequest);
        return ResponseEntity.ok(seat);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/seats")
    public ResponseEntity<?> findAll() {
        return ResponseEntity.ok(seatService.findAll());
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/seats/{id}")
    public ResponseEntity<?> findById(@PathVariable String id) {
        Seat seat = seatService.findById(id);
        return ResponseEntity.ok(seat);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/seats/{id}")
    public ResponseEntity<?> updateById(@RequestBody @Valid SeatRequest seatRequest,
                                        @PathVariable String id) {
        Seat seat = seatService.update(id, seatRequest);
        return ResponseEntity.ok(seat);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/seats/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") String id) {
        seatService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/seats")
    public ResponseEntity<?> deleteAllCities() {
        seatService.deleteAll();
        return ResponseEntity.noContent().build();
    }
}
