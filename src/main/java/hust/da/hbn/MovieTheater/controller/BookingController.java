package hust.da.hbn.MovieTheater.controller;

import hust.da.hbn.MovieTheater.dto.request.BookingRequest;
import hust.da.hbn.MovieTheater.entities.Booking;
import hust.da.hbn.MovieTheater.enums.BookingStatus;
import hust.da.hbn.MovieTheater.services.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api")
public class BookingController {
    @Value("#{T(java.lang.Integer).parseInt(${payment.second.delay})}")
    private int paymentDelay;


    @Autowired
    private BookingService bookingService;

    @PreAuthorize("hasRole('EMPLOYEE')")
    @GetMapping("/bookings")
    public ResponseEntity<?> findAllBooking() {
        List<Booking> bookingList = bookingService.findAll();
        return ResponseEntity.ok(bookingList);
    }


    @GetMapping("/movies/{movieId}/bookings")
    public ResponseEntity<?> findAndPagingBooking(
            @PathVariable(name = "movieId") String movieId,
            @RequestParam(name = "createdDate", required = false) String createdDate,
            @RequestParam(name = "status", required = false) String status,
            @RequestParam(name = "pageNumber", required = false) String pageNumber) {
        List<Booking> bookingList = bookingService.filterAndPagingByMovieAndStatusAndCreatedDate(
                movieId,
                status,
                createdDate,
                pageNumber
        );

        return ResponseEntity.ok(bookingList);
    }

    @PreAuthorize("hasRole('MEMBER')")
    @PostMapping("/bookings")
    public ResponseEntity<?> createBooking(@RequestBody @Valid BookingRequest bookingRequest) {

        Booking booking = bookingService.create(bookingRequest);

        bookingService.cancelUnpaidBooking(booking.getBookingId().toString(), 60);


        return ResponseEntity.ok(booking);
    }

//    @PostMapping("/bookings/{bookingId}/unconfirm/cancel")
//    public ResponseEntity<?> cancelUnConfirmBooking(@PathVariable(name = "bookingId") String bookingId) {
//        bookingService.cancelUnpaidBooking();
//        return ResponseEntity.noContent().build();
//    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @PostMapping("/bookings/cancel/{bookingId}")
    public ResponseEntity<?> cancelBooking(@PathVariable String bookingId) {
        Booking booking = bookingService.cancelById(bookingId);
        return ResponseEntity.ok(booking);
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @PostMapping("/bookings/{bookingId}/check-in")
    public ResponseEntity<?> checkinBooking(@PathVariable(name = "bookingId") String bookingId) {
        Booking booking = bookingService.checkin(bookingId);
        return ResponseEntity.ok(booking);
    }

    @PreAuthorize("hasRole('MEMBER')")
    @GetMapping("/users/{userId}/bookings")
    public ResponseEntity<?> findMyBookings(@PathVariable(name = "userId") String userId) {
        List<Booking> bookingList = bookingService.findByUserAndStatusNot(userId, BookingStatus.REQUESTED);
        return ResponseEntity.ok(bookingList);
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @GetMapping("/bookings/status")
    public ResponseEntity<?> findByStatusNot(@RequestParam(name = "status") String status,
                                             @RequestParam(name = "pageNumber") String pageNumber) {
        List<Booking> bookingList = bookingService.findByStatusNot(status, pageNumber);
        return ResponseEntity.ok(bookingList);
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @GetMapping("/bookings/code")
    public ResponseEntity<?> findByCode(@RequestParam(name = "bookingCode") String bookingCode) {
        Booking booking = bookingService.findByBookingCode(bookingCode);
        return ResponseEntity.ok(booking);
    }
}
