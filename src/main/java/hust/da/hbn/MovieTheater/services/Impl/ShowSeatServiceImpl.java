package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.entities.Show;
import hust.da.hbn.MovieTheater.entities.ShowSeat;
import hust.da.hbn.MovieTheater.enums.ShowSeatStatus;
import hust.da.hbn.MovieTheater.exceptions.ShowSeatNotFoundException;
import hust.da.hbn.MovieTheater.repository.ShowSeatRepository;
import hust.da.hbn.MovieTheater.services.ShowSeatService;
import hust.da.hbn.MovieTheater.services.ShowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ShowSeatServiceImpl implements ShowSeatService {
    @Autowired
    private ShowSeatRepository showSeatRepository;

    @Autowired
    private ShowService showService;

    @Override
    public ShowSeat findById(String id) {
        long longId;
        try {
            longId = Long.parseLong(id);
        } catch (Exception e) {
            throw new ShowSeatNotFoundException();
        }
        return showSeatRepository.findById(longId).orElseThrow(ShowSeatNotFoundException::new);
    }

    @Override
    public List<ShowSeat> findByShowId(String showId) {
        Show show = showService.findById(showId);

        return showSeatRepository.findByShow(show);
    }
}
