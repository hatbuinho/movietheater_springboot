package hust.da.hbn.MovieTheater.services;

import hust.da.hbn.MovieTheater.dto.request.CinemaRequest;
import hust.da.hbn.MovieTheater.entities.later.Cinema;
import hust.da.hbn.MovieTheater.exceptions.later.CinemaExistedException;
import hust.da.hbn.MovieTheater.exceptions.later.CinemaNotFoundException;
import hust.da.hbn.MovieTheater.exceptions.later.CityNotFoundException;

import java.util.List;

public interface CinemaService {
    List<Cinema> findAll(String name, String pageNumber);

    Cinema findById(String id);

    void deleteById(String id) throws CinemaNotFoundException;

    void deleteAll();

    Cinema create(CinemaRequest c) throws CinemaExistedException, CityNotFoundException;

    Cinema update(String id, CinemaRequest c) throws CinemaNotFoundException;
}
