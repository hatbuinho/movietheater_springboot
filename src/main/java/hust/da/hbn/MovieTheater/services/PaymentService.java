package hust.da.hbn.MovieTheater.services;

import hust.da.hbn.MovieTheater.dto.request.PaymentRequest;
import hust.da.hbn.MovieTheater.entities.Payment;

import java.util.List;

public interface PaymentService {
    Payment create(PaymentRequest req);

    List<Payment> findByYearAndMonth(String year, String month);
}
