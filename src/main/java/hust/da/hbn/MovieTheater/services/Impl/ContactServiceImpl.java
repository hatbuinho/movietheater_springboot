package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.entities.Contact;
import hust.da.hbn.MovieTheater.entities.User;
import hust.da.hbn.MovieTheater.repository.ContactRepository;
import hust.da.hbn.MovieTheater.services.ContactService;
import hust.da.hbn.MovieTheater.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContactServiceImpl implements ContactService {
    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    private UserService userService;

    @Override
    public Optional<Contact> findByUserId(String userId) {
        User user = userService.findById(userId);
        return contactRepository.findByUser(user);
    }
}
