package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.dto.request.SigninRequest;
import hust.da.hbn.MovieTheater.dto.request.SignupRequest;
import hust.da.hbn.MovieTheater.dto.response.LoginResponse;
import hust.da.hbn.MovieTheater.entities.Contact;
import hust.da.hbn.MovieTheater.entities.Role;
import hust.da.hbn.MovieTheater.entities.User;
import hust.da.hbn.MovieTheater.enums.Gender;
import hust.da.hbn.MovieTheater.enums.RoleName;
import hust.da.hbn.MovieTheater.repository.UserRepository;
import hust.da.hbn.MovieTheater.security.jwt.JwtUtils;
import hust.da.hbn.MovieTheater.security.services.UserDetailsImpl;
import hust.da.hbn.MovieTheater.services.AuthService;
import hust.da.hbn.MovieTheater.services.RoleService;
import hust.da.hbn.MovieTheater.utils.Convertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyEmitter;

import javax.xml.ws.Response;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    PasswordEncoder encoder;

    @Autowired
    RoleService roleService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    AuthenticationManager authenticationManager;

    @Override
    public void signup(SignupRequest dto) {
        User user = new User(dto.getUsername(), encoder.encode(dto.getPassword()), dto.getEmail());


        LocalDate dob = Convertor.convertToLocalDate(dto.getDateOfBirth());
        Gender gender;
        try {
            gender = Gender.valueOf(dto.getGender());
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid Gender");
        }

        Contact contact = new Contact(dto.getPhone(), dto.getFullName(), gender, dob, dto.getAddress());

        Set<Role> roles = new HashSet<>();
        Role memberRole = roleService.createIfNotFound(RoleName.ROLE_MEMBER);
        roles.add(memberRole);

        user.setRoles(roles);
        user.setContact(contact);
        contact.setUser(user);
        userRepository.save(user);
    }

    @Override
    public LoginResponse login(SigninRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return new LoginResponse(jwt, userDetails.getId(), userDetails.getUsername(), userDetails.getEmail(),
                userDetails.getFullName(), roles);

    }
}
