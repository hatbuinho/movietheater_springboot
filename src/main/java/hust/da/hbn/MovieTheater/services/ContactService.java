package hust.da.hbn.MovieTheater.services;


import hust.da.hbn.MovieTheater.entities.Contact;
import hust.da.hbn.MovieTheater.entities.User;

import java.util.Optional;

public interface ContactService {
    Optional<Contact> findByUserId(String userId);
}
