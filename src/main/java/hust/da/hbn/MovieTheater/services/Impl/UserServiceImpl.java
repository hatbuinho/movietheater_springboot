package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.dto.request.SignupRequest;
import hust.da.hbn.MovieTheater.entities.Contact;
import hust.da.hbn.MovieTheater.entities.Role;
import hust.da.hbn.MovieTheater.entities.User;
import hust.da.hbn.MovieTheater.enums.Gender;
import hust.da.hbn.MovieTheater.enums.RoleName;
import hust.da.hbn.MovieTheater.exceptions.UserExistedException;
import hust.da.hbn.MovieTheater.exceptions.UserNotFoundException;
import hust.da.hbn.MovieTheater.repository.UserRepository;
import hust.da.hbn.MovieTheater.security.jwt.JwtUtils;
import hust.da.hbn.MovieTheater.services.ContactService;
import hust.da.hbn.MovieTheater.services.RoleService;
import hust.da.hbn.MovieTheater.services.UserService;
import hust.da.hbn.MovieTheater.utils.Convertor;
import hust.da.hbn.MovieTheater.utils.SortUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleService roleService;

    @Autowired
    JwtUtils jwtUtils;

    @Autowired
    private PasswordEncoder encoder;

    @Autowired
    private ContactService contactService;

    @Override
    public List<User> findAll() {
        return userRepository.findAll(Sort.by(Sort.Order.desc("createdDate"), Sort.Order.desc("createdTime")));
    }

    @Override
    public User findById(String id) {
        long longId;
        try {
            longId = Long.parseLong(id);
        } catch (NumberFormatException e) {
            throw new UserNotFoundException();
        }
        return userRepository.findById(longId).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User update(String id, SignupRequest req) {
        User old = this.findById(id);

        User updated = this.convertToEntity(req);

        Contact oldContact = contactService.findByUserId(id).get();
        Contact updatedContact = updated.getContact();
        updatedContact.setContactId(oldContact.getContactId());

        updated.setContact(updatedContact);
        updated.setUserId(old.getUserId());
        updated.setStatus(old.getStatus());

        return userRepository.save(updated);
    }

    @Override
    public User create(SignupRequest req) {
        User user = this.convertToEntity(req);

        // check exist
        if (userRepository.existsByUsername(req.getUsername()))
            throw new UserExistedException("This username already in use");
        if (userRepository.existsByEmail(req.getEmail()))
            throw new UserExistedException("This email already in use");

        return userRepository.save(user);
    }

    @Override
    public void delete(String userId) {
        User user = this.findById(userId);
        userRepository.delete(user);
    }

    private User convertToEntity(SignupRequest dto) {
        User user = new User(dto.getUsername(), encoder.encode(dto.getPassword()), dto.getEmail());

        LocalDate dob = Convertor.convertToLocalDate(dto.getDateOfBirth());
        String role = dto.getRole();

        Role adminRole;
        Role empRole;
        Role memberRole;

        Gender gender;
        try {
//            int genderIndex = Integer.parseInt(dto.getGender());
            gender = Gender.valueOf(dto.getGender());
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid gender");
        }


        Contact contact = new Contact(dto.getPhone(), dto.getFullName(), gender, dob, dto.getAddress());

        Set<Role> roles = new HashSet<>();

        if (role == null) {
            memberRole = roleService.createIfNotFound(RoleName.ROLE_MEMBER);
            roles.add(memberRole);
        } else {
            switch (role) {
                case "admin":
                    adminRole = roleService.createIfNotFound(RoleName.ROLE_ADMIN);
                    empRole = roleService.createIfNotFound(RoleName.ROLE_EMPLOYEE);
                    memberRole = roleService.createIfNotFound(RoleName.ROLE_MEMBER);
                    roles.addAll(Arrays.asList(adminRole, empRole, memberRole));

                    break;
                case "employee":
                    empRole = roleService.createIfNotFound(RoleName.ROLE_EMPLOYEE);
                    memberRole = roleService.createIfNotFound(RoleName.ROLE_MEMBER);
                    roles.addAll(Arrays.asList(empRole, memberRole));

                    break;
                default:
                    memberRole = roleService.createIfNotFound(RoleName.ROLE_MEMBER);
                    roles.add(memberRole);

            }
            ;
        }


        user.setRoles(roles);
        user.setContact(contact);
        contact.setUser(user);


        return user;


    }

}
