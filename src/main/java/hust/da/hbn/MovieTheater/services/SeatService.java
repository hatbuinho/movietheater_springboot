package hust.da.hbn.MovieTheater.services;

import hust.da.hbn.MovieTheater.dto.request.SeatRequest;
import hust.da.hbn.MovieTheater.dto.request.ShowRequest;
import hust.da.hbn.MovieTheater.entities.Seat;
import hust.da.hbn.MovieTheater.entities.Show;

import java.util.List;
import java.util.Optional;

public interface SeatService {
//    List<Seat> findAll(String roomId);

    List<Seat> findAll();

    Seat findById(String id);

    void deleteById(String id);

    void deleteAll();

    Seat create(SeatRequest req);

    Seat update(String id, SeatRequest req);
}
