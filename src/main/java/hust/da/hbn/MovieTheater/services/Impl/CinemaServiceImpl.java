package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.dto.request.CinemaRequest;
import hust.da.hbn.MovieTheater.entities.later.Cinema;
import hust.da.hbn.MovieTheater.entities.later.City;
import hust.da.hbn.MovieTheater.exceptions.later.CinemaExistedException;
import hust.da.hbn.MovieTheater.exceptions.later.CinemaNotFoundException;
import hust.da.hbn.MovieTheater.exceptions.later.CityNotFoundException;
import hust.da.hbn.MovieTheater.repository.later.CinemaRepository;
import hust.da.hbn.MovieTheater.services.CinemaService;
import hust.da.hbn.MovieTheater.services.CityService;
import hust.da.hbn.MovieTheater.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CinemaServiceImpl implements CinemaService {
    @Autowired
    private CinemaRepository cinemaRepository;

    @Autowired
    private CityService cityService;

    @Override
    public List<Cinema> findAll(String name, String numberOfPage) {
        List<Cinema> cinemas;

        // handle pageNumber
        int intNumberOfPage = Constants.ONE_PAGE;
        if (numberOfPage != null) {
            intNumberOfPage = Integer.parseInt(numberOfPage);
        }

        Pageable paging = PageRequest.of(
                Constants.FIRST_PAGE,
                intNumberOfPage * Constants.PAGE_SIZE);

        // handle name
        if (name == null) {
            cinemas = cinemaRepository.findByNameEnhance("", paging);
        } else {
            cinemas = cinemaRepository.findByNameEnhance(name, paging);
        }

        return cinemas;
    }

    @Override
    public Cinema findById(String id) {
        long longId;
        try {
            longId = Long.parseLong(id);
        } catch (Exception e) {
            throw new CinemaNotFoundException();
        }

        return cinemaRepository.findById(longId).orElseThrow(CinemaNotFoundException::new);
    }

    @Override
    public void deleteById(String id) throws CinemaNotFoundException {
        Cinema cinema = this.findById(id);
        cinemaRepository.deleteById(cinema.getCinemaId());
    }

    @Override
    public void deleteAll() {
        cinemaRepository.deleteAll();
    }

    @Override
    public Cinema create(CinemaRequest c) throws CinemaExistedException, CityNotFoundException {
        // check city existence
        Cinema cinema = this.convertToEntity(c);

        return cinemaRepository.save(cinema);
    }

    @Override
    public Cinema update(String id, CinemaRequest c) throws CinemaNotFoundException {
        Cinema old = this.findById(id);
        Cinema updated = this.convertToEntity(c);
        return cinemaRepository.save(
                new Cinema(
                        old.getCinemaId(),
                        updated.getName(),
                        updated.getCity()
                )
        );
    }

    private Cinema convertToEntity(CinemaRequest req) {
        // check city existence
        City city = cityService.findById(req.getCityId());

        // check existed cinema
        Optional<Cinema> cinema = cinemaRepository.findByNameAndCity(req.getName(), city);
        if (cinema.isPresent()) throw new CinemaExistedException();

        Cinema _cinema = new Cinema();
        _cinema.setCity(city);
        _cinema.setName(req.getName());

        return _cinema;
    }
}
