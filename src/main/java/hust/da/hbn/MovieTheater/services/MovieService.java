package hust.da.hbn.MovieTheater.services;

import hust.da.hbn.MovieTheater.dto.request.MovieRequest;
import hust.da.hbn.MovieTheater.entities.Movie;
import hust.da.hbn.MovieTheater.exceptions.MovieNotFoundException;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface MovieService {
    List<Movie> getAll(String title, String pageNumber);

    Movie findById(String id);

    void deleteById(String id);

    void deleteAll();

    Movie create(MovieRequest movie) throws IOException;

    Movie update(String id, MovieRequest m) throws IOException;
}
