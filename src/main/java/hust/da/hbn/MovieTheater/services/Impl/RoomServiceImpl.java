package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.dto.request.RoomRequest;
import hust.da.hbn.MovieTheater.entities.Room;
import hust.da.hbn.MovieTheater.exceptions.later.CinemaNotFoundException;
import hust.da.hbn.MovieTheater.exceptions.later.RoomExistedException;
import hust.da.hbn.MovieTheater.exceptions.later.RoomNotFoundException;
import hust.da.hbn.MovieTheater.repository.RoomRepository;
import hust.da.hbn.MovieTheater.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomServiceImpl implements RoomService {
    @Autowired
    private RoomRepository roomRepository;

    @Override
    public List<Room> findAll(String name, String pageNumber) {
        return roomRepository.findAll();
    }

    @Override
    public Room findById(String id) {
        long longId;
        try {
            longId = Long.parseLong(id);
        } catch (Exception e) {
            throw new RoomNotFoundException();
        }

        return roomRepository.findById(longId).orElseThrow(RoomNotFoundException::new);
    }

    @Override
    public void deleteById(String id) throws RoomNotFoundException {
        Room room = this.findById(id);
        roomRepository.delete(room);
    }

    @Override
    public void deleteAll() {
        roomRepository.deleteAll();
    }

    @Override
    public Room create(RoomRequest req) throws CinemaNotFoundException, RoomExistedException {
        Room room = convertToEntity(req);
        return roomRepository.save(room);
    }

    @Override
    public Room update(String id, RoomRequest r) throws CinemaNotFoundException, RoomNotFoundException {
        Room updatedRoom = convertToEntity(r);
        // check room existence
        Room oldRoom = this.findById(id);

        return roomRepository.save(
                new Room(oldRoom.getRoomId(),
                        updatedRoom.getName(),
                        updatedRoom.getRowSize(),
                        updatedRoom.getColSize(),
                        oldRoom.getSeats())
        );
    }


    private Room convertToEntity(RoomRequest req) throws CinemaNotFoundException {
        int rowSize = Integer.parseInt(req.getRowSize());
        int colSize = Integer.parseInt(req.getColSize());

        return new Room(null, req.getName(), rowSize, colSize, null);
    }
}
