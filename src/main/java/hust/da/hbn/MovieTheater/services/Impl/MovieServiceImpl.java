package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.controller.FilesController;
import hust.da.hbn.MovieTheater.dto.request.MovieRequest;
import hust.da.hbn.MovieTheater.entities.Movie;
import hust.da.hbn.MovieTheater.exceptions.MovieNotFoundException;
import hust.da.hbn.MovieTheater.repository.MovieRepository;
import hust.da.hbn.MovieTheater.services.FileStorageService;
import hust.da.hbn.MovieTheater.services.MovieService;
import hust.da.hbn.MovieTheater.utils.Constants;
import hust.da.hbn.MovieTheater.utils.Convertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class MovieServiceImpl implements MovieService {
    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Override
    public List<Movie> getAll(String title, String numberOfPage) {
        int intNumberOfPage = Constants.ONE_PAGE;
        if (numberOfPage != null) {
            intNumberOfPage = Integer.parseInt(numberOfPage);
        }

        Pageable sortedByReleaseDateDesc = PageRequest.of(Constants.FIRST_PAGE, Constants.PAGE_SIZE * intNumberOfPage,
                Sort.by(Sort.Order.desc("releaseDate"), Sort.Order.asc("title")));

        Page<Movie> movies;

        if (title == null) {
            movies = movieRepository.findByTitleEnhance("", sortedByReleaseDateDesc);
        } else {
            movies = movieRepository.findByTitleEnhance(title, sortedByReleaseDateDesc);

        }
        return movies.getContent();
    }

    @Override
    public Movie findById(String id) {
        long longId;
        try {
            longId = Long.parseLong(id);
        } catch (Exception e) {
            throw new MovieNotFoundException();
        }

        return movieRepository.findById(longId).orElseThrow(MovieNotFoundException::new);
    }

    @Override
    public void deleteById(String id) throws MovieNotFoundException {
        Movie movie = this.findById(id);
        movieRepository.deleteById(movie.getMovieId());
    }

    @Override
    public void deleteAll() {
        movieRepository.deleteAll();
    }

    @Override
    public Movie update(String id, MovieRequest m) throws MovieNotFoundException, IOException {
        Movie updated = this.dtoToEntity(m);
        Movie old = this.findById(id);

        return movieRepository.save(
                new Movie(
                        old.getMovieId(),
                        updated.getTitle(),
                        updated.getDescription(),
                        updated.getDuration(),
                        updated.getReleaseDate(),
                        updated.getGenre(),
                        updated.getSmallImage(),
                        updated.getLargeImage(),
                        updated.getLanguage(),
                        updated.getCountry(),
                        null // no need
                )
        );
    }


    @Override
    public Movie create(MovieRequest m) throws IOException {
        Movie movie = dtoToEntity(m);
        return movieRepository.save(movie);
    }

    private Movie dtoToEntity(MovieRequest m) throws IOException {
        // handle image
        String smallImageFileName = fileStorageService.save(m.getSmallImage());
        String largeImageFileName = fileStorageService.save(m.getLargeImage());

        String smallImageUrl = MvcUriComponentsBuilder
                .fromMethodName(FilesController.class, "getFile", smallImageFileName).build().toString();
        String largeImageUrl = MvcUriComponentsBuilder
                .fromMethodName(FilesController.class, "getFile", largeImageFileName).build().toString();

        // convert type
        LocalDate releaseDate = Convertor.convertToLocalDate(m.getReleaseDate());

        String stringId = m.getMovieId();
        Long longId = null;
        if (stringId != null) {
            longId = Long.parseLong(stringId);
        }
        Integer duration = null;
        try {
            duration = Integer.valueOf(m.getDuration());
        } catch (Exception e) {
            throw new IllegalArgumentException("Duration must be a positive number");
        }

        return new Movie(longId,
                m.getTitle(),
                m.getDescription(),
                duration,
                releaseDate,
                m.getGenre(),
                smallImageUrl,
                largeImageUrl,
                m.getLanguage(),
                m.getCountry(),
                null);
    }
}
