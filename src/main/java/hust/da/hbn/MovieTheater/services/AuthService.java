package hust.da.hbn.MovieTheater.services;

import hust.da.hbn.MovieTheater.dto.request.SigninRequest;
import hust.da.hbn.MovieTheater.dto.request.SignupRequest;
import hust.da.hbn.MovieTheater.dto.response.LoginResponse;

public interface AuthService {
    void signup(SignupRequest registerRequest);
    LoginResponse login(SigninRequest loginRequest);
}
