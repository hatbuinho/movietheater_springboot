package hust.da.hbn.MovieTheater.services;

import hust.da.hbn.MovieTheater.entities.ShowSeat;

import java.util.List;
import java.util.Optional;
import java.util.Set;


public interface ShowSeatService {
    ShowSeat findById(String id);

    List<ShowSeat> findByShowId(String showId);
}
