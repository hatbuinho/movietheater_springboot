package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.dto.request.SeatRequest;
import hust.da.hbn.MovieTheater.entities.Room;
import hust.da.hbn.MovieTheater.entities.Seat;
import hust.da.hbn.MovieTheater.enums.SeatType;
import hust.da.hbn.MovieTheater.exceptions.InvalidFormatException;
import hust.da.hbn.MovieTheater.exceptions.SeatExistedException;
import hust.da.hbn.MovieTheater.exceptions.SeatNotFoundException;
import hust.da.hbn.MovieTheater.repository.SeatRepository;
import hust.da.hbn.MovieTheater.services.RoomService;
import hust.da.hbn.MovieTheater.services.SeatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SeatServiceImpl implements SeatService {
    @Autowired
    private SeatRepository seatRepository;

    @Autowired
    private RoomService roomService;

//    @Override
//    public List<Seat> findAll(String roomId) {
//        Room room = roomService.findById(roomId);
//
//        return seatRepository.findByRoom(room);
//    }

    @Override
    public List<Seat> findAll() {
        return seatRepository.findAll();
    }

    @Override
    public Seat findById(String id) {
        long longId;
        try {
            longId = Long.parseLong(id);
        } catch (Exception e) {
            throw new SeatNotFoundException();
        }

        return seatRepository.findById(longId).orElseThrow(SeatNotFoundException::new);
    }

    @Override
    public void deleteById(String id) {
        Seat seat = this.findById(id);
        seatRepository.delete(seat);
    }

    @Override
    public void deleteAll() {
        seatRepository.deleteAll();
    }

    @Override
    public Seat create(SeatRequest req) {
        Seat seat = this.convertToEntity(req);
        if (seatRepository.findByRowAndColAndRoom(seat.getRow(), seat.getCol(), seat.getRoom()).isPresent()) {
            throw new SeatExistedException();
        }

        return seatRepository.save(seat);
    }

    @Override
    public Seat update(String id, SeatRequest req) {
        Seat old = this.findById(id);
        Room room = roomService.findById(req.getRoomId());
        Seat updated = this.convertToEntity(req);

        return seatRepository.save(new Seat(old.getSeatId(),
                updated.getRow(),
                updated.getCol(),
                updated.getType(),
                room, old.getShowSeats()));
    }

    private Seat convertToEntity(SeatRequest req) {

        Room room = roomService.findById(req.getRoomId());

        int row, col;
        try {
            row = Integer.parseInt(req.getRow());
            col = Integer.parseInt(req.getCol());
        } catch (Exception e) {
            throw new InvalidFormatException("Invalid seat position");
        }
        SeatType type;
        try {
            type = SeatType.valueOf(req.getType());
        } catch (Exception e) {
            throw new InvalidFormatException("Invalid seat type");
        }

        Seat seat = new Seat();
        seat.setCol(col);
        seat.setRow(row);
        seat.setType(type);
        seat.setRoom(room);

        return seat;
    }
}
