package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.dto.request.PaymentRequest;
import hust.da.hbn.MovieTheater.entities.Booking;
import hust.da.hbn.MovieTheater.entities.Payment;
import hust.da.hbn.MovieTheater.enums.BookingStatus;
import hust.da.hbn.MovieTheater.enums.PaymentMethod;
import hust.da.hbn.MovieTheater.repository.PaymentRepository;
import hust.da.hbn.MovieTheater.services.BookingService;
import hust.da.hbn.MovieTheater.services.PaymentService;
import hust.da.hbn.MovieTheater.utils.Convertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private BookingService bookingService;

    @Override
    public Payment create(PaymentRequest req) {
        Payment payment = new Payment();
        BigDecimal amount = Convertor.convertToBigDecimal(req.getAmount());
        Booking booking = bookingService.findById(req.getBookingId());
        booking.setStatus(BookingStatus.CONFIRMED);

        PaymentMethod paymentMethod;
        try {
            paymentMethod = PaymentMethod.valueOf(req.getPaymentMethod());
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid payment method");
        }
        payment.setAmount(amount);
        payment.setPaymentMethod(paymentMethod);
        payment.setBooking(booking);

        return paymentRepository.save(payment);
    }

    @Override
    public List<Payment> findByYearAndMonth(String year, String month) {
        Integer intYear, intMonth;
        try {
            intYear = Integer.valueOf(year);

        } catch (Exception e) {
            intYear = null;
        }
        try {
            intMonth = Integer.valueOf(month);

        } catch (Exception e) {
            intMonth = null;
        }

        return paymentRepository.findByYearAndMonth(intYear, intMonth);
    }
}
