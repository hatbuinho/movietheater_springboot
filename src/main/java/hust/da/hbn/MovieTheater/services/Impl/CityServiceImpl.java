package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.dto.request.CityRequest;
import hust.da.hbn.MovieTheater.entities.later.City;
import hust.da.hbn.MovieTheater.exceptions.later.CityExistedException;
import hust.da.hbn.MovieTheater.exceptions.later.CityNotFoundException;
import hust.da.hbn.MovieTheater.repository.later.CityRepository;
import hust.da.hbn.MovieTheater.services.CityService;
import hust.da.hbn.MovieTheater.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


import java.util.Optional;
import java.util.List;

@Service
public class CityServiceImpl implements CityService {

    @Autowired
    private CityRepository cityRepository;

    @Override
    public List<City> findAll(String name, String numberOfPage) {
        List<City> cities;

        // handle numberOfPage
        int intNumberOfPage = Constants.ONE_PAGE;
        if (numberOfPage != null) {
            intNumberOfPage = Integer.parseInt(numberOfPage);
        }

        Pageable paging = PageRequest.of(
                Constants.FIRST_PAGE,
                intNumberOfPage * Constants.PAGE_SIZE);

        // handle name
        if (name == null) {
            cities = cityRepository.findByNameEnhance("", paging);
        } else {
            cities = cityRepository.findByNameEnhance(name, paging);
        }

        return cities;
    }

    @Override
    public City findById(String id) {
        long longId;
        try {
            longId = Long.parseLong(id);
        } catch (Exception e) {
            throw new CityNotFoundException();
        }

        return cityRepository.findById(longId).orElseThrow(CityNotFoundException::new);
    }

    @Override
    public void deleteById(String id) {
        City city = this.findById(id);
        cityRepository.deleteById(city.getCityId());
    }

    @Override
    public void deleteAll() {
        cityRepository.deleteAll();
    }

    @Override
    public City create(CityRequest c) throws CityExistedException {
        Optional<City> city = cityRepository.findByName(c.getName());

        if (city.isPresent()) {
            throw new CityExistedException();
        }

        City _city = new City();
        _city.setName(c.getName());
        _city.setZipCode(c.getZipCode());

        return cityRepository.save(_city);
    }

    @Override
    public City update(String id, CityRequest c) {
        City old = this.findById(id);
        return cityRepository.save(
                new City(
                        old.getCityId(),
                        c.getName(),
                        c.getZipCode(),
                        null
                )
        );
    }
}
