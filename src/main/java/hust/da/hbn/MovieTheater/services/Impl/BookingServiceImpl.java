package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.dto.request.BookingRequest;
import hust.da.hbn.MovieTheater.entities.*;
import hust.da.hbn.MovieTheater.enums.BookingStatus;
import hust.da.hbn.MovieTheater.enums.ShowSeatStatus;
import hust.da.hbn.MovieTheater.exceptions.BookingNotFoundException;
import hust.da.hbn.MovieTheater.exceptions.ShowSeatNotAvailableException;
import hust.da.hbn.MovieTheater.repository.BookingRepository;
import hust.da.hbn.MovieTheater.repository.MovieRepository;
import hust.da.hbn.MovieTheater.repository.ShowSeatRepository;
import hust.da.hbn.MovieTheater.services.*;
import hust.da.hbn.MovieTheater.utils.Constants;
import hust.da.hbn.MovieTheater.utils.Convertor;
import hust.da.hbn.MovieTheater.utils.SortUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class BookingServiceImpl implements BookingService {
    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private ShowSeatRepository showSeatRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ShowSeatService showSeatService;

    @Autowired
    private ShowService showService;

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private ThreadPoolTaskScheduler taskScheduler;


    @Override
    public List<Booking> findAll() {
        return bookingRepository.findAll(Sort.by("createdDate").descending());
    }

    @Override
    public List<Booking> filterAndPagingByMovieAndStatusAndCreatedDate(
            String movieIdString,
            String statusString,
            String createdDateString,
            String pageNumber) {

        long longMovieId;
        Movie movie;
        BookingStatus status;
        LocalDate createdDate;
        // movie
        try {
            longMovieId = Long.parseLong(movieIdString);
            movie = movieRepository.findById(longMovieId).orElse(null);
        } catch (Exception e) {
            movie = null;
        }
        // status
        try {
            status = BookingStatus.valueOf(statusString);
        } catch (Exception e) {
            status = null;
        }
        // createdDate
        try {
            createdDate = LocalDate.parse(createdDateString);
        } catch (Exception e) {
            createdDate = null;
        }

        Pageable pageable = Constants.WHOLE_PAGE.equals(pageNumber) ?
                Pageable.unpaged() :
                SortUtils.sortedAndPagingByCreatedDateAndTimeDescending(pageNumber);

        return bookingRepository.filterAndPagingByMovieAndStatusAndCreatedDate(
                movie,
                status,
                createdDate,
                pageable
        );
    }

    @Override
    public Booking findByBookingCode(String bookingCode) {
        if (bookingCode == null) return null;
        return bookingRepository.findByBookingCode(bookingCode).orElse(null);
    }

    @Override
    public List<Booking> findByUserAndStatusNot(String userId, BookingStatus status) {
        User user = userService.findById(userId);

        return bookingRepository.findByUserAndStatusNot(user,
                status,
                Sort.by(Sort.Order.desc("createdDate"), Sort.Order.desc("createdTime")));
    }

    @Override
    public Booking findById(String id) {
        long longId;
        try {
            longId = Long.parseLong(id);
        } catch (Exception e) {
            throw new BookingNotFoundException();
        }
        return bookingRepository.findById(longId).orElseThrow(BookingNotFoundException::new);
    }

    @Override
    public void deleteById(String id) {
        Booking booking = this.findById(id);
        Set<ShowSeat> showSeats = booking
                .getShowSeats()
                .stream()
                .peek(seat -> {
                    seat.setStatus(ShowSeatStatus.AVAILABLE);
                    seat.setBooking(null);
                })
                .collect(Collectors.toSet());
        showSeatRepository.saveAll(showSeats);
        bookingRepository.delete(booking);
    }

    @Override
    public void deleteAll() {
        Set<ShowSeat> showSeats = showSeatRepository
                .findAll()
                .stream()
                .peek(showSeat -> {
                    showSeat.setStatus(ShowSeatStatus.AVAILABLE);
                    showSeat.setBooking(null);
                })
                .collect(Collectors.toSet());

        bookingRepository.deleteAll();
    }

    @Override
    public Booking create(BookingRequest req) {
        Booking booking = this.convertToEntity(req);
        booking.setStatus(BookingStatus.REQUESTED);
        System.out.println("start atttttttttttttttttttt " + LocalTime.now());
        return bookingRepository.save(booking);
    }

    @Override
    public Booking update(String id, String statusString) {
        Booking booking = this.findById(id);
        BookingStatus status = Convertor.convertToBookingStatus(statusString);

        booking.setStatus(status);
        return bookingRepository.save(booking);
    }


    @Override
    public Booking cancel(Booking booking) {
        booking.setStatus(BookingStatus.CANCELLED);
        this.releaseShowSeats(booking);

        return bookingRepository.save(booking);
    }

    @Override
    public Booking cancelById(String bookingId) {
        Booking booking = this.findById(bookingId);
        return this.cancel(booking);
    }

    @Override
    public void cancelUnpaidBooking(String bookingId, int secondDelays) {

        taskScheduler.scheduleWithFixedDelay(() -> {
            Booking booking = this.findById(bookingId);
            if (BookingStatus.REQUESTED.equals(booking.getStatus())) {
                this.releaseShowSeats(booking);
                bookingRepository.delete(booking);
            }
        }, new Date(System.currentTimeMillis() + (secondDelays * 1000)), Long.MAX_VALUE);
    }

    @Override
    public Booking checkin(String bookingId) {
        Booking booking = this.findById(bookingId);
        booking.setStatus(BookingStatus.CHECKED_IN);
        return bookingRepository.save(booking);
    }

    @Override
    public List<Booking> findByStatusNot(String bookingStatus, String pageNumber) {
        BookingStatus status = Convertor.convertToBookingStatus(bookingStatus);

        Pageable sortedAndPagingByCreatedDateAndTime =
                SortUtils.sortedAndPagingByCreatedDateAndTimeDescending(pageNumber);

        return bookingRepository.findByStatusNot(status,
                sortedAndPagingByCreatedDateAndTime
        );
    }


    private Booking convertToEntity(BookingRequest req) {
        Booking booking = new Booking();
        User user = userService.findById(req.getUserId());
        Show show = showService.findById(req.getShowId());
        // find and update status of show seat
        Set<ShowSeat> showSeats = req
                .getShowSeatIds()
                .stream()
                .map(seatId -> {
                    ShowSeat showSeat = showSeatService.findById(seatId);

                    // get seat name
                    Seat seat = showSeat.getSeat();

                    ShowSeatStatus status = showSeat.getStatus();
                    if (ShowSeatStatus.RESERVED.equals(status)) {
                        throw new ShowSeatNotAvailableException("Sorry! Some seat is no longer available");
                    }

                    showSeat.setStatus(ShowSeatStatus.RESERVED);
                    showSeat.setBooking(booking);
                    return showSeat;
                })
                .collect(Collectors.toSet());

        int numberOfSeat = Integer.parseInt(req.getNumberOfSeat());

        numberOfSeat = numberOfSeat == showSeats.size() ? numberOfSeat : showSeats.size();


        booking.setUser(user);
        booking.setShow(show);
        booking.setShowSeats(showSeats);
        booking.setNumberOfSeat(numberOfSeat);

        return booking;
    }

    private void releaseShowSeats(Booking booking) {
        Set<ShowSeat> showSeats = booking
                .getShowSeats()
                .stream()
                .peek(showSeat -> {
                    showSeat.setBooking(null);
                    showSeat.setStatus(ShowSeatStatus.AVAILABLE);
                })
                .collect(Collectors.toSet());
        showSeatRepository.saveAll(showSeats);
    }
}
