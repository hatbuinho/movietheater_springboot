package hust.da.hbn.MovieTheater.services.Impl;

import hust.da.hbn.MovieTheater.dto.request.ShowRequest;
import hust.da.hbn.MovieTheater.entities.*;
import hust.da.hbn.MovieTheater.enums.SeatType;
import hust.da.hbn.MovieTheater.exceptions.ShowNotFoundException;
import hust.da.hbn.MovieTheater.repository.ShowRepository;
import hust.da.hbn.MovieTheater.services.MovieService;
import hust.da.hbn.MovieTheater.services.SeatService;
import hust.da.hbn.MovieTheater.services.ShowSeatService;
import hust.da.hbn.MovieTheater.services.ShowService;
import hust.da.hbn.MovieTheater.utils.Convertor;

import hust.da.hbn.MovieTheater.utils.SortUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ShowServiceImpl implements ShowService {
    @Autowired
    private ShowRepository showRepository;

    @Autowired
    private MovieService movieService;

    @Autowired
    private SeatService seatService;

    @Autowired
    private ShowSeatService showSeatService;

    @Override
    public List<Show> findAll() {
        return showRepository.findAll(Sort.by("date").descending());
    }

    @Override
    public List<Show> findAll(String movieId, String pageNumber) {
        Movie movie = movieService.findById(movieId);

        int intPageNumber = Convertor.convertPageNumber(pageNumber);

        Pageable sortedByDate = SortUtils.sortedByDateDescending(pageNumber);

        return showRepository.findByMovie(movie, sortedByDate);
    }

    @Override
    public Show findById(String id) {

        long longId;
        try {
            longId = Long.parseLong(id);
        } catch (Exception e) {
            throw new ShowNotFoundException();
        }

        return showRepository.findById(longId).orElseThrow(ShowNotFoundException::new);
    }

    @Override
    public void deleteById(String id) {
        Show show = this.findById(id);
        showRepository.deleteById(show.getShowId());
    }

    @Override
    public void deleteAll() {
        showRepository.deleteAll();
    }

    @Override
    public Show create(ShowRequest req) {
        Show show = this.convertToEntity(req);

        Set<ShowSeat> showSeats = seatService.findAll().stream().map(seat -> {
            ShowSeat showSeat = new ShowSeat();
            showSeat.setShow(show); // show id
            showSeat.setSeat(seat); // seat id
            showSeat.setListPrice(show.getListPrice().multiply(BigDecimal.valueOf(seat.getType().getRatio())));
            return showSeat;
        }).collect(Collectors.toSet());

        show.setShowSeats(showSeats);

        return showRepository.save(show);
    }

    @Override
    public Show update(String id, ShowRequest req) {
        Show old = this.findById(id);
        Show updated = this.convertToEntity(req);
        BigDecimal newPrice = updated.getListPrice();
        Set<ShowSeat> showSeats = old.getShowSeats().stream().peek(showSeat -> {
            SeatType seatType = showSeat.getSeat().getType();
            showSeat.setListPrice(newPrice.multiply(BigDecimal.valueOf(seatType.getRatio())));
        }).collect(Collectors.toSet());

        return showRepository.save(
                new Show(
                        old.getShowId(),
                        updated.getDate(),
                        updated.getStartTime(),
                        updated.getEndTime(),
                        updated.getMovie(),
                        updated.getListPrice(),
                        null, // no need update booking
                        showSeats //
                )
        );
    }

    @Override
    public List<Show> findByDateAndMovie(String date, String movieId) {
        Movie movie = movieService.findById(movieId);
        LocalDate iDate = Convertor.convertToLocalDate(date);
        return showRepository.findByDateAndMovie(iDate, movie);
    }


    private Show convertToEntity(ShowRequest req) {
        Show show = new Show();

        // check existence movie
        Movie movie = movieService.findById(req.getMovieId());


        // convert list price
        BigDecimal listPrice = Convertor.convertToBigDecimal(req.getListPrice());

        // convert date
        LocalDate date;
        try {
            date = Convertor.convertToLocalDate(req.getDate());
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid date format");
        }

        // convert time
        LocalTime startTime;
        LocalTime endTime;

        startTime = Convertor.convertToLocalTime(req.getStartTime());
        endTime = startTime.plusMinutes(movie.getDuration());


        // assign all properties
        show.setMovie(movie);
        show.setDate(date);
        show.setStartTime(startTime);
        show.setEndTime(endTime);
        show.setListPrice(listPrice);

        return show;
    }
}
