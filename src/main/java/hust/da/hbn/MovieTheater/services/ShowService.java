package hust.da.hbn.MovieTheater.services;

import hust.da.hbn.MovieTheater.dto.request.ShowRequest;
import hust.da.hbn.MovieTheater.entities.Show;

import java.util.List;

public interface ShowService {
    //    List<Show> findAll(String movieId, String roomId, String pageNumber);
    List<Show> findAll();

    List<Show> findAll(String movieId, String pageNumber);

    Show findById(String id);

    void deleteById(String id);

    void deleteAll();

    Show create(ShowRequest req);

    Show update(String id, ShowRequest req);

    List<Show> findByDateAndMovie(String date, String movieId);
}
