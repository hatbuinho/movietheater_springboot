package hust.da.hbn.MovieTheater.services;


import hust.da.hbn.MovieTheater.entities.Role;
import hust.da.hbn.MovieTheater.enums.RoleName;


public interface RoleService {


    Role createIfNotFound(final RoleName role);
}