package hust.da.hbn.MovieTheater.services;

import hust.da.hbn.MovieTheater.dto.request.BookingRequest;
import hust.da.hbn.MovieTheater.entities.Booking;
import hust.da.hbn.MovieTheater.enums.BookingStatus;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface BookingService {
    List<Booking> findAll();

    List<Booking> filterAndPagingByMovieAndStatusAndCreatedDate(String movieId,
                                                                String status,
                                                                String createdDate,
                                                                String pageNumber);

    Booking findByBookingCode(String bookingCode);

    List<Booking> findByUserAndStatusNot(String userId, BookingStatus status);

    Booking findById(String id);

    void deleteById(String id);

    void deleteAll();

    Booking create(BookingRequest req);

    Booking update(String id, String status);

    Booking cancel(Booking booking);

    Booking cancelById(String bookingId);

    void cancelUnpaidBooking(String bookingId, int secondDelays);

    Booking checkin(String bookingId);

    List<Booking> findByStatusNot(String bookingStatus, String pageNumber);
}
