package hust.da.hbn.MovieTheater.services;

import hust.da.hbn.MovieTheater.dto.request.CityRequest;
import hust.da.hbn.MovieTheater.entities.later.City;
import hust.da.hbn.MovieTheater.exceptions.later.CityExistedException;
import hust.da.hbn.MovieTheater.exceptions.later.CityNotFoundException;

import java.util.List;

public interface CityService {
    List<City> findAll(String name, String numberOfPage);

    City findById(String id);

    void deleteById(String id) throws CityNotFoundException;

    void deleteAll();

    City create(CityRequest c) throws CityExistedException;

    City update(String id, CityRequest c) throws CityNotFoundException;
}
