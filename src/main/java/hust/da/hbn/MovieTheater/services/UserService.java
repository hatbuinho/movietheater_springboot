package hust.da.hbn.MovieTheater.services;

import hust.da.hbn.MovieTheater.dto.request.SignupRequest;
import hust.da.hbn.MovieTheater.entities.User;

import java.util.List;
import java.util.Optional;


public interface UserService {
    List<User> findAll();

    User findById(String id);

    User update(String id, SignupRequest req);

    User create(SignupRequest req);

    void delete(String userId);
}
