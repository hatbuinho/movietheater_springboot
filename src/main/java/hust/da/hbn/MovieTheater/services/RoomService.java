package hust.da.hbn.MovieTheater.services;

import hust.da.hbn.MovieTheater.dto.request.RoomRequest;
import hust.da.hbn.MovieTheater.entities.Room;
import hust.da.hbn.MovieTheater.exceptions.later.CinemaNotFoundException;
import hust.da.hbn.MovieTheater.exceptions.later.RoomExistedException;
import hust.da.hbn.MovieTheater.exceptions.later.RoomNotFoundException;

import java.util.List;

public interface RoomService {
    List<Room> findAll(String name, String pageNumber);

    Room findById(String id);

    void deleteById(String id) throws RoomNotFoundException;

    void deleteAll();

    Room create(RoomRequest r) throws CinemaNotFoundException, RoomExistedException;

    Room update(String id, RoomRequest r) throws RoomNotFoundException, CinemaNotFoundException;
}
