package hust.da.hbn.MovieTheater.services.Impl;


import hust.da.hbn.MovieTheater.entities.Role;
import hust.da.hbn.MovieTheater.enums.RoleName;
import hust.da.hbn.MovieTheater.repository.RoleRepository;
import hust.da.hbn.MovieTheater.services.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Role createIfNotFound(RoleName name) {
        Optional<Role> role = roleRepository.findByName(name);
        return role.orElseGet(() -> roleRepository.save(new Role(name)));
    }
}
