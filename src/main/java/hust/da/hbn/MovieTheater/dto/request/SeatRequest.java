package hust.da.hbn.MovieTheater.dto.request;

import javax.validation.constraints.NotBlank;

import hust.da.hbn.MovieTheater.validation.annotation.IsLong;
import hust.da.hbn.MovieTheater.validation.annotation.IsPositive;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class SeatRequest {
    @IsLong(message = "{room.id.invalid}")
    private String roomId;

    @IsPositive(message = "{seat.row.invalid}")
    private String row;

    @IsPositive(message = "{seat.col.invalid}")
    private String col;

    @NotBlank(message = "{seat.type.invalid}")
    private String type;
}
