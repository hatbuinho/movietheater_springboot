package hust.da.hbn.MovieTheater.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CinemaRequest {
    @NotNull
    private String cityId;
    @NotBlank
    private String name;
}
