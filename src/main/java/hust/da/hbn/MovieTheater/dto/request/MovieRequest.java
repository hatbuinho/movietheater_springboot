package hust.da.hbn.MovieTheater.dto.request;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import hust.da.hbn.MovieTheater.validation.annotation.IsPositive;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MovieRequest {

    private String movieId;

    @NotBlank
    private String title;

    private String description;

    @IsPositive(message = "{movie.duration.invalid}")
    private String duration;

    @NotNull
    private String releaseDate;

    @NotNull
    private String genre;

    @NotBlank
    private String language;

    @NotBlank
    private String country;

    @NotNull
    private MultipartFile smallImage;

    @NotNull
    private MultipartFile largeImage;

}
