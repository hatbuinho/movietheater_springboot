package hust.da.hbn.MovieTheater.dto.response;

import hust.da.hbn.MovieTheater.entities.Movie;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@Getter
@Setter
public class MoviesSearchResponse {
    private List<Movie> movies;
}
