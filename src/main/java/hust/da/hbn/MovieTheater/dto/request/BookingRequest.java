package hust.da.hbn.MovieTheater.dto.request;

import hust.da.hbn.MovieTheater.validation.annotation.IsLong;
import hust.da.hbn.MovieTheater.validation.annotation.IsPositive;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

//@AllArgsConstructor
//@NoArgsConstructor
@Getter
@Setter
public class BookingRequest {
    @IsPositive(message = "{booking.numberOfSeat.invalid}")
    private String numberOfSeat;

    private Set<String> showSeatIds;

    @IsLong(message = "{user.id.invalid}")
    private String userId;
    
    private String showId;
}
