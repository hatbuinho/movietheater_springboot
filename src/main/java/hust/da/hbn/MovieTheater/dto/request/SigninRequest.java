package hust.da.hbn.MovieTheater.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;


@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class SigninRequest {
    @Size(min = 3, max = 20, message = "{user.username.invalid}")
    private String username;
    @Size(min = 6, max = 40, message = "{user.password.invalid}")
    private String password;

    private String message;

    public SigninRequest(String message) {
        this.message = message;
    }
}
