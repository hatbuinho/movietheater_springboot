package hust.da.hbn.MovieTheater.dto.response;


import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
public class LoginResponse {
    @NonNull
    private String token;
    @NonNull
    private Long id;
    @NonNull
    private String username;
    @NonNull
    private String email;

    @NonNull
    private String fullName;

    @NonNull
    private List<String> roles;
}
