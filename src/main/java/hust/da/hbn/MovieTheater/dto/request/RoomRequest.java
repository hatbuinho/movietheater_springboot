package hust.da.hbn.MovieTheater.dto.request;


import javax.validation.constraints.NotBlank;

import hust.da.hbn.MovieTheater.validation.annotation.IsLong;
import hust.da.hbn.MovieTheater.validation.annotation.IsPositive;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RoomRequest {
//    @IsLong(message = "{cinema.id.invalid}")
//    private String cinemaId;

    private String name;

    @IsPositive(message = "{room.rowSize.invalid}")
    private String rowSize;

    @IsPositive(message = "{room.colSize.invalid}")
    private String colSize;
}
