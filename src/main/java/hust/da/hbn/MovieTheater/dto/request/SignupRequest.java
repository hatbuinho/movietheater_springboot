package hust.da.hbn.MovieTheater.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@NoArgsConstructor
@Getter
@Setter
@ToString
public class SignupRequest {

    @Size(min = 3, max = 20, message = "{user.username.invalid}")
    private String username;

    @Email(message = "{user.email.invalid}")
    private String email;

    private String role;

    @Size(min = 6, max = 40, message = "{user.password.invalid}")
    private String password;

    private String message;

    @NotBlank(message = "{user.fullName.invalid}")
    private String fullName;

    @NotNull(message = "{user.gender.invalid}")
    private String gender;

    @Pattern(regexp = "^0\\d{9}", message = "{user.phone.invalid}")
    private String phone;

    private String dateOfBirth;

    @NotBlank(message = "{user.address.invalid}")
    private String address;


    public SignupRequest(String message) {
        this.message = message;
    }
}
