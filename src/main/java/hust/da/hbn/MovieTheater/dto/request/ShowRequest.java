package hust.da.hbn.MovieTheater.dto.request;

import hust.da.hbn.MovieTheater.validation.annotation.IsLong;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

import hust.da.hbn.MovieTheater.validation.annotation.IsMoney;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ShowRequest {

    @NotBlank(message = "{show.date.invalid}")
    private String date;
    @NotBlank(message = "{show.startTime.invalid}")
    private String startTime;


    @IsMoney(message = "{price.invalid}")
    private String listPrice;

    //    @NotBlank(message = "{room.id.invalid}")
//    private String roomId;
    @NotBlank(message = "{movie.id.invalid}")
    private String movieId;
}
