package hust.da.hbn.MovieTheater.dto.request;

import hust.da.hbn.MovieTheater.validation.annotation.IsMoney;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PaymentRequest {
    @IsMoney
    private String amount;

    private String paymentMethod;

    private String bookingId;
}
