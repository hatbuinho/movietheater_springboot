package hust.da.hbn.MovieTheater.dto.request;


import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CityRequest {
    @NotBlank
    private String zipCode;
    @NotBlank
    private String name;
}
