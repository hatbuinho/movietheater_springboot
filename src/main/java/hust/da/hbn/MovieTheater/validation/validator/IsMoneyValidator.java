package hust.da.hbn.MovieTheater.validation.validator;

import java.math.BigDecimal;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import hust.da.hbn.MovieTheater.validation.annotation.IsMoney;

public class IsMoneyValidator implements ConstraintValidator<IsMoney, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        // TODO Auto-generated method stub
        boolean isValid = true;
        try {
        	System.out.println("valueeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee"+value);
            if (value == null || value.trim().isEmpty()) isValid = false;
            
            BigDecimal money = BigDecimal.valueOf(Double.parseDouble(value));
            System.out.println("before compareeeeeeeeeeeeeeeee" + isValid);
            if (money.compareTo(new BigDecimal(0)) == -1) isValid = false;
        } catch (Exception e) {
            isValid = false;
        }
        System.out.println("is money yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" + isValid);
        return isValid;
    }

}
