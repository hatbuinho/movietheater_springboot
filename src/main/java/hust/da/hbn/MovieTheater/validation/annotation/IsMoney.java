package hust.da.hbn.MovieTheater.validation.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import hust.da.hbn.MovieTheater.validation.validator.IsMoneyValidator;

@Documented
@Constraint(validatedBy = IsMoneyValidator.class)
@Retention(RUNTIME)
@Target({FIELD, METHOD})
public @interface IsMoney {

    String message() default "invalid money";

    // phai co de Hibernate validator hoat dong
    Class<?>[] groups() default {};

    // phai co de Hibernate validator hoat dong
    Class<? extends Payload>[] payload() default {};
}
