package hust.da.hbn.MovieTheater.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import hust.da.hbn.MovieTheater.validation.annotation.IsLong;

public class IsLongValidator implements ConstraintValidator<IsLong, String>{

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		boolean isValid = true;
		try {
			Long.parseLong(value);
		} catch(Exception e) {
			isValid = false;
		}
		return isValid;
	}
	
}
