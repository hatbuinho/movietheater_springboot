package hust.da.hbn.MovieTheater.validation.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import hust.da.hbn.MovieTheater.validation.validator.IsPositiveValidator;

@Documented
@Constraint(validatedBy = IsPositiveValidator.class)
@Retention(RUNTIME)
@Target({FIELD, METHOD})
public @interface IsPositive {

    String message() default "must not be negative";

    // phai co de Hibernate validator hoat dong
    Class<?>[] groups() default {};

    // phai co de Hibernate validator hoat dong
    Class<? extends Payload>[] payload() default {};
}
