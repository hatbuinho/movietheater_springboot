package hust.da.hbn.MovieTheater.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import hust.da.hbn.MovieTheater.validation.annotation.IsPositive;

public class IsPositiveValidator implements ConstraintValidator<IsPositive, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        boolean isValid = true;
        try {
            if (value == null || value.trim().isEmpty()) isValid = false;
            int intExpectedValue = Integer.parseInt(value);
            if (intExpectedValue <= 0) isValid = false;
        } catch (Exception e) {
            isValid = false;
        }

        return isValid;
    }

}
